       SUBROUTINE TREL(IE)  
C=========================================================
C1 POMERA PRIMITIVU U 2-D SCENI 
C
C  ULAZNE PROMENLJIVE:
C
C     IE    = REDNI BROJ PRIMITIVE
C
C  ULAZ:
C
C      USER INTERFACE
C
C  IZLAZ:
C
C      KOMON COMDRF
C---------------------------------------------------------
C $Header: trel.f,v 1.7 90/02/13 09:48:15 sveta Exp $
C $Log:	trel.f,v $
C---> Revision 1.7  90/02/13  09:48:15  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.6  88/05/23  11:05:27  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.5  87/12/07  16:22:11  sveta
C---> ??
C---> 
C---> Revision 1.4  87/12/04  15:46:36  sveta
C---> ??
C---> 
C---> Revision 1.3  87/10/19  17:23:06  sveta
C---> ???
C---> 
C---> Revision 1.2  87/05/25  14:01:56  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  12:02:40  joca
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'COMGRI.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'TTYPE.COM'
      INCLUDE 'GSPLT.COM'
      INCLUDE 'EROR.COM'
C
      DIMENSION C(3,3)
C--------------------------------- KONTROLA PIVOT-POINT-A
      INDPP=0
      INDXX=0
      INDYY=0
      XMOV=0.
      YMOV=0.
      XP=PIV(1,IE)
      YP=PIV(2,IE)
      IXP=(XP*FS+X0)*FACTW
      IYP=(YP*FS+Y0)*FACTW
      IF(IXP.GE.IXWMIN.AND.IXP.LT.IXWMAX)THEN
          IF(IYP.GE.IYWMIN.AND.IYP.LT.IYWMAX)THEN
             GOTO 10
          ELSE
             YMOV=-Y0/FS+YP
          ENDIF
      ELSE
          XMOV=-X0/FS+XP
          IF(IYP.GE.IYWMIN.AND.IYP.LT.IYWMAX)THEN
             CONTINUE
          ELSE
             YMOV=-Y0/FS+YP
          ENDIF
      ENDIF
      INDPP=1
      CALL TROM2D(XMOV,YMOV,0.,C)
      CALL DRFAZU(IE,C)
      CALL SCRAZU(IE)
C------------------------------------ POMERANJE PRIMITIVE
10    CALL WIDTH(3)
      CALL MOVPRI(IE,XF,YF,ANGLE)
      IF(IERR.NE.0)THEN
         IF(INDPP.EQ.1)THEN
            CALL TROM2D(-XMOV,-YMOV,0.,C)
            CALL DRFAZU(IE,C)
         ENDIF
         GOTO 99
      ENDIF
C--------------------------------- MATRICA TRANSFORMACIJE
      XF1=XF-PIV(1,IE)
      YF1=YF-PIV(2,IE)
      CALL TROM2D(XF1,YF1,ANGLE,C)
C-------------------------------------- AZURIRANJE TABELE
      CALL DRFAZU(IE,C)
C      CALL SCRAZU(IE)
C
99    RETURN
      END  
