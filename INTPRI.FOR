      SUBROUTINE INTPRI(IE1,IE2,NOINT,XINT,YINT)
C=============================================================
C1  RACUNA PRESEK 2 PRIMITIVE  
C
C  NAPOMENA:
C
C    AKO SU ZADATI NA PRIMER DUZ I KRUZNI LUK PROGRAM TRAZI
C    PRESEK PRAVE I KRUZNICE !!!
C
C  ULAZNI ARGUMENTI:
C
C    IE1               = REDNI BROJ PRVE PRIMITIVE
C    IE2               = REDNI BROJ DRUGE PRIMITIVE
C 
C  IZLAZNI ARGUMENTI:
C
C    NOINT             = BROJ PRESECNIH TACAKA
C    (XINT(i),YINT(i)) = KOORDINATE PRESECNIH TACAKA
C-------------------------------------------------------------
C $Header: intpri.f,v 1.4 90/02/07 12:20:53 sveta Exp $
C $Log:	intpri.f,v $
C---> Revision 1.4  90/02/07  12:20:53  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.3  89/12/06  12:36:23  sveta
C---> ??
C---> 
C---> Revision 1.2  89/06/05  12:09:34  sveta
C---> ubacen presek LINE - POLYLINE
C---> 
C---> Revision 1.1  88/10/13  13:09:16  sveta
C---> Initial revision
C---> 
C=============================================================
C
      INCLUDE 'COMDRF.COM'
      DIMENSION XINT(1),YINT(1),ARG1(200),ARG2(200)
      INTEGER IEDAT(10)
C     INTEGER IEDAT(NMAX)
      DATA NMAX/10/
      DATA IEDAT/1,2,3,4,5,6,7,8,9,15/
C
      NOINT=0
C
C  AKO JE IT1 < IT2 MENJA SE REDOSLED ARGUMENATA
C
      IT1=IELTYP(IE1)
      IP1=IELPOI(IE1)
      IR1=IELNR(IE1)
      DO 1 I=1,IR1
         ARG1(I)=ARGEL(IP1-1+I)
1     CONTINUE
C
      IT2=IELTYP(IE2)
      IP2=IELPOI(IE2)
      IR2=IELNR(IE2)
      DO 2 I=1,IR2
         ARG2(I)=ARGEL(IP2-1+I)
2     CONTINUE
C
      ICOUN=0
      I1=IT1
      I2=IT2
      IF(IT1.GT.IT2)THEN
         I1=IT2
         I2=IT1
         DO 11 I=1,IR1
            ARG2(I)=ARGEL(IP1-1+I)
11       CONTINUE
         DO 22 I=1,IR2
            ARG1(I)=ARGEL(IP2-1+I)
22       CONTINUE
      ENDIF
C
C NALAZENJE LABELE NA KOJU SE SKACE
C
      K=1
      DO 3 I=1,NMAX
         DO 4 J=K,NMAX
             ICOUN=ICOUN+1
             IF(I1.EQ.IEDAT(I).AND.I2.EQ.IEDAT(J))GOTO 5
4        CONTINUE
         K=K+1
3     CONTINUE
C
5     GOTO(100,200,300,400,500,600,700,800,900,1000,
     *     1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,
     *     2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,
     *     3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,
     *     4100,4200,4300,4400,4500,4600,4700,4800,4900,5000,
     *     5100,5200,5300,5400,5500)ICOUN
C
C
C------------------------------------------------
C        LINE  LINE
C------------------------------------------------
100   CONTINUE
      CALL XYABC(ARG1(1),ARG1(2),ARG1(3),ARG1(4),A1,B1,C1,IND)
      IF(IND.EQ.0)GOTO 99
      CALL XYABC(ARG2(1),ARG2(2),ARG2(3),ARG2(4),A2,B2,C2,IND)
      IF(IND.EQ.0)GOTO 99
      CALL INT2L(A1,B1,C1,A2,B2,C2,XINT(1),YINT(1),NOINT)
      GOTO 99
C
C------------------------------------------------
C        LINE  POLY
C------------------------------------------------
200   CONTINUE
          NOINT=0
          CALL XYABC(ARG1(1),ARG1(2),ARG1(3),ARG1(4),A1,B1,C1,IND)
          IF(IND.EQ.0)GOTO 99
          X1=ARG2(1)
          Y1=ARG2(2)
          ICN=3  
201       X2=ARG2(ICN)
          ICN=ICN+1
          Y2=ARG2(ICN)
          ICN=ICN+1
C--------------------- ISTO KAO ZA DVE DUZI --------------------
          CALL XYABC(X1,Y1,X2,Y2,A2,B2,C2,IND)
          IF(IND.EQ.0)GOTO 99
          CALL INT2L(A1,B1,C1,A2,B2,C2,XNT,YNT,IND)
          IF(IND.GT.0)THEN
             NOINT=NOINT+1
             XINT(NOINT)=XNT
             YINT(NOINT)=YNT
          ENDIF
C---------------------------------------------------------------
          X1=X2
          Y1=Y2
          IF(ICN.LT.IR2)GOTO 201
      GOTO 99
C
C------------------------------------------------
C        LINE  SPLI
C------------------------------------------------
300   CONTINUE
      GOTO 99
C
C------------------------------------------------
C        LINE  CIRC
C------------------------------------------------
400   CONTINUE
      CALL XYABC(ARG1(1),ARG1(2),ARG1(3),ARG1(4),A,B,C,NOINT)
      IF(NOINT.EQ.0)GOTO 99
      CALL ABCFG(A,B,C,X0,F,Y0,G,NOINT)
      IF(NOINT.EQ.0)GOTO 99
      CALL INTLC(X0,F,Y0,G,ARG2(1),ARG2(2),ARG2(3),XINT(1),YINT(1),
     *           XINT(2),YINT(2),NOINT)
      GOTO 99
C
C------------------------------------------------
C        LINE  ARC 
C------------------------------------------------
500   CONTINUE
      CALL DIST(ARG2(1),ARG2(2),ARG2(3),ARG2(4),R)
      CALL XYABC(ARG1(1),ARG1(2),ARG1(3),ARG1(4),A,B,C,NOINT)
      IF(NOINT.EQ.0)GOTO 99
      CALL ABCFG(A,B,C,X0,F,Y0,G,NOINT)
      IF(NOINT.EQ.0)GOTO 99
      CALL INTLC(X0,F,Y0,G,ARG2(1),ARG2(2),R,XINT(1),YINT(1),
     *           XINT(2),YINT(2),NOINT)
      GOTO 99
C
C------------------------------------------------
C        LINE  ELLI
C------------------------------------------------
600   CONTINUE
      GOTO 99
C
C------------------------------------------------
C        LINE  HYPE
C------------------------------------------------
700   CONTINUE
      GOTO 99
C
C------------------------------------------------
C        LINE  PARA
C------------------------------------------------
800   CONTINUE
      GOTO 99
C
C------------------------------------------------
C        LINE  BOX 
C------------------------------------------------
900   CONTINUE
      GOTO 99
C
C------------------------------------------------
C        LINE  NET 
C------------------------------------------------
1000  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  POLY
C------------------------------------------------
1100  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  SPLI
C------------------------------------------------
1200  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  CIRC
C------------------------------------------------
1300  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  ARC 
C------------------------------------------------
1400  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  ELLI
C------------------------------------------------
1500  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  HYPE
C------------------------------------------------
1600  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  PARA
C------------------------------------------------
1700  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  BOX 
C------------------------------------------------
1800  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        POLY  NET 
C------------------------------------------------
1900  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  SPLI
C------------------------------------------------
2000  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  CIRC
C------------------------------------------------
2100  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  ARC 
C------------------------------------------------
2200  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  ELLI
C------------------------------------------------
2300  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  HYPE
C------------------------------------------------
2400  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  PARA
C------------------------------------------------
2500  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  BOX 
C------------------------------------------------
2600  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        SPLI  NET 
C------------------------------------------------
2700  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        CIRC  CIRC
C------------------------------------------------
2800  CONTINUE
      CALL CIRCA(ARG1(1),ARG1(2),ARG2(1),ARG2(2),ARG1(3),ARG2(3),
     *           XINT(1),YINT(1),XINT(2),YINT(2),R,NOINT)
      GOTO 99
C
C------------------------------------------------
C        CIRC  ARC 
C------------------------------------------------
2900  CONTINUE
      CALL DIST(ARG2(1),ARG2(2),ARG2(3),ARG2(4),RR)
      CALL CIRCA(ARG1(1),ARG1(2),ARG2(1),ARG2(2),ARG1(3),RR,
     *           XINT(1),YINT(1),XINT(2),YINT(2),R,NOINT)
      GOTO 99
C
C------------------------------------------------
C        CIRC  ELLI
C------------------------------------------------
3000  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        CIRC  HYPE
C------------------------------------------------
3100  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        CIRC  PARA
C------------------------------------------------
3200  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        CIRC  BOX 
C------------------------------------------------
3300  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        CIRC  NET 
C------------------------------------------------
3400  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ARC   ARC 
C------------------------------------------------
3500  CONTINUE
      CALL DIST(ARG1(1),ARG1(2),ARG1(3),ARG1(4),RR1)
      CALL DIST(ARG2(1),ARG2(2),ARG2(3),ARG2(4),RR2)
      CALL CIRCA(ARG1(1),ARG1(2),ARG2(1),ARG2(2),RR1,RR2,
     *           XINT(1),YINT(1),XINT(2),YINT(2),R,NOINT)
      GOTO 99
C
C------------------------------------------------
C        ARC   ELLI
C------------------------------------------------
3600  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ARC   HYPE
C------------------------------------------------
3700  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ARC   PARA
C------------------------------------------------
3800  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ARC   BOX 
C------------------------------------------------
3900  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ARC   NET 
C------------------------------------------------
4000  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ELLI  ELLI
C------------------------------------------------
4100  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ELLI  HYPE
C------------------------------------------------
4200  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ELLI  PARA
C------------------------------------------------
4300  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ELLI  BOX 
C------------------------------------------------
4400  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        ELLI  NET 
C------------------------------------------------
4500  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        HYPE  HYPE
C------------------------------------------------
4600  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        HYPE  PARA
C------------------------------------------------
4700  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        HYPE  BOX 
C------------------------------------------------
4800  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        HYPE  NET 
C------------------------------------------------
4900  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        PARA  PARA
C------------------------------------------------
5000  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        PARA  BOX 
C------------------------------------------------
5100  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        PARA  NET 
C------------------------------------------------
5200  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        BOX   BOX 
C------------------------------------------------
5300  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        BOX   NET 
C------------------------------------------------
5400  CONTINUE
      GOTO 99
C
C------------------------------------------------
C        NET   NET 
C------------------------------------------------
5500  CONTINUE
      GOTO 99
C
99    RETURN
      END
