      COMMON/COMSET/IDEBUG,DEPREP,IPICK,IBACK,ITIME,AUTORE,AUTOCH
      LOGICAL AUTORE,AUTOCH
C=====================================================================
C1  KOMON ZA SPECIJALNE SISTEMSKE FUNKCIJE
C
C   OPIS PROMENLJIVIH:
C
C     IDEBUG   = PREKIDAC ZA KONTROLNU STAMPU
C     IPICK    = PREKIDAC ZA SINGLE/SELECT
C     IBACK    = VREME U [sec] POSLE KOJEG SE RADI BACK-UP
C     ITIME    = VREME PROTEKLO OD POSLEDNJEG BACK-UP-a
C     AUTORE   = FLAG FOR AUTOMATIC DECLARATION REAL VARIABLE
C     AUTOCH   = FLAG FOR AUTOMATIC DECLARATION CHARACTER VARIABLE
C---------------------------------------------------------------------
C $Header: COMSET.COM,v 1.4 90/04/26 09:38:16 sveta Exp $
C $Log:	COMSET.COM,v $
C---> Revision 1.4  90/04/26  09:38:16  sveta
C---> nova verzija interpretera
C---> 
C---> Revision 1.3  90/02/01  15:55:50  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  16:08:24  sveta
C---> ubacena promenljiva za back-up frekvenciju
C---> 
C---> Revision 1.1  87/04/10  08:50:38  joca
C---> Initial revision
C---> 
C=====================================================================
