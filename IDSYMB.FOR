      SUBROUTINE IDSYMB(IT,INDL,INDS,NOL,XC,YC,
     *                  XL,YL,HX,SX,SY,TEXT1,TEXT2)
C==================================================
C1  CRTA  ID  SIMBOLE I ISPISUJE TEXT [cm]
C     
C ULAZNE PROMENLJIVE:
C
C     IT     = TIP ZNAKA (1-10)
C     INDL   = INDIKATOR CRTANJA STRELICE
C              0 - NE, 1 - DA
C     INOS   = INDIKATOR STRANE ODAKLE POCINJE         
C              STRELICA, ZA SVAKI TIP ZNAKA IT
C              IMA DRUGACIJE ZNACENJE
C     NOL    = BROJ MEDJUTACAKA KOJE CINE STRELICU
C     XC,YC  = CENTAR ZNAKA
C     XL,YL  = VEKTORI SA MEDJUTACKAMA
C     H      = POLUPRECNIK OPISANE KRUZNICE OKO ZNAKA
C     SX,SY  = DIMENZIJE VRHA STRELICE
C     TEXT1  = PRVI TEXT KOJI SE UPISUJE U ZNAK
C     TEXT2  = DRUGI TEXT KOJI SE UPISUJE U ZNAK
C
C IZLAZ:
C    
C     EKRAN, PLOTER
C-------------------------------------------------
C $Header: idsymb.f,v 1.2 90/02/07 09:32:27 sveta Exp $
C $Log:	idsymb.f,v $
C---> Revision 1.2  90/02/07  09:32:27  sveta
C---> ispravljeno zaglavlje
C---> 
C=================================================
C
      dimension xl(8), yl(8)
      character*6 text1, text2, bl1, bl2
c
c 
c  CRTANJE SYMBOLA
c  
      h=hx/0.13
      a=cos(0.5235988)
      b=sin(0.5235988)
      oc=cos(0.78538)
      bl1='      '
      bl2='      '
c 
      goto (101,102,103,104,105,106,107,108,    
     1      109,110) it
      goto 9999
c
101   call circle(xc,yc,h)
      goto 99
c
102   call circle(xc,yc,h)
      x1=xc-h
      x2=xc+h
      call plot(x1,yc,3) 
      call plot(x2,yc,2)
      if(it.eq.9)goto 3
      goto 99
c
103   x1=xc-h*oc
      y1=yc-h*oc
      call plot(x1,y1,3)
      x2=xc+h*oc
      call plot(x2,y1,2)
      y2=yc+h*oc 
      call plot(x2,y2,2)
      call plot(x1,y2,2)
      call plot(x1,y1,2)
      if(it.eq.4)goto 1
      goto 99
c
104   goto 103
1     continue
      call plot(x1,yc,3)
      call plot(x2,yc,2)
      goto 99
c
105   x1=xc-h
      x2=xc-0.5*h
      x3=xc+0.5*h
      x4=xc+h
      y1=yc-h*a
      y2=yc+h*a
      call plot(x1,yc,3)
      call plot(x2,y1,2)
      call plot(x3,y1,2)
      call plot(x4,yc,2)
      call plot(x3,y2,2)
      call plot(x2,y2,2)
      call plot(x1,yc,2)
      if(it.eq.6)goto 2
      goto 99
c
106   goto 105
2     continue
      call plot(x4,yc,2)
      goto 99
c
107   x1=xc-a*h
      x2=xc+a*h
      y1=yc-a*h*0.5
      y2=yc+h
      call plot(x1,y1,3)
      call plot(x2,y1,2)
      call plot(xc,y2,2)
      call plot(x1,y1,2)
      goto 99
c
108   x1=xc-a*h
      x2=xc+a*h   
      y1=yc+a*h*0.5
      y2=yc-h
      call plot(x1,y1,3)
      call plot(xc,y2,2)
      call plot(x2,y1,2)
      call plot(x1,y1,2)
      goto 99
c
109   goto 102
3     continue
      y1=yc-h
      y2=yc+h
      call plot(xc,y1,3)
      call plot(xc,y2,2)
      goto 99
c
110   x1=xc-0.75*h
      x2=xc+0.75*h
      y1=yc-0.25*h
      y2=yc+0.25*h
      call arcc(x1,yc,x1,y1,x1,y2)
      call plot(x2,y1,2)
      call arcc(x2,yc,x2,y2,x2,y1)
      call plot(x1,y2,2)
      goto 99
c
c
c  ISPISIVANJE TEKSTA
c
c  ispitivanje duzine teksta    
c
99    n1=0
      n2=0
      n11=0
      n21=0
c
      call cistac(text1,n1,n2,n3)
      nraz=n2-n1
      nbr=nraz+1 
      if(nraz.eq.0)nbr=0
      if(nbr.eq.0)goto 17
      bl1(1:nbr)=text1(n1:n2)
c
c  ispisivanje teksta TEXT1
c
      if(it.eq.9.or.it.eq.10)goto 10
      ht=0.24*h
      if(it.eq.7.or.it.eq.8)ht=0.17*h
      duzt1=nbr*ht
      xt=xc-duzt1*0.5
      yt=yc-0.5*ht
      if(it.eq.2.or.it.eq.4.or.it.eq.6)
     1   yt=yc+0.05*h
      goto 11
c
10    ht=0.13*h
      duzt1=nbr*ht
      xt=xc-0.5*h-duzt1*0.5
      yt=yc+0.05*h
      if(it.eq.10)then
         ht=0.24*h
         yt=yc-0.5*ht
         duzt1=nbr*ht
         xt=xc-duzt1*0.5
      endif
c 
11    call symbol(xt,yt,ht,bl1,0,nbr)
c
c  ispisivanje teksta TEXT2
c
17    if(it.eq.2.or.it.eq.4.or.it.eq.6.or.it.eq.9)then
c
12      call cistac(text2,n11,n21,n3)
        nraz=n21-n11
        nbr1=nraz+1          
        if(nraz.eq.0)then   
          nbr1=0
        else 
          bl2(1:nbr1)=text2(n11:n21)
        endif
        if(it.eq.10)goto 13
        if(nbr1.eq.0)goto 14
c
        if(it.eq.2.or.it.eq.4.or.it.eq.6)then
          duzt2=nbr1*ht
          xt2=xc-duzt2*0.5
          yt2=yc-0.05*h-ht
        elseif(it.eq.9)then
          duzt2=ht*nbr1
          yt2=yc-0.05*h-ht 
          xt2=xc+(h-duzt2)*0.5 
        endif
c
      call symbol(xt2,yt2,ht,bl2,0,nbr1)
c
      endif
c  
      goto 14
c
13    ht=0.15*h
      l=nbr+1+nbr1
      if(nbr.eq.0)l=nbr1
      if(nbr1.eq.0)l=nbr
      if(nbr.eq.0.and.nbr1.eq.0)goto 14
      x1=xc-l*0.5*ht
      y1=yc-0.5*ht
      x2=x1+(nbr+1)*ht
      if(nbr.eq.0)x2=x1
      if(nbr.eq.0)goto 16
      call symbol(x1,y1,ht,bl1,0,nbr)
      if(nbr1.eq.0)goto 14
16    call symbol(x2,y1,ht,bl2,0,nbr1)
c
14    continue
c 
c  CRTANJE STRELICE
c     indl=0 strelica se ne crta
c     inds=0 strelica sa leve strane
c     inds=1 strelica sa desne strane
c
      if(indl.eq.0)goto 9999
      if(inds.ne.0)goto 6
c
      goto(201,201,202,202,201,201,203,203,
     1     201,201)it
c           
201   x1=xc-h
      x2=x1-0.5*h
      goto 999
c
202   x1=xc-h*oc
      x2=x1-0.5*h
      goto 999
c
203   x1=xc-0.66667*a*h
      x2=x1-0.5*h
      goto 999
c
c
6     goto(211,211,212,212,211,211,213,213,
     1     211,211)it
c
211   x1=xc+h
      x2=x1+0.5*h
      goto 999
c
212   x1=xc+h*oc
      x2=x1+0.5*h
      goto 999
c
213   x1=xc+0.67*a*h
      x2=x1+0.5*h      
c
c  crtanje izlomljene linije od znaka do strelice
c
999   call plot(x1,yc,3)
      call plot(x2,yc,2)
      do 5 i=nol,1,-1
      aa=xl(i)
      bb=yl(i)
      call plot(aa,bb,2)       
5     continue
c 
c  crtanje strelice
c 
      XX2=XL(2)
      YY2=YL(2)
      if(nol.ge.2)goto 15
      if(nol.eq.0)goto 9999
      xl(2)=x2
      yl(2)=yc
15    CONTINUE      
      xx1=xl(1)
      yy1=yl(1)
      
      ssx=sx
      ssy=sy
      if(ssx.eq.0.or.ssy.eq.0)goto 18
      call strel(ssx,ssy,xx1,yy1,XX2,YY2)
18    continue
c
c
9999  return
      end
