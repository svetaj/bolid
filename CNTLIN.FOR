      SUBROUTINE CNTLIN(X1,Y1,X2,Y2,XC,YC,IND)
C==========================================================
C2  NALAZI TACKU KOJA PREDSTAVLJA SREDINU DUZI
C
C  ULAZNE PROMENLJIVE:
C
C   X1,Y1   = PRVA TACKA DUZI
C   X2,Y2   = DRUGA TACKA DUZI
C 
C  IZLAZNE PROMENLJIVE:
C
C   XC,YC   = TACKA KOJA PREDSTAVLJA SREDINU DUZI
C   IND     = 0 - NEMA RESENJA ; 1 - IMA RESENJA
C
C  POZIVI PODPROGRAMA:
C
C   DIST    = NALAZI RASTOJANJE IZMEDJU 2 TACKE
C   LIND    = NALAZI DRUGO TEME DUZI KOJA JE POD UGLOM
C             ALFA U ODNOSU NA DATU DUZ I IMA JEDNO TEME
C             ZAJEDNICKO
C----------------------------------------------------------
C $Header: cntlin.f,v 1.2 90/02/02 15:19:42 sveta Exp $
C $Log:	cntlin.f,v $
C---> Revision 1.2  90/02/02  15:19:42  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  89/06/05  11:55:36  sveta
C---> Initial revision
C---> 
C==========================================================
C
      CALL DIST(X1,Y1,X2,Y2,DST)
      CALL LIND(X1,Y1,X2,Y2,DST/2.,0.,XC,YC,IND)
C
      RETURN
      END
