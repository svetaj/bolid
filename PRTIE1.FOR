      SUBROUTINE PRTIE1(IX)
C=========================================================
C1  LISTA PRIMITIVE IZ TABELE,
C1  SAMO ZA UNIT INCH 
C
C  ULAZNE PROMENLJIVE:
C
C     IX = REDNI BROJ PRIMITIVE U TABELI
C 
C  IZLAZ:
C
C     EKRAN
C---------------------------------------------------------
C $Header: prtie1.f,v 1.12 90/02/09 12:25:04 sveta Exp $
C $Log:	prtie1.f,v $
C---> Revision 1.12  90/02/09  12:25:04  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.11  89/12/06  12:46:30  sveta
C---> ??
C---> 
C---> Revision 1.10  89/10/11  11:48:37  sveta
C---> ubac. primitive IDS, SQR i FPT
C---> 
C---> Revision 1.9  89/07/20  12:56:54  sveta
C---> ubacen TABLE za HATCH, MDIM
C---> 
C---> Revision 1.8  88/10/13  13:13:37  sveta
C---> povecan broj dec. za redni br. na 6
C---> 
C---> Revision 1.7  88/05/23  11:03:44  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.6  88/04/08  08:48:01  vera
C---> WRITE ---> PORUKA!!!
C---> 
C---> Revision 1.5  88/04/06  11:26:44  vera
C---> WRITE --->  PORUKA!!!
C---> 
C---> Revision 1.4  88/02/01  13:48:08  sveta
C---> ??
C---> 
C---> Revision 1.3  87/12/07  16:21:49  sveta
C---> ??
C---> 
C---> Revision 1.2  87/10/20  19:05:05  sveta
C---> ??
C---> 
C---> Revision 1.1  87/10/05  11:49:10  sveta
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMDIA.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'COMTXT.COM'
      CHARACTER*12 AA(10)
      CHARACTER*80 LINOUT
      DIMENSION N1(100),N2(100),AAA(100),BB(100)
C
      IT=IELTYP(IX)
      IP=IELPOI(IX)
      NARG=IELNR(IX)
      IC=IELCOL(IX)
      IY=IELSTY(IX)
      IM=IELMA(IX)
C
C     DUZ
C
      IF(IT.EQ.1) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          CALL IDPF1(ARGEL(IP+3),AA(4))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,118) IX,AA(1),AA(2),AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
118          FORMAT(1X,I6,':LINE',6X,4(A10,', '))
          ELSE
             WRITE(LINOUT,11) IX,AA(1),AA(2),AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
11           FORMAT(1X,I6,':',4X,'LINE',6X,4(A10,', '))
          ENDIF
C
C     POLYLINE
C
      ELSE IF(IT.EQ.2) THEN
          ICN=1
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,2) IX,ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
2            FORMAT(1X,I6,':','POLYLINE',2X,I2,':',2X,2(A10,', '))
          ELSE
             WRITE(LINOUT,221) IX,ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
221          FORMAT(1X,I6,':',4X,'POLYLINE',2X,I2,':',2X,
     *              2(A10,', '))
          ENDIF
          ICOUN=2
21        X=ARGEL(IP+ICOUN)
          ICN=ICN+1
          ICOUN=ICOUN+1
          Y=ARGEL(IP+ICOUN)
          ICOUN=ICOUN+1
          CALL IDPF1(X,AA(1))
          CALL IDPF1(Y,AA(2))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,22) ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
22           FORMAT(16X,I2,':',1X,2(A10,', '))
          ELSE
             WRITE(LINOUT,222) ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
222          FORMAT(20X,I2,':',1X,2(A10,', '))
          ENDIF
          IF(ICOUN.LT.NARG) GOTO 21
C
C     SPLINE
C
      ELSE IF(IT.EQ.3) THEN
           CONTINUE
C
C     CIRCLE
C
      ELSE IF(IT.EQ.4) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,4) IX,AA(1),AA(2),AA(3)
             CALL PRTOUT(3,80,0,LINOUT,0)
4            FORMAT(1X,I6,':','CIRCLE',8X,3(A10,', '))
          ELSE
             WRITE(LINOUT,44) IX,AA(1),AA(2),AA(3)
             CALL PRTOUT(3,80,0,LINOUT,0)
44           FORMAT(1X,I6,':',4X,'CIRCLE',8X,3(A12,', '))
          ENDIF
C
C     ARC
C
      ELSE IF(IT.EQ.5) THEN
C
C     ARC 
C
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          CALL IDPF1(ARGEL(IP+3),AA(4))
          CALL IDPF1(ARGEL(IP+4),AA(5))
          CALL IDPF1(ARGEL(IP+5),AA(6))
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,5) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
5             FORMAT(1X,I6,':','ARC',11X,2(A10,', '))
              WRITE(LINOUT,51) AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
51            FORMAT(20X,2(A10,', '))
              WRITE(LINOUT,53) AA(5),AA(6)
             CALL PRTOUT(3,80,0,LINOUT,0)
53            FORMAT(20X,2(A10,', '))
          ELSE
              WRITE(LINOUT,57) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
57            FORMAT(1X,I6,':',4X,'ARC',T24,2(A10,', '))
              WRITE(LINOUT,517) AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
517           FORMAT(24X,2(A10,', '))
              WRITE(LINOUT,537) AA(5),AA(6)
             CALL PRTOUT(3,80,0,LINOUT,0)
537           FORMAT(24X,2(A10,', '))
          ENDIF
C
C     ELLIPSE
C
      ELSE IF(IT.EQ.6) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          CALL IDPF1(ARGEL(IP+3),AA(4))
          ALFA=ARGEL(IP+4)
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,6) IX,AA(1),AA(2),AA(3),AA(4),ALFA
             CALL PRTOUT(3,80,0,LINOUT,0)
6             FORMAT(1X,I6,':','ELLIPSE',7X,4(A10,', '),F8.3)
          ELSE
              WRITE(LINOUT,67) IX,AA(1),AA(2),AA(3),AA(4),ALFA
             CALL PRTOUT(3,80,0,LINOUT,0)
67            FORMAT(1X,I6,':',4X,'ELLIPSE',7X,4(A10,', '),F8.3)
          ENDIF
C
C     HYPERBOLA
C
      ELSE IF(IT.EQ.7) THEN
           CONTINUE
C
C     PARABOLA
C
      ELSE IF(IT.EQ.8) THEN
          CONTINUE
C
C     BOX
C
      ELSE IF(IT.EQ.9) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          CALL IDPF1(ARGEL(IP+3),AA(4))
          ALFA=ARGEL(IP+4)
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,9) IX,AA(1),AA(2),AA(3),AA(4),ALFA
             CALL PRTOUT(3,80,0,LINOUT,0)
9             FORMAT(1X,I6,':','BOX',11X,4(A10,', '),F8.3)
          ELSE
              WRITE(LINOUT,91) IX,AA(1),AA(2),AA(3),AA(4),ALFA
             CALL PRTOUT(3,80,0,LINOUT,0)
91            FORMAT(1X,I6,':',4X,'BOX',11X,4(A10,', '),F8.3)
          ENDIF
C
C     POINT
C
      ELSE IF(IT.EQ.10) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,10) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
10            FORMAT(1X,I6,':','POINT',9X,2(A10,', '))
          ELSE
              WRITE(LINOUT,101) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
101           FORMAT(1X,I6,':',4X,'POINT',9X,2(A10,', '))
          ENDIF
C
C    TEXT
C
      ELSE IF(IT.EQ.11) THEN
          CALL IDPF1(ARGTX(1,IP),AA(1))
          CALL IDPF1(ARGTX(2,IP),AA(2))
          CALL IDPF1(ARGTX(3,IP),AA(3))
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,111) IX,(AA(III),III=1,3),ARGTX(4,IP)
             CALL PRTOUT(3,80,0,LINOUT,0)
111           FORMAT(1X,I6,':','TEXT',10X,3(A10,', '),F8.3)
              WRITE(LINOUT,1177)(TX(III,IP),III=1,10)
             CALL PRTOUT(3,80,0,LINOUT,0)
117           FORMAT(T20,10A4)
          ELSE
              WRITE(LINOUT,112) IX,(AA(III),III=1,3),ARGTX(4,IP)
             CALL PRTOUT(3,80,0,LINOUT,0)
112           FORMAT(1X,I6,':',4X,'TEXT',10X,3(A10,', '),F8.3)
              WRITE(LINOUT,1177)(TX(III,IP),III=1,10)
             CALL PRTOUT(3,80,0,LINOUT,0)
1177          FORMAT(T24,10A4)
          ENDIF
C
C    DIM
C
      ELSE IF(IT.EQ.12) THEN
          CALL IDPF1(ARGEL(IP),AA(1))
          CALL IDPF1(ARGEL(IP+1),AA(2))
          CALL IDPF1(ARGEL(IP+2),AA(3))
          CALL IDPF1(ARGEL(IP+3),AA(4))
          IF(IM.EQ.0)THEN
              WRITE(LINOUT,12) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
12            FORMAT(1X,I6,':','DIMENSION',5X,2(A10,', '))
              WRITE(LINOUT,121) AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
121           FORMAT(20X,2(A12,', '))
          ELSE
              WRITE(LINOUT,127) IX,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
127           FORMAT(1X,I6,':',4X,'DIMENSION',5X,2(A10,', '))
              WRITE(LINOUT,1217) AA(3),AA(4)
             CALL PRTOUT(3,80,0,LINOUT,0)
1217          FORMAT(24X,2(A10,', '))
          ENDIF
C
C     SET
C
      ELSE IF(IT.EQ.13) THEN
          IZZZ=3
          IF(IBBB.EQ.3)IZZZ=5
          CALL LLDAIN(IZZZ,ICCC,IDDD)
          IF(IC.EQ.1) THEN
             IFA=IY
             IF(IM.EQ.0)THEN
                 WRITE(LINOUT,13) IX,IFA
             CALL PRTOUT(3,80,0,LINOUT,0)
13               FORMAT(1X,I6,':BEGIN FILL AREA',3X,I5)
             ELSE
                 WRITE(LINOUT,137) IX,IFA
             CALL PRTOUT(3,80,0,LINOUT,0)
137              FORMAT(1X,I6,4X,':BEGIN FILL AREA',3X,I5)
             ENDIF
          ELSE
             IF(IM.EQ.0)THEN
                 WRITE(LINOUT,131) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
131              FORMAT(1X,I6,':END FILL AREA')
             ELSE
                 WRITE(LINOUT,1317) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
1317             FORMAT(1X,I6,4X,':END FILL AREA')
             ENDIF
          ENDIF
          CALL LLDAIN(IBBB,ICCC,IDDD)
C
C     MAKRO 
C
      ELSE IF(IT.EQ.14) THEN
          CALL IDPF1(ARGMAC(1,IELPOI(IX)),AA(1))
          CALL IDPF1(ARGMAC(2,IELPOI(IX)),AA(2))
          IZZZ=2
          IF(IBBB.EQ.2)IZZZ=5
          CALL LLDAIN(IZZZ,ICCC,IDDD)
          WRITE(LINOUT,141)IX,(MACID(IMM,IELPOI(IX)),IMM=1,3),
     *                     AA(1),AA(2),ARGMAC(3,IELPOI(IX))
          CALL PRTOUT(3,80,0,LINOUT,0)
141       FORMAT(1X,I6,':MACRO  ',9X,3A4,3X,2(A10,', '),F8.3)
          CALL LLDAIN(IBBB,ICCC,IDDD)
C
C     NET
C
      ELSE IF(IT.EQ.15) THEN
C
        XX=ARGEL(IP)
        YY=ARGEL(IP+1)
        CALL IDPF1(XX,AA(1))
        CALL IDPF1(YY,AA(2))
        ALFA1=ARGEL(IP+2)
        ALFA2=ARGEL(IP+3)
        N1UK=IFIX(ARGEL(IP+4))
        N2UK=IFIX(ARGEL(IP+5))
        DO 1000 I=1,N1UK
           N1(I)=IFIX(ARGEL(IP+5+I))
           AAA(I)=ARGEL(IP+5+N1UK+N2UK+I)
1000    CONTINUE
        DO 1001 I=1,N1UK
           N2(I)=IFIX(ARGEL(IP+5+N1UK+I))
           BB(I)=ARGEL(IP+5+2*N1UK+N2UK+I)
1001    CONTINUE
       IF(IMM.EQ.0)THEN
        WRITE(LINOUT,671)IX,AA(1),AA(2),ALFA1,ALFA2,N1UK,N2UK
671     FORMAT(1X,I6,':NET',11X,2(A8,1X),2F8.3,2X,2I5)
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=0
6813    CALL IDPF1(AAA(ID+1),AA(1))
        CALL IDPF1(AAA(ID+2),AA(2))
        CALL IDPF1(AAA(ID+3),AA(3))
        CALL IDPF1(AAA(ID+4),AA(4))
        WRITE(6,68)N1(ID+1),AA(1),N1(ID+2),AA(2),
     *                  N1(ID+3),AA(3),N1(ID+4),AA(4)
68      FORMAT(20X,'H: ',4(I4,'x',A8))
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=ID+4
        IF(ID.LT.N1UK)GOTO 6813
        ID=0
6913    CALL IDPF1(BB(ID+1),AA(1))
        CALL IDPF1(BB(ID+2),AA(2))
        CALL IDPF1(BB(ID+3),AA(3))
        CALL IDPF1(BB(ID+4),AA(4))
        WRITE(6,69)N2(ID+1),AA(1),N2(ID+2),AA(2),
     *                  N2(ID+3),AA(3),N2(ID+4),AA(4)
69      FORMAT(20X,'V: ',4(I4,'x',A8))
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=ID+4
        IF(ID.LT.N2UK)GOTO 6913
       ELSE
        WRITE(LINOUT,77)IX,AA(1),AA(2),ALFA1,ALFA2,N1UK,N2UK
77      FORMAT(1X,I6,':',4X,'NET',11X,2(A8,1X),2F8.3,2X,2I5)
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=0
7813    CALL IDPF1(AAA(ID+1),AA(1))
        CALL IDPF1(AAA(ID+2),AA(2))
        CALL IDPF1(AAA(ID+3),AA(3))
        CALL IDPF1(AAA(ID+4),AA(4))
        WRITE(6,68)N1(ID+1),AA(1),N1(ID+2),AA(2),
     *                  N1(ID+3),AA(3),N1(ID+4),AA(4)
78      FORMAT(24X,'H: ',4(I4,'x',A8))
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=ID+4
        IF(ID.LT.N1UK)GOTO 7813
        ID=0
7913    CALL IDPF1(BB(ID+1),AA(1))
        CALL IDPF1(BB(ID+2),AA(2))
        CALL IDPF1(BB(ID+3),AA(3))
        CALL IDPF1(BB(ID+4),AA(4))
        WRITE(6,69)N2(ID+1),AA(1),N2(ID+2),AA(2),
     *                  N2(ID+3),AA(3),N2(4),AA(4)
79      FORMAT(24X,'V: ',4(I4,'x',A8))
        CALL PRTOUT(3,80,0,LINOUT,0)
        ID=ID+4
        IF(ID.LT.N2UK)GOTO 7913
       ENDIF
      ELSE IF(IT.EQ.16)THEN
          ICN=0
           NLOOP=IFIX(ARGEL(IP))
           IFS=IFIX(ARGEL(IP+NLOOP+1))
           IBR=IFIX(ARGEL(IP+NLOOP+2))
           AG=ARGEL(IP+NLOOP+3)
           AD=ARGEL(IP+NLOOP+4)
          CALL IDPF1(AD,AA(1))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,1189) IX,IFS,IBR,AG,AA(1)
             CALL PRTOUT(3,80,0,LINOUT,0)
1189         FORMAT(1X,I6,':HATCH',2X,2I4,2X,F8.3,2X,A12)
          ELSE
             WRITE(LINOUT,119) IX,IFS,IBR,AG,AA(1)
             CALL PRTOUT(3,80,0,LINOUT,0)
119          FORMAT(1X,I6,':',4X,'HATCH',2X,2I4,2X,F8.3,2X,A12)
          ENDIF
          ICOUN=0
          IP=IP+IFIX(ARGEL(IP))+5
219       X=ARGEL(IP+ICOUN)
          ICN=ICN+1
          ICOUN=ICOUN+1
          Y=ARGEL(IP+ICOUN)
          ICOUN=ICOUN+1
          CALL IDPF1(X,AA(1))
          CALL IDPF1(Y,AA(2))
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,229) ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
229          FORMAT(16X,I2,':',1X,2(A10,', '))
          ELSE
             WRITE(LINOUT,2229) ICN,AA(1),AA(2)
             CALL PRTOUT(3,80,0,LINOUT,0)
2229         FORMAT(20X,I2,':',1X,2(A10,', '))
          ENDIF
          IF(ICOUN.LT.NARG) GOTO 219
      ELSE IF(IT.EQ.17)THEN
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,11891) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
11891        FORMAT(1X,I6,':MECHANICAL DIMENSIONING')
          ELSE
             WRITE(LINOUT,1191) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
1191         FORMAT(1X,I6,':',4X,'MECHANICAL DIMENSIONING')
          ENDIF
C
C ------- ID SYMBOL  ----------------------------------
C
      ELSE IF(IT.EQ.18)THEN
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,21891) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
21891        FORMAT(1X,I6,':ID SYMBOL')
          ELSE
             WRITE(LINOUT,2191) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
2191         FORMAT(1X,I6,':',4X,'ID SYMBOL')
          ENDIF
C
C ------- SQR SYMBOL ----------------------------------
C
      ELSE IF(IT.EQ.19)THEN
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,31891) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
31891        FORMAT(1X,I6,':SQR SYMBOL')
          ELSE
             WRITE(LINOUT,3191) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
3191         FORMAT(1X,I6,':',4X,'SQR SYMBOL')
          ENDIF
C
C ------- FPT SYMBOL ----------------------------------
C
      ELSE IF(IT.EQ.20)THEN
          IF(IM.EQ.0)THEN
             WRITE(LINOUT,41891) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
41891        FORMAT(1X,I6,':FPT SYMBOL')
          ELSE
             WRITE(LINOUT,4191) IX
             CALL PRTOUT(3,80,0,LINOUT,0)
4191         FORMAT(1X,I6,':',4X,'FPT SYMBOL')
          ENDIF
C
      ENDIF
C
99    RETURN
      END
