      SUBROUTINE AXSMOD
C==========================================================================
C1  PODPROGRAM ZA MODULARNO OBELEZAVANJE OSA
C
C  IZLAZ:
C
C     EKRAN
C--------------------------------------------------------------------------
C $Header: axsmod.f,v 1.3 90/02/02 10:14:24 sveta Exp $
C $Log:	axsmod.f,v $
C---> Revision 1.3  90/02/02  10:14:24  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  88/01/07  14:02:14  sveta
C---> UVEDENA DINAMICKA DODELA BROJEVA SEGMENATA.
C---> 
C---> Revision 1.1  87/06/29  06:53:59  sveta
C---> Initial revision
C---> 
C==========================================================================
C
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'COMGRI.COM'
      INCLUDE 'COMRAS.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'COMSEG.COM'
      INCLUDE 'COMDRF.COM'
C
C     PROVERAVA SE DA LI JE KOORDINATNI POCETAK
C     SMESTEN U TEME RASTERA
C     NOMARK=2 --> JESTE
C
      NOMARK=0
      IRX=0
      X=0.
      IRY=0
      Y=0.
      IF(X0.EQ.0..AND.Y0.EQ.0.) GOTO 4
1     IRX=IRX+1
      IF(IRX.GT.NRX)IRX=1
      X=X+RAX(IRX)*FS
      IF(ABS(X-X0).LT.0.0001) GOTO 2
      IF(X.LT.X0) GOTO 1
      GOTO 33
2     NOMARK=1
3     IRY=IRY+1
      IF(IRY.GT.NRY) IRY=1
      Y=Y+RAY(IRY)*FS
      IF(ABS(Y-Y0).LT.0.001) GOTO 4
      IF(Y.LT.Y0) GOTO 3
      GOTO 33
4     NOMARK=2
      IRXF=IRX+1
      IF(IRXF.GT.NRX)IRXF=1
      IRXB=IRX
      IF(IRXB.EQ.0) IRXB=NRX
      IRYF=IRY+1
      IF(IRYF.GT.NRY)IRYF=1
      IRYB=IRY
      IF(IRYB.EQ.0) IRYB=NRY
C
C
C     CRTANJE OSA
C
C
33    HA=0.3
      HM=36.37
      YM=27.85
      D=0.1
      D2=D/2
      CALL PLOTSE(0,0,11)
      CALL PPLNIN(2)
      IS=IVSEG(IWTEK,3)
      IF(IS.NE.0)CALL SSDLSG(IS)
      CALL SSOPSG(NPSEG)
      IVSEG(IWTEK,3)=NPSEG
      CALL PLOT(0.,Y0,3)
      CALL PLOT(XM,Y0,2)
      CALL PLOT(X0,0.,3)
      CALL PLOT(X0,YM,2)
      CALL NUMBER(X0+D,Y0+D,HA,0.,0.,-1)
      IF(NOMARK.NE.2) GOTO 999
C
C     X-OSA U OBA SMERA
C
      DO 111 JX=1,2
      X=X0
      IPOS=1
      IRX=IRXF
      IF(JX.EQ.2) IRX=IRXB
100   DX=RAX(IRX)*FS
      IF(JX.EQ.1) THEN
      X=X+DX
      R=FLOAT(IPOS)
      IRX=IRX+1
      IF(IRX.GT.NRX) IRX=1
      IF(X.GT.XM) GOTO 111
      ELSE
      X=X-DX
      R=-FLOAT(IPOS)
      IRX=IRX-1
      IF(IRX.LT.1)IRX=NRX
      IF(X.LT.0.) GOTO 111
      ENDIF
      CALL PLOT(X,Y0-D2,3)
      CALL PLOT(X,Y0+D2,2)
      IF(FS.LT.0.5.AND.(IPOS-5*(IPOS/5)).NE.0) GOTO 121
      CALL NUMBER(X,Y0+D,HA,R,0.,-1)
121   IPOS=IPOS+1
      GOTO 100
111   CONTINUE
C
C     Y-OSA U OBA SMERA
C
      DO 222 JY=1,2
      Y=Y0
      IPOS=1
      IRY=IRYF
      IF(JY.EQ.2) IRY=IRYB
200   DY=RAY(IRY)*FS
      IF(JY.EQ.1) THEN
      Y=Y+DY
      R=FLOAT(IPOS)
      IRY=IRY+1
      IF(IRY.GT.NRY)IRY=1
      IF(Y.GT.YM) GOTO 222
      ELSE
      R=-FLOAT(IPOS)
      IRY=IRY-1
      IF(IRY.LT.1)IRY=NRY
      IF(Y.LT.0.) GOTO 222
      ENDIF
      CALL PLOT(X0-D2,Y,3)
      CALL PLOT(X0+D2,Y,2)
      IF(FS.LT.0.5.AND.(IPOS-5*(IPOS/5)).NE.0) GOTO 212
      CALL NUMBER(X0+D,Y,HA,R,0.,-1)
212   IPOS=IPOS+1
      GOTO 200
222   CONTINUE
      CALL SSCLSG
999   CALL PPLNIN(ILIN)
      RETURN
      END
