      SUBROUTINE OPNMSG(IMEMSG,OPNSTA)
C=========================================================
C1  ROUTINE TO OPEN THE MESSAGE FILE
C
C   ULAZNE PROMENLJIVE:
C
C     IMEMSG   = IME FAJLA SA PORUKAMA (*)
C
C   IZLAZNE PROMENLJIVE:
C
C     OPNSTA   = INDIKATOR USPESNOSTI OTVARANJA
C
C   IZLAZ:
C
C     KOMON EROR
C---------------------------------------------------------
C $Header: opnmsg.f,v 1.2 90/02/08 16:34:39 sveta Exp $
C $Log:	opnmsg.f,v $
C---> Revision 1.2  90/02/08  16:34:39  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/04/22  11:35:03  vera
C---> Initial revision
C---> 
C---> Revision 1.1  88/01/08  14:36:57  joca
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'EROR.COM'
      INCLUDE 'PROTCT.COM'
      INCLUDE 'OUTMSG.COM'
      CHARACTER*(*) IMEMSG
      CHARACTER*(*) OPNSTA
      LUNMSG=75
      OPEN(LUNMSG,ERR=98,FILE=IMEMSG,STATUS=OPNSTA,
     *     ACCESS='DIRECT',FORM='UNFORMATED',RECL=LENREC)
      GOTO 99
C
98    CONTINUE
      IERR=1
      GOTO 99
C
99    CONTINUE
      RETURN
      END
