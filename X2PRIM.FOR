      SUBROUTINE X2PRIM(X,Y,IND)
C=================================================================
C1  GLAVNA RUTINA ZA PRESEK 2 PRIMITIVE
C
C   ULAZ:
C
C      USER INTERFACE
C      KOMON COMDRF
C
C   IZLAZNE PROMENLJIVE:
C
C      X,Y  = IZABRANA TACKA PRESEKA
C      IND  = 0 - OK 
C             1 - QUIT
C-----------------------------------------------------------------
C $Header: x2prim.f,v 1.2 90/02/13 12:55:09 sveta Exp $
C $Log:	x2prim.f,v $
C---> Revision 1.2  90/02/13  12:55:09  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/06/05  12:12:34  sveta
C---> Initial revision
C---> 
C=================================================================
C  
      INCLUDE 'COMVAR.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'EROR.COM'
      INCLUDE 'INDIK.COM'
      INCLUDE 'COMGRI.COM'
      INCLUDE 'GINBEG.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'GSPLT.COM'
      DIMENSION NV(1),XSEC(1),YSEC(1),XTEMP(4),YTEMP(4)
      DIMENSION XINT(100),YINT(100),DST(100)
      CHARACTER NKEY
C
      IND=1
C
60    CONTINUE
64    PRINT *,' PICK FIRST CURVE'
      CALL PICKPE(X1,Y1,IE1)
      IF(IE1) 98,62,67
62    CALL PORUKA(166,0)
      GOTO 64
C
67    PRINT *,' PICK SECOND CURVE'
      CALL PICKPE(X2,Y2,IE2)
      IF(IE2) 98,65,66
65    CALL PORUKA(166,0)
      GOTO 67
C
66    IF(IE1.EQ.IE2)GOTO 67
      CALL INTPR1(IE1,IE2,NOINT,XINT,YINT)
      IF(NOINT.EQ.0)GOTO 99
      DO 69 I=1,NOINT
         CALL MARKER(5,XINT(I),YINT(I))
69    CONTINUE
      NTEST=1
C
      IF(NOINT.GT.1)THEN
         CALL PORUKA(800,0)
         CALL CURSXY(NKEY,XT,YT)
         IF(NKEY.EQ.'E')GOTO 99
         IF(NKEY.EQ.'Q')GOTO 99
C
         CALL DIST(XT,YT,XINT(1),YINT(1),DST(1))
         DTEST=DST(1)
         DO 68 I=2,NOINT
            CALL DIST(XT,YT,XINT(I),YINT(I),DST(I))
            IF(DST(I).LT.DTEST)THEN
                DTEST=DST(I)
                NTEST=I
            ENDIF
68       CONTINUE
      ENDIF
C
      X=XINT(NTEST)
      Y=YINT(NTEST)
      IND=0
C
C AZURIRANJE IXBEG,IYBEG
C
      RXF=(X*FS-X0)*FACTW
      IXBEG=NINT(RXF)
      RYF=(Y*FS-X0)*FACTW
      IYBEG=NINT(RYF)
      GOTO 99
C
98    IND=1
      GOTO 99
C
99    RETURN
      END  
