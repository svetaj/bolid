      COMMON /GRPTMP/ IOPG,NOPG,MAXTG,IDTG(3),ITEMPG(18000)
      CHARACTER*4 IDTG
C====================================================================
C1 TABELA ZA PRIVREMENO CUVANJE PRIMITIVA KOJE TREBAJU DA UDJU U
C1 GRUPU
C
C OPIS PROMENLJIVIH:
C
C  ITEMPG  = TABELA SA REDNIM BROJEVIMA PRIMITIVA
C  IOPG    = INDIKATOR DA LI JE GRUPA OTVORENA : 0 - NE ; 1 - DA
C  NOPG    = ADRESA ZADNJEG POPUNJENOG ENTRY-A U ITEMPG
C  MAXTG   = MAX DUZINA ITEMPG / MAXTG=IELMAX /
C  IDTG    = IME BUDUCE GRUPE
C-------------------------------------------------------------------
C $Header: GRPTMP.COM,v 1.3 90/02/01 15:56:29 sveta Exp $
C $Log:	GRPTMP.COM,v $
C---> Revision 1.3  90/02/01  15:56:29  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  16:09:24  sveta
C---> ?
C---> 
C---> Revision 1.1  88/02/26  11:03:24  sveta
C---> Initial revision
C---> 
C===================================================================
