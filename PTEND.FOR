        SUBROUTINE PTEND(X,Y)
C=================================================================
C1  PROGRAM ZA SELEKTIRANJE I IZBOR KRAJNJIH TACAKA ENTITETA
C1  /PERINA RUTINA !!!!/
C1  NE KORISTI SE
C
C    ULAZ:
C
C       USER INTERFACE
C       KOMON COMDRF
C
C    IZLAZNE PROMENLJIVE:
C
C       X,Y     = KOORDINATE DATE TACKE
C
C    POZIVI PODPROGRAMA:
C
C       PICKPE  = POKAZIVANJE PRIMITIVE NA EKRANU
C-----------------------------------------------------------------
C $Header: ptend.f,v 1.3 90/02/15 11:53:22 sveta Exp $
C $Log:	ptend.f,v $
C---> Revision 1.3  90/02/15  11:53:22  sveta
C---> ispravljeno zaglavlje
C---> 
C=================================================================
C
        include 'COMDRF.COM'
        include 'COMVAR.COM'
        include 'OZNAKE.COM'
        include 'TTYPE.COM'
        include 'ptcom.com'

        character*1 nkey
        call poruka(ibpt(24),0)
        call pickpe(xff,yff,ie)
        it=ieltyp(ie)
C
C ako je selektirana duz
C
        ip=ielpoi(ie)
        if(it.eq.1)then
                xk=argel(ip)
                yk=argel(ip+1)
                xl=argel(ip+2)
                yl=argel(ip+3)
        end if
C
C  ako je izabrana kruznica
C
        if(it.eq.4)then
                xc=argel(ip)
                yc=argel(ip+1)
                r=argel(ip+2)
                call citoar(xc,yc,r,xk,yk,xl,yl)
        end if
c
C ako je selektiran luk
C
        if(it.eq.5)then
                xc=argel(ip)
                yc=argel(ip+1)
                xk=argel(ip+2)
                yk=argel(ip+3)
                xl=argel(ip+4)
                yl=argel(ip+5)
        end if
        call dist(xff,yff,xk,yk,dist1)
        call dist(xff,yff,xl,yl,dist2)
        if(dist1.lt.dist2)then
                 x=xk
                 y=yk
        else
                 x=xl
                 y=yl
        end if
        return
        end
