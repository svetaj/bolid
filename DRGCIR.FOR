        SUBROUTINE DRGCIR(X,Y,R1,DR,R2,IST)
C=================================================================
C1 RUTINA ODABIRA POLUPRECNIK KRUZNICE INTERAKTIVNIM MENJANJEM
C1 (DRAGGING POSTUPKOM)   
C
C   ULAZNE PROMENLJIVE:
C
C         X,Y = CENTAR KRUZNICE
C         R1  = POCETNI POLUPRECNIK KRUZNICE
C         DR  = KORAK PROMENE R1
C
C   ULAZ:
C
C         USER INTERFACE
C
C   IZLAZNE PROMENLJIVE:
C
C         R2  = IZABRANI POLUPRECNIK KRUZNICE
C         IST = STATUS 
C               0 - OK
C               1 - QUIT
C-----------------------------------------------------------------
C $Header: drgcir.f,v 1.3 90/02/05 15:08:31 sveta Exp $ 
C $Log:	drgcir.f,v $
C---> Revision 1.3  90/02/05  15:08:31  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  89/06/05  12:05:01  sveta
C---> KOMPLETIRANA FUNKCIJA DRAGGING . SADA RADI I POMERANJE
C---> UBACENA ZASTITA OD PREVELIKOG SMANJIVANJA.
C---> KORAK UVECANJA JE SADA JEDNAK GRID-U; POCETNI POLUPRECNIK JE 
C---> FIKSNE VELICINE 5 cm.
C---> 	
C=================================================================
C
      INCLUDE 'COMVAR.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMGRI.COM'
      INCLUDE 'GSPLT.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'GINBEG.COM'
      CHARACTER CHAR*1
      DATA ACCY/1.E-5/
C
      SCALEX=1.
      SCALEY=1.
      XP=X*FS+X0 
      YP=Y*FS+Y0 
      IIX=XP*FACTW
      IIY=YP*FACTW
C  
C
C Setujemo pivot-point u centru
C
      CALL PIVOT(X,Y)
C
C Nacrtamo pocetnu kruznicu u segmentu
C
      CALL SSOPSG(ISEG)
C
      CALL LLLNIN(ILIN)
      CALL CIRC(X,Y,R1)
      CALL MARKER(2,X,Y)
C
C Zatvaranje novog segmenta
C
      CALL SSCLSG
C
C Cita se jedan karakter sa tastature
C
      R2=R1
      CALL PORUKA(835,0)
10    IXBEG=IIX
      IYBEG=IIY
      CALL LLCRGN(LLOC,ISEG)
      CALL CURSXY(CHAR,XX,YY)
      CALL LLCRGN(LLOC,0)
      IIX=IXBEG
      IIY=IYBEG
      X=XX
      Y=YY
      IKAR=ICHAR(CHAR)
      IF(IKAR.EQ.70.OR.IKAR.EQ.69) GOTO 20
      IF(IKAR.EQ.81) GOTO 30 
C
      IF(IKAR.EQ.45) THEN
        SCALEX=SCALEX-DR
        IF(SCALEX.LT.ACCY)SCALEX=ACCY
        SCALEY=SCALEX
      ENDIF
      IF(IKAR.EQ.43) THEN
        SCALEX=SCALEX+DR
        IF(SCALEX.LT.ACCY)SCALEX=ACCY
        SCALEY=SCALEX
      ENDIF
C
C Reskaliramo dati segment pri svakom pritisku '+' ili '-'
C
      CALL LLIMSG(ISEG,SCALEX,SCALEY,0.,IIX,IIY)
      GOTO 10
C
20       R2=R2*SCALEX
         IST=0
         GOTO 99 
30       IST=1
C
99     CALL SSDLSG(ISEG)
       R=R2
       RETURN
       END
