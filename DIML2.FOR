      SUBROUTINE DIML2(X1,Y1,X2,Y2,DU,XA,YA,XB,YB)
C=================================================================
C1  CRTA KOTNU LINIJU :    --------------------
C
C ULAZNE PROMENLJIVE:
C
C   (X1,Y1),(X2,Y2)  =  KOORDINATE REFERENTNE DUZI
C    DU              =  RASTOJANJE KOTNE I REFERENTNE DUZI
C   (XA,YA),(XB,YB)  =  KOORDINATE KOTNE DUZI
C
C IZLAZ:
C
C    EKRAN, PLOTER
C-----------------------------------------------------------------
C $Header: diml2.f,v 1.2 90/02/05 12:12:36 sveta Exp $
C $Log:	diml2.f,v $
C---> Revision 1.2  90/02/05  12:12:36  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  87/05/25  13:50:37  sveta
C---> Initial revision
C---> 
C=================================================================
C
      CALL MOVE(XA,YA)
      CALL DRAW(XB,YB)
C
      RETURN
      END
