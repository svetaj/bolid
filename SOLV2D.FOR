      SUBROUTINE SOLV2D(INDIC) 
C=========================================================
C1 ODABIRANJE KOMANDE IZ DRAFT MODA
C1 NE KORISTI SE
C  
C   ULAZNE PROMENLJIVE:
C
C      INDIC = 0 - FOR INTERNAL LOOP MODE
C              1 - FOR EXITING IMIDIATELY 
C                  AFTER A COMMAND IS EXECUTED
C---------------------------------------------------------
C $Header: solv2d.f,v 1.14 90/02/12 16:04:38 sveta Exp $
C $Log:	solv2d.f,v $
C---> Revision 1.14  90/02/12  16:04:38  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.13  89/05/15  11:45:23  sveta
C---> ubacene komande ICOMPILE i ILOAD 
C---> 
C---> Revision 1.12  89/05/08  15:23:55  sveta
C---> UBACENE KOMANDE MARC MARX
C---> 
C---> Revision 1.11  89/04/24  09:07:00  sveta
C---> DODATO KREIRANJE MACRO ARCHIVE FAJLA
C---> 
C---> Revision 1.10  88/12/08  09:17:26  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.9  88/10/13  13:17:07  sveta
C---> UBACENE NOVE KOMANDE ZA RAD SA LAYER-IMA
C---> 
C---> Revision 1.8  88/05/23  11:05:21  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.7  88/04/22  11:17:06  vera
C---> ??
C---> 
C---> Revision 1.6  88/04/11  14:42:37  vera
C---> MSG  ---> PORUKA!!!
C---> 
C---> Revision 1.5  88/04/11  13:15:43  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.4  87/11/12  11:56:35  sveta
C---> ??
C---> 
C---> Revision 1.3  87/10/05  11:51:15  sveta
C---> ???
C---> 
C---> Revision 1.2  87/05/25  14:00:59  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  11:55:54  joca
C---> Initial revision
C---> 
C=========================================================
C  
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'EROR.COM'
      INCLUDE 'LIN80.COM'
C  
1     CONTINUE 
      IF(INDIC.EQ.0) THEN
         CALL PORUKA(654,0)
         CALL READL1 
         CALL NORMAL(0)
      ENDIF
      CALL VAR 
      IF(ITIP.NE.1) GOTO 88
      CALL V2DKEY(ID(1),IKEY)  
      IF(IKEY.EQ.0) GOTO 87
      GOTO(999,120,130,140,150,160,170,180,190,200,210,220,230,
     *     240,250,260,270,280,290,300,310,320,330,340,350,360,
     *     370),IKEY
C
120   CALL V2DHLP  
      GOTO 88  
130   CALL VIEDRF  
      GOTO 88  
140   CALL LISDRF  
      GOTO 88  
150   CALL RECOIN  
      GOTO 88  
160   CALL SOLGO
      GOTO 88
170   CALL REACRT
      GOTO 88
180   CALL WRICRT
      GOTO 88
190   CALL RENCRT
      GOTO 88
200   CALL DELCRT
      GOTO 88
210   CALL PIPCRT
      GOTO 88
220   CALL PRTCRT
      GOTO 88
230   CALL LAYER
      GOTO 88
240   CALL EDTCRT
      GOTO 88
250   CALL REACR1
      GOTO 88
260   CALL LCOPY
      GOTO 88
270   CALL LVIEW
      GOTO 88
280   CALL LFREEZ(1)
      GOTO 88
290   CALL LMOVE
      GOTO 88
300   CALL LFREEZ(0)
      GOTO 88
310   CALL ARCHIV(1,1)
      GOTO 88
320   CALL ARCHIV(1,2)
      GOTO 88
330   CALL ARCHIV(2,1)
      GOTO 88
340   CALL ARCHIV(2,2)
      GOTO 88
350   CALL TOUCH(1)
      GOTO 88
360   CALL IGESOU
      GOTO 88
C370   CALL IGESIN
370   CONTINUE
      GOTO 88
C
87    CONTINUE 
      IF(INDIC.EQ.1) GOTO 88
C
C MOZDA JE DIREKTNO KUCANA DRAFT KOMANDA
C
      IERR=0
      IKAR=0
      CALL DRFCOM(1)
      IF(IERR.EQ.0) GOTO 88
C
C MOZDA JE DIREKTNO KUCANA SCREEN KOMANDA  
C
      IERR=0
      IKAR=0
      CALL SOLSCR(1)
      IF(IERR.EQ.0) GOTO 88
C
C MOZDA JE DIREKTNO KUCANA MACRO KOMANDA
C
      IERR=0
      IKAR=0
      CALL SOLMAC(1)
      IF(IERR.EQ.0) GOTO 88
C
C MOZDA JE DIREKTNO KUCANA MODIFY KOMANDA  
C
      IERR=0
      IKAR=0
      CALL DRFMDI(1)
      IF(IERR.EQ.0) GOTO 88
      IERR=0
C
      CALL PORUKA(363,0)
88    CONTINUE 
      IF(IKEY.EQ.0.AND.INDIC.EQ.1) IERR=1  
      IF(INDIC.EQ.0) GOTO 1
C
999   RETURN
      END  
