       SUBROUTINE WINDAL
C=========================================================
C1  SET WINDOW ALL, IWMAX WINDOWS TOGETER ON THE SCREEN  
C
C  IZLAZ:
C
C    EKRAN
C---------------------------------------------------------
C $Header: windal.f,v 1.2 90/02/13 12:54:41 sveta Exp $
C $Log:	windal.f,v $
C---> Revision 1.2  90/02/13  12:54:41  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  87/04/09  12:12:04  joca
C---> Initial revision
C---> 
C=========================================================
C  
      INCLUDE 'LIN80.COM'
      INCLUDE 'COMSCR.COM'
      INCLUDE 'COMWIN.COM'
C  
      CALL LLSLVW(IWTOT+1) 
      CALL LLPAGE  
C  
      DO 10 IW=1,IWMAX 
          CALL LLSLVW(IW)  
          IX1=IXSTRT(IW)
          IY1=IYSTRT(IW)
          IX2=IX1+2047 
          IY2=IY1+1535 
          CALL LLVWPT(IX1,IY1,IX2,IY2) 
          CALL LLWIND(0,0,4095,3130)
          CALL EKOKVI  
          CALL LLPAGE  
10    CONTINUE 
C  
      CALL CHOWIN(IWUSE)
C
99    RETURN
      END  
