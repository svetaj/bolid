      PROGRAM DRAFTINI
C=================================================================
C1  INICIJALIZACIJA DATOTEKA ZA BOLID  VERZIJA 2.0
C1  MAKROI - BOLIDDRF, BOLIDDRX
C
C   IZLAZ:
C
C       BAZA PODATAKA MAKROA
C-----------------------------------------------------------------
C $Header: draftini.f,v 1.3 90/02/05 13:19:15 sveta Exp $
C $Log:	draftini.f,v $
C---> Revision 1.3  90/02/05  13:19:15  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  88/04/05  12:45:21  vera
C---> WRITE --->  PORUKA!!!
C---> 
C---> Revision 1.1  87/04/09  08:02:22  joca
C---> Initial revision
C---> 
C=================================================================
C
      INTEGER*4 MASTAC(280)
      CHARACTER*12 IMEMAT,IMEMAX
C
C******************************
C* BROJ REZERVISANIH RECORD-A *
C*                            *
      IBUSY=2
C******************************
      IMEMAT='BOLIDDRF'
      IMEMAX='BOLIDDRX'
      IRECL=280
      LUNMAT=17
      LUNMAX=18
      NXMA=56
      NBMA=280
C
      CALL OPEND(LUNMAT,IMEMAT,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 1
      CALL PORUKA(517,1)
      CALL PRTOUT(3,12,0,IMEMAT,1)
      CALL PORUKA(518,1)
      CALL PRTOUT(1,ISTAT,0,' ',0)
C
1     CALL OPEND(LUNMAX,IMEMAX,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 2
      CALL PORUKA(517,1)
      CALL PRTOUT(3,12,0,IMEMAX,0)
      CALL PORUKA(318,1)
      CALL PRTOUT(1,ISTAT,0,' ',0)
C
2     CONTINUE
      DO 3 I=1,NBMA
3     MASTAC(I)=0
C
      DO 20 I=1,IBUSY+1
          CALL WRITED(LUNMAT,I,MASTAC)
20    CONTINUE
C
      MASTAC(2)=IBUSY+1
      CALL WRITED(LUNMAX,1,MASTAC)
C
      MASTAC(2)=0
      CALL WRITED(LUNMAX,2,MASTAC)
C
      CALL CLOSED(LUNMAT)
      CALL CLOSED(LUNMAX)
C
      STOP
      END
