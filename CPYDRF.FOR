      SUBROUTINE CPYDRF
C==================================================================
C1  VRSI KOPIRANJE SELEKTIRANIH PRIMITIVA UZ POMERANJE NA ZELJENO 
C1  MESTO 
C
C   ULAZ:
C
C      KOMONI GRPTMP, COMDRF
C
C   IZLAZ:
C
C      KOMON COMDRF
C      EKRAN
C------------------------------------------------------------------
C $Header: cpydrf.f,v 1.1 90/04/20 12:55:41 sveta Exp $
C $Log:	cpydrf.f,v $
C---> Revision 1.1  90/04/20  12:55:41  sveta
C---> Initial revision
C---> 
C==================================================================
C
      INCLUDE 'DRFBUF.COM'
      INCLUDE 'GRPTMP.COM'
      INCLUDE 'EROR.COM'
C
      IF(IERR.NE.0)GOTO 99
      IF(NOPG.EQ.0)GOTO 99
C
C KOPIRANJE SELEKTIRANIH PRIMITIVA NA KRAJ TABELE
C
1     CALL COPENT
      IF(IERR.NE.0)GOTO 99
C 
C POMERANJE SELEKTIRANIH PRIMITIVA
C
      CALL MOVEL
      IF(IERR.NE.0)GOTO 99
      IF(IND.EQ.0)GOTO 1
C
99    RETURN
      END
