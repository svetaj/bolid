      SUBROUTINE DRFTRM(IE,X1,Y1,XINT,YINT,IPP)
C=====================================================================
C1       ***  TRIMOVANJE PRIMITIVA  ***
C1   PRONALAZI KOJA TACKA TREBA DA SE PROMENI
C
C  ULAZNE PROMENLJIVE:
C
C    IE               = REDNI BROJ PRIMITIVE
C    (X1,Y1)          = TACKA BLISKA TACKI KOJA SE MENJA             
C    (XINT,YINT)      = NOVA TACKA
C
C  IZLAZNE PROMENLJIVE:
C
C    IPP              = POINTER OD KOGA SE MENJA TACKA
C---------------------------------------------------------------------
C $Header: drftrm.f,v 1.3 90/02/05 14:29:31 sveta Exp $
C $Log:	drftrm.f,v $
C---> Revision 1.3  90/02/05  14:29:31  sveta
C---> izmenjeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  14:56:11  sveta
C---> dodat indikator u listu argumenata
C---> 
C---> Revision 1.1  88/10/13  13:04:58  sveta
C---> Initial revision
C---> 
C=====================================================================
C
      INCLUDE 'COMDRF.COM'
      DIMENSION ARG(200)
C
      IT=IELTYP(IE)
      IP=IELPOI(IE)
      IR=IELNR(IE)
      DO 1 I=1,IR
         ARG(I)=ARGEL(IP-1+I)
1     CONTINUE
C
      GOTO(10,20,30,40,50,60,70,80,90,100,110,120,130,140,150)IT
C
C  LINE
C
10    CONTINUE
      CALL DIST(X1,Y1,ARG(1),ARG(2),D1)
      CALL DIST(X1,Y1,ARG(3),ARG(4),D2)
      IF(D1.LT.D2)THEN
          IPP=IP
      ELSE
          IPP=IP+2
      ENDIF
      GOTO 99
C
C  POLY
C
20    CONTINUE
      GOTO 99
C
C  SPLINE
C
30    CONTINUE
      GOTO 99
C
C  CIRCLE
C
40    CONTINUE
      GOTO 99
C
C  ARC
C
50    CONTINUE
      CALL DIST(X1,Y1,ARG(3),ARG(4),D1)
      CALL DIST(X1,Y1,ARG(5),ARG(6),D2)
      IF(D1.LT.D2)THEN
          IPP=IP+2
      ELSE
          IPP=IP+4
      ENDIF
      GOTO 99
C
C  ELLIPSE
C
60    CONTINUE
      GOTO 99
C
C  HYPERBOLA
C
70    CONTINUE
      GOTO 99
C
C  PARABOLA
C
80    CONTINUE
      GOTO 99
C
C  BOX
C
90    CONTINUE
      GOTO 99
C
C  POINT
C
100   CONTINUE
      GOTO 99
C
C  TEXT
C
110   CONTINUE
      GOTO 99
C
C  DIMENSION
C
120   CONTINUE
      GOTO 99
C
C  FILL AREA
C
130   CONTINUE
      GOTO 99
C
C  MACRO
C
140   CONTINUE
      GOTO 99
C
C  NET
C
150   CONTINUE
      GOTO 99
C
99    RETURN
      END
