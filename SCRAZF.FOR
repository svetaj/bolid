      SUBROUTINE SCRAZF(IE)
C=================================================================
C1    ISPITUJE DA LI JE PRIMITIVA BILA SA FILL-OM
C1    I AZURIRA CRTEZ (SVE OD BF DO EF)
C1    NE KORISTI SE ZATO STO SE I BF I EF NE KORISTI
C
C  ULAZNE PROMENLJIVE:
C
C     IE      = REDNI BROJ PRIMITIVE
C
C  IZLAZ:
C
C     EKRAN
C
C  POZIVI PODPROGRAMA:
C
C     SCRAZU  = CRTA JEDNU PRIMITIVU 
C-----------------------------------------------------------------
C $Header: scrazf.f,v 1.3 90/02/12 12:00:56 sveta Exp $
C $Log:	scrazf.f,v $
C---> Revision 1.3  90/02/12  12:00:56  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  87/11/12  11:56:27  sveta
C---> ??
C---> 
C---> Revision 1.1  87/10/05  11:50:16  sveta
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDRF.COM'
C
C     TRAZENJE BEGIN FILL AREA I END FILL AREA U TABELI
C
      IF(IELTYP(IE).EQ.14)THEN
         CALL SCRAZU(IE)
         GOTO 99
      ENDIF
C
      DO 10 I=IE+1,IELI
         IF(IELTYP(I).EQ.13)THEN
            I2=I
            GO TO 20
         ENDIF
10    CONTINUE
      GO TO 50
C
20    DO 30 I=IE-1,1,-1
         IF(IELTYP(I).EQ.13)THEN
            I1=I
            GO TO 40
         ENDIF
30    CONTINUE
      GO TO 50
C
40    IF(IELCOL(I1).EQ.1.AND.IELCOL(I2).EQ.2) GO TO 60
50    CALL SCRAZU(IE)
      GO TO 99
C
60    DO 70 I=I1,I2
         CALL SCRAZU(I)
70    CONTINUE
C
99    RETURN
      END 
