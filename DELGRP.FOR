      SUBROUTINE DELGRP(INDT)
C==============================================================
C1    SUBROUTINE TO DELETE A GROUP
C
C   ULAZNE PROMENLJIVE:
C
C     INDT     = 0 - IME SE UNOSI PREKO TASTATURE
C                1 - IME JE U ID-U
C   ULAZ:
C
C     USER INTERFACE
C     KOMON COMVAR
C
C   IZLAZ:
C
C     KOMON COMGRP, GRPINF
C--------------------------------------------------------------
C $Header: delgrp.f,v 1.7 90/02/05 10:22:20 sveta Exp $
C $Log:	delgrp.f,v $
C---> Revision 1.7  90/02/05  10:22:20  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.6  90/01/29  14:51:38  sveta
C---> ??
C---> 
C---> Revision 1.5  88/04/11  11:34:42  vera
C---> MSG --->  PORUKA!!!
C---> 
C---> Revision 1.4  88/04/07  12:47:02  vera
C---> WRITE ---> PORUKA!!!
C---> 
C---> Revision 1.3  88/04/06  10:49:16  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.2  88/04/05  12:44:10  vera
C---> WRITE --->  PORUKA!!!
C---> 
C---> Revision 1.1  88/02/01  13:43:05  sveta
C---> Initial revision
C---> 
C==============================================================
C
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMGRP.COM'
      INCLUDE 'GRPINF.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'COMMSG.COM'
C
      IGRP=INDT
      IF(IGRP.NE.0)THEN
         GOTO 60
      ELSE
         GOTO 51
      ENDIF
C
C     INTERACTIVE PART
50    CONTINUE
      CALL PORUKA(598,0)
      CALL READL1
51    CALL VAR
      IF(ITIP.EQ.0)GOTO 50
      IF(ITIP.EQ.4)GOTO 51
      IF(ITIP.NE.1)GOTO 50
      IF(ID(1).EQ.LE)GOTO 99
      IF(ID(1).EQ.LH)GOTO 80
      IF(ID(1)//ID(2)//ID(3).EQ.'DEFAULT     ')THEN
          IGRP=NGRP
      ELSE
          CALL FGID(IGRP)
          IF(IGRP.EQ.0)GOTO 50
      ENDIF
C
C     ACTUAL DELETION
C
60    CONTINUE
      CALL BDDEL(LGROUP,NGRP,LGASG,NGA,IGRP)
      DO 61 K=IGRP,NGRP
      DO 61 J=1,3
      IDGRP(J,K)=IDGRP(J,K+1)
61    CONTINUE
      IF(INDT.EQ.0)THEN
        CALL PRTOUT(3,4,0,ID,1)
        CALL PORUKA(514,0)
      ENDIF
      GOTO 99
C HELP
80    CONTINUE
      CALL PORUKA(213,0)
      CALL PRTGRP
      GOTO 50
99    CONTINUE
      RETURN
      END
