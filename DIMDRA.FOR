      SUBROUTINE DIMDRA(IANGUL,IAFON1,IAFON2,IKFONT,ITFONT,SX,SY,HC,
     *                  X1,Y1,X2,Y2,XK,YK,DU)
C======================================================================
C1   CRTA KOMPLETNU KOTU (ARHITEKTONSKO DIMENZIONISANJE)
C
C ULAZNE PROMENLJIVE:
C
C   IANGUL            =  INDIKATOR KAKO DA SE TUMACE OSTALI ARGUMENTI
C                        0 --> LINEARNO  ;  1 --> UGLOVNO KOTIRANJE
C   IAFON1            =  FONT LEVE STRELICE
C   IAFON2            =  FONT DESNE STRELICE
C   IKFONT            =  FONT KOTNE LINIJE
C   ITFONT            =  FONT TEKSTA
C   SX,SY             =  PARAMETRI STRELICE
C   HC                =  VISINA TEKSTA
C   (X1,Y1), (X2,Y2)  =  REFERENTNE KOTNE TACKE
C   DU                =  UDALJENOST KOTNE LINIJE OD REFERENTNIH
C                        TACAKA
C   (XK,YK)           =  MESTO ISPISIVANJA TEKSTA
C
C IZLAZ:
C
C   EKRAN, PLOTER
C
C POZIVI PODPROGRAMA:
C
C   DIMLIN            =  CRTA KOTNU LINIJU
C   ARROW             =  CRTA STRELICU
C   DIMTEX            =  ISPISUJE TEKST
C   DIST              =  RACUNA RASTOJANJE IZMEDJU 2 TACKE
C   LIND              =  RACUNA 2 DUZI SA 1 ZAJEDNICKIM TEMENOM
C   ANG2D             =  RACUNA UGAO DUZI SA X-OSOM
C-----------------------------------------------------------------------
C $Header: dimdra.f,v 1.5 90/02/05 12:12:29 sveta Exp $
C $Log:	dimdra.f,v $
C---> Revision 1.5  90/02/05  12:12:29  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.4  88/02/01  13:43:09  sveta
C---> ??
C---> 
C---> Revision 1.3  87/06/16  09:34:21  sveta
C---> DODATO UGAONO KOTIRANJE
C---> 
C---> Revision 1.2  87/05/27  10:33:58  sveta
C---> NOVA VERZIJA GRAFICKE RUTINE ZA KOTIRANJE
C---> KOMBINOVANJEM RAZLICITIH FONTOVA STRELICA, KOTNIH LINIJA I
C---> TEKSTA , KORISNIK SAM DEFINISE TIP KOTIRANJA
C---> 
C=======================================================================
C
      DATA PI/3.1415926/
C
      IF(IANGUL.EQ.0)THEN
C
C##################### LINEARNO KOTIRANJE ##########################
C
C-------------------------------- IZRACUNAVANJE POTREBNIH PARAMETARA
          CALL ANG2D(X1,Y1,X2,Y2,ALFA)
          CALL LIND(X1,Y1,X2,Y2,DU,90.,XA,YA,IND)
          CALL LIND(X2,Y2,X1,Y1,DU,270.,XB,YB,IND)
C------------------------------------------------------ KOTNA LINIJA
          CALL DIMLIN(IKFONT,X1,Y1,X2,Y2,DU,XA,YA,XB,YB)
C---------------------------------------------------------- STRELICE
          CALL ARROW(IAFON1,SX,SY,XA,YA,ALFA)
          CALL ARROW(IAFON2,SX,SY,XB,YB,ALFA+180.)
C------------------------------------------------------------- TEKST
          CALL DIST(X1,Y1,X2,Y2,VAL)
          IF(ITFONT.LT.0)CALL ANG2D(X2,Y2,X1,Y1,ALFA)
          ITF=ABS(ITFONT)
          CALL DIMTEX(ITF,XK,YK,ALFA,HC,VAL)
      ELSE
C
C##################### UGLOVNO KOTIRANJE ###########################
C
C----------------------------------- IZRACUNAVANJE POLOZAJA STRELICA
          CALL DIST(X1,Y1,X2,Y2,R)
          CALL LIND(X1,Y1,X2,Y2,R,DU,X3,Y3,IND)
          CALL LIND(X3,Y3,X1,Y1,R,90.,X4,Y4,IND)
          CALL ANG2D(X3,Y3,X4,Y4,ALFA1)
          CALL LIND(X2,Y2,X1,Y1,R,270.,X5,Y5,IND)
          CALL ANG2D(X2,Y2,X5,Y5,ALFA2)
C---------------------------------------------------------- STRELICE
          CALL ARROW(IAFON1,SX,SY,X3,Y3,ALFA1)
          CALL ARROW(IAFON2,SX,SY,X2,Y2,ALFA2)
C------------------------------------------------------ KOTNA LINIJA
          IKF=IKFONT+4
          IF(IKFONT.EQ.0)IKF=0
          CALL DIMLIN(IKF,X1,Y1,X2,Y2,DU,X3,Y3,XB,YB)
C------------------------------------------------------------- TEKST
          IF(ITFONT.LT.0)CALL ANG2D(X2,Y2,X3,Y3,ANGLE)
          IF(ITFONT.GT.0)CALL ANG2D(X3,Y3,X2,Y2,ANGLE)
          ITF=ABS(ITFONT)
          CALL DIMTEX(ITF,XK,YK,ANGLE,HC,DU)
      ENDIF
C
      RETURN
      END
