      SUBROUTINE KILLEL(IE)
C=================================================================
C1  BRISE ELEMENT IZ 2-D SCENE  ( I MAKROE)
C
C  ULAZNE PROMENLJIVE:
C
C      IE      = REDNI BROJ PRIMITIVE U TABELI
C
C  IZLAZ:
C
C      KOMON COMDRF
C
C  POZIVI PODPROGRAMA:
C
C      KILL1   = BRISE 1 PRIMITIVU
C      KILMAC  = BRISE MAKRO
C-----------------------------------------------------------------
C $Header: killel.f,v 1.6 90/02/07 12:21:14 sveta Exp $
C $Log:	killel.f,v $
C---> Revision 1.6  90/02/07  12:21:14  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.5  87/11/19  17:07:32  sveta
C---> ??
C---> 
C---> Revision 1.4  87/10/05  11:46:19  sveta
C---> ???
C---> 
C---> Revision 1.3  87/06/29  06:56:39  sveta
C---> ????
C---> 
C---> Revision 1.2  87/05/25  13:55:50  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  10:58:37  joca
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMWIN.COM'
C
      IWW=1
      IF(IWTEK.GT.1)IWW=2
C
C RESET CELE TABELE ILI CELOG LAYER-A
C
      IF(IE.LT.0)THEN
          IF(IWW.EQ.1)THEN
              IELI=0
              IARGI=0
              ITXI=0
              IMAC=0
          ELSE
              DO 5 I=IELI,1,-1
                  IF(IELLAY(I).EQ.ILAY)CALL KILL1(I)
5             CONTINUE
          ENDIF
      ELSE
C  
C BRISE 1 ELEMENT IZ TABELE
C AKO JE MAKRO PRVO BRISE ELEMENTE KOJI MU PRIPADAJU
C  
          IF(IELTYP(IE).EQ.14)THEN 
              CALL KILMAC(IE)
          ELSE
              CALL KILL1(IE)
          ENDIF
      ENDIF
C  
      RETURN
      END  
