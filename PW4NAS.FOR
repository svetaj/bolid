      SUBROUTINE PW4NAS(WAS,NCH)
C====================================================================
C1  PUT WORD (CHAR*4)(NCH) NUM. ASTERIX
C1  PAKUJE IZ ULAZNE PROMENJIVE U WORDAS I POZIVA 
C1  RUTINU NUMAST KOJA SETUJE INDAS
C
C  ULAZNE PROMENLJIVE:
C
C     WAS    = 
C     NCH    = DUZINA WAS
C  
C  POZIVI PODPROGRAMA:
C
C     NUMAST =
C--------------------------------------------------------------------
C $Header: pw4nas.f,v 1.2 90/02/12 10:07:55 sveta Exp $
C $Log:	pw4nas.f,v $
C---> Revision 1.2  90/02/12  10:07:55  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/05/08  15:22:01  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMAST.COM'
      CHARACTER*4 WAS(*)
C
C   PAKOVANJE U WORDAS
C
      NCHARA=0
      IF(NCH.GT.20) NCH=20
      DO 10 I=1,NCH
         DO 20 J=1,4
            IJ=(I-1)*4+J
            WORDAS(IJ)=WAS(I)(J:J)
            IF(WORDAS(IJ).EQ.' ') THEN
               NCHARA=IJ-1
               GO TO 30
            ENDIF
20       CONTINUE
10    CONTINUE
      NCHARA=NCH*4
30    CONTINUE
C
C   BROJANJE DELOVA RECI IZMEDJU ZVEZDICA
C
      CALL NUMAST
      RETURN
      END
