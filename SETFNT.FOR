      SUBROUTINE SETFNT
C=========================================================
C1 UCITAVA SA FAJLA DATI FONT I VRSI DOWNLOAD NA DATI 
C1 UREDJAJ - EKRAN/PLOTER
C
C  ULAZ:
C
C     USER INTERFACE
C
C  IZLAZ:
C
C     EKRAN, PLOTER
C
C  INTERNA PROMENLJIVA:
C 
C     IGX = 0  - SLOVA SE CRTAJU PROGRAMSKI
C     IGX = 1  - SETOVANJE DATOG FONTA NA TERMINALU
C     IGX = 2  - SETOVANJE DATOG FONTA NA PLOTERU
C---------------------------------------------------------
C $Header: setfnt.f,v 1.2 90/02/12 12:50:10 sveta Exp $
C $Log:	setfnt.f,v $
C---> Revision 1.2  90/02/12  12:50:10  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/06/07  08:38:07  sveta
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'DRFBUF.COM'
      INCLUDE 'CFIND.COM'
      INCLUDE 'COMMAF.COM'
      INCLUDE 'COMLMF.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMDRM.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'PROTCT.COM'
      INCLUDE 'EROR.COM'
      INCLUDE 'COMGO.COM'
      INCLUDE 'FNTTAB.COM'
      INCLUDE 'COMSLO.COM'
      DIMENSION RMAF(5,56) 
      DIMENSION IA(2),IBUFF(1000),XBUFF(1)
      EQUIVALENCE (MAF(1,1),RMAF(1,1)) 
      DATA IA(1)/68/,IA(2)/76/
C  
C OTVARANJE DRF-FAJLOVA
C
      IRDO=1
      CALL OPNFNT
      IF(IERR.NE.0)GOTO 99
C
1     CALL VAR 
      IF(ITIP.NE.0) GOTO 12
11    CALL PORUKA(406,0)
      CALL READL1
      GOTO 1
C
12    IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LH) GOTO 50  
         IF(ID(1).EQ.LE) GOTO 98  
      ENDIF
      IF(ITIP.NE.2)GOTO 11
      IF(INUM.LE.O)GOTO 11
      ID(1)='TEXT'
      ID(2)='FONT'
      CALL INTCHR(INUM,ID(3),2)
      IFONT=INUM
C
      CALL FNDDRF(LENMA)
      IF(LENMA.EQ.0)THEN
          CALL PORUKA(422,1)
          CALL PRTOUT(3,1,0.,' ',1)
          CALL PRTOUT(3,12,0.,ID,0)
          GOTO 98  
      ENDIF
C  
C   START READING DRF-MACRO
C  
      CALL SREDRF  
C
C============================================================
C
C  DA LI JE USER-DEFINED FONT SETOVAN ?
C
      IGX=IGG
      IF(IUDFNT.EQ.0)IGX=0
C
      IF(IGX.EQ.0)THEN
          FACFNT=1.3
          LSTTRG=0
          DO 13 IJ=1,MAKTSG
             ITASG(1,IJ)=0
             ITASG(2,IJ)=0
13        CONTINUE
      ELSE IF(IGX.EQ.1)THEN
          CALL LLDGCH(IFONT,-1)
          CALL LLGGRD(IFONT,MAF(4,KR),MAF(5,KR)*2)
      ELSE IF(IGX.EQ.2)THEN
          CALL PLOTS(0,0,11)
          CALL BUFF(1,IA,XBUFF,-2)
      ENDIF
C
      IFIRST=0
20    II=MAF(1,KR)
      IF(II.EQ.0)THEN
          IF(IGX.EQ.0)THEN
              ITASG(1,IADRR)=LSTOLD+1
              ITASG(2,IADRR)=LSTTRG-LSTOLD
          ENDIF
          IF(IGX.EQ.1.AND.IFIRST.GT.0)CALL LLEGCH
          IF(IGX.EQ.2.AND.IFIRST.GT.0)THEN
              CALL BUFF(4,IBUFF,XBUFF,ICOUN)
              CALL PLOT(0.,0.,999)
          ENDIF
          IF(IGX.EQ.1)CALL LLGFNT(IFONT)
          GOTO 98
      ENDIF
      IF(II.GT.0)THEN
C          PRINT *,'****************************************************'
C          PRINT *,' KARAKTER: ',CHAR(II)
C          PRINT *,' GABARITI: ',MAF(2,KR),MAF(3,KR),MAF(4,KR),MAF(5,KR)
          IF(IGX.EQ.0)THEN
              IF(IFIRST.GT.0)THEN
                 ITASG(1,IADRR)=LSTOLD+1
                 ITASG(2,IADRR)=LSTTRG-LSTOLD
              ENDIF
              IADRR=II-31
              XGAB=FLOAT(MAF(4,KR))*FACFNT
              YGAB=FLOAT(MAF(5,KR))
              LSTOLD=LSTTRG
              IFIRST=1
          ELSE IF(IGX.EQ.1)THEN
              IF(IFIRST.GT.0)CALL LLEGCH
              CALL LLDGCH(IFONT,II)
              CALL LLPVSG(0,0)
              CALL LLBGCH(IFONT,II)
              IFIRST=1
          ELSE IF(IGX.EQ.2)THEN
              IF(IFIRST.GT.0)CALL BUFF(4,IBUFF,XBUFF,ICOUN)
C              PRINT *,' ICOUN=',ICOUN
              IBUFF(1)=II
              ICOUN=1
              CALL BUFF(1,IA,XBUFF,2)
              IFIRST=1
          ENDIF
      ENDIF
      IF(II.LT.0)THEN
C          PRINT *,' LINIJA: ',MAF(2,KR),MAF(3,KR),MAF(4,KR),
C     *                        MAF(5,KR)
          IF(IGX.EQ.0)THEN
              IF(LSTTRG+4.GT.MAXTRG)THEN
                  CALL PORUKA(427,0)
                  GOTO 99
              ENDIF
              TARG(LSTTRG+1)=FLOAT(MAF(2,KR))/XGAB
              TARG(LSTTRG+2)=FLOAT(MAF(3,KR))/YGAB
              TARG(LSTTRG+3)=FLOAT(MAF(4,KR))/XGAB
              TARG(LSTTRG+4)=FLOAT(MAF(5,KR))/YGAB
              LSTTRG=LSTTRG+4
          ELSE IF(IGX.EQ.1)THEN
              CALL LLMOVE(MAF(2,KR),MAF(3,KR)*2)
              CALL LLDRAW(MAF(4,KR),MAF(5,KR)*2)
          ELSE IF(IGX.EQ.2)THEN
              IBUFF(ICOUN+1)=-128
              IBUFF(ICOUN+2)=MAF(2,KR)
              IBUFF(ICOUN+3)=MAF(3,KR)
              IBUFF(ICOUN+4)=MAF(4,KR)
              IBUFF(ICOUN+5)=MAF(5,KR)
              ICOUN=ICOUN+5
          ENDIF
      ENDIF
      CALL TEGDRF
      GOTO 20
C============================================================
C
C     HELP 
C
50    CONTINUE 
      CALL CLSDRF
      CALL PRTFNT  
      CALL OPNFNT
      GOTO 11  
C  
C ZATVARANJE DRF-FAJLOVA
C
98    CALL CLSDRF
C
99    RETURN
      END  
