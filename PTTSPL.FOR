      SUBROUTINE PTTSPL(IRB,X,Y,IND)
C======================================================================
C1 NALAZENJE TEST TACAKA ZA PRIMITIVU POLYLINE.
C1 TEST TACKE SU TEMENA IZLOMLJENE LINIJE, 
C1 KAO I SREDINE SEGMENATA.
C
C  ULAZNE PROMENLJIVE:
C
C    IRB = REDNI BROJ PRIMITIVE IZ TABELE
C
C  IZLAZNE PROMENLJIVE:
C
C    X,Y = KOORDINATE TEST TACAKA 
C    IND = BROJ TEST TACAKA 
C--------------------------------------------------------------------
C $Header: pttspl.f,v 1.3 90/02/12 08:49:27 sveta Exp $
C $Log:	pttspl.f,v $
C---> Revision 1.3  90/02/12  08:49:27  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  89/12/06  12:50:59  sveta
C---> ??
C---> 
C---> revision 1.1  89/10/11  11:49:47  sveta
C---> initial revision
C---> 
C======================================================================
C
      INCLUDE 'COMDRF.COM'
      REAL X(*),Y(*)
C
      IND=0
      IT=IELTYP(IRB)
      IF(IT.NE.2)GOTO 99
C
C POCETNI CLAN VEKTORA ARGEL  
C
      IP=IELPOI(IRB)
C
C ODREDJIVANJE BROJA PAROVA (X,Y) KOORDINATA TEMENA
C
      NBROJ=IELNR(IRB)/2
C
C ODREDJIVANJE TEST TACAKA
C
      DO 50 I=1,NBROJ*2-1,2
C
C PUNJENJE POZNATIH KOORDINATA
C
         X(I)=ARGEL(IP)
         Y(I)=ARGEL(IP+1)
         IP=IP+2
50    CONTINUE
C
C ODREDJIVANJE SREDISNJE TEST TACKE
C
      DO 100 I=2,NBROJ*2-1,2
          X(I)=(X(I-1)+X(I+1))/2
          Y(I)=(Y(I-1)+Y(I+1))/2
100   CONTINUE
C
C VRACANJE INDIKATORA (BROJ TP-A)
C
      IND=NBROJ*2-1
C
99    RETURN
      END
