      PROGRAM INISAV
C======================================================================
C1  GLAVNI PROGRAM
C1  INICIJALIZACIJA SOLIDSAV I SOLIDSVX DATOTEKA
C
C   IZLAZ:
C
C      SAVE BAZA PODATAKA
C
C   OSTALE PROMENLJIVE:
C
C      NMAE     = NUMBER OF ENTRIES IN X-FILE
C      ISEQMA   = NUMBER OF RECORD STORED IN IPRMAX
C      NAWMA    = LAST OCCUPIED RECORD IN T-FILE
C      LUNMAT   = LOGICAL UNIT OF T-FILE
C      LUNMAX   = LOGICAL UNIT OF X-FILE
C      NENMA    = POINTER TO AN ENTRY IN IPRMAX BUFFER
C      NXMA     = DIMENSION OF X RECORD
C      NBMA     = DIMENSION OF T RECORD
C      IPRMAX   = RECORD FROM X-FILE
C----------------------------------------------------------------------
C $Header: inisav.f,v 1.2 90/02/07 10:29:05 sveta Exp $
C $Log:	inisav.f,v $
C---> Revision 1.2  90/02/07  10:29:05  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/12/06  12:31:47  sveta
C---> Initial revision
C---> 
C---> Revision 1.2  88/02/08  17:51:43  joca
C---> INCLUDE je zamanjen COMMON-m
C---> 
C---> Revision 1.1  88/01/07  08:43:37  tanja
C---> Initial revision
C---> 
C======================================================================
C
      COMMON/COMMAX/ NMAE,IARMA,IAWMA,NAWMA,ISEQMA,LUNMAT,LUNMAX,
     *NENMA,NXMA,NBMA,IPRMAX(5,56)
C
C FREE STACKS FOR I/O OPERATIONS ON MAT FILES
C
      COMMON/COMSTA/NSTAMA,MASTAC(280),NSTAMX,MXSTAC(280)               
      CHARACTER*12 IMEMAT,IMEMAX
C
C******************************
C* BROJ REZERVISANIH RECORD-A *
C*                            *
      IBUSY=2
C******************************
C*******************************
C* REVIZIJA: 2.0.1             *
C*                             *
      IREV1=2
      IREV2=0
      IREV3=1
C*                             *
C*******************************
C
      IMEMAT='BOLIDSAV'
      IMEMAX='BOLIDSVX'
      IRECL=280
      LUNMAT=31
      LUNMAX=32
      NXMA=56
      NBMA=280
C
      CALL OPEND(LUNMAT,IMEMAT,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 1
      WRITE(6,10) IMEMAT,ISTAT
10    FORMAT(' ***ERROR IN: FILE ',A12,'.  STATUS:',I5)
C
1     CALL OPEND(LUNMAX,IMEMAX,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 2
      WRITE(6,10) IMEMAX,ISTAT
C
2     CONTINUE
      DO 3 I=1,NBMA
3     MASTAC(I)=0
C
      DO 20 I=1,IBUSY+1
          CALL WRITED(LUNMAT,I,MASTAC)
20    CONTINUE
C
      MASTAC(2)=IBUSY+1
      CALL WRITED(LUNMAX,1,MASTAC)
C
      MASTAC(2)=0
      CALL WRITED(LUNMAX,2,MASTAC)
C
C HEADER
C
      MASTAC(1)=IREV1
      MASTAC(2)=IREV2
      MASTAC(3)=IREV3
      CALL WRITED(LUNMAT,3,MASTAC)
C
      CALL CLOSED(LUNMAT)
      CALL CLOSED(LUNMAT)
      CALL CLOSED(LUNMAX)
C
      STOP
      END
