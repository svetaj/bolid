      SUBROUTINE NEWDRX(IE,IR,IEX,INDSTA)  
C=================================================================
C1  NA POZIV DODELJUJE SLOBODNI ENTRY U MAFX-FILE-U  
C1  NE KORISTI SE
C
C   ULAZ:
C
C     MAF BAZA PODATAKA
C
C   IZLAZNE PROMENLJIVE:
C
C     IR     - ADRESA MAFX-BUFFERA U MAFX-DATOTECI 
C     IE     - REDNI BROJ ENTRY-A U MAFX-BUFFERU
C     IEX    - REDNI BROJ ENTRY-A U MAFX-DATOTECI,
C              IEX=(IR-1)*NXMA+IE  
C     INDSTA - INDIKATOR DA LI JE ENTRY DODELJEN IZ STACKA 
C-----------------------------------------------------------------
C $Header: newdrx.f,v 1.3 90/02/15 11:41:14 sveta Exp $
C $Log:	newdrx.f,v $
C---> Revision 1.3  90/02/15  11:41:14  sveta
C---> iz solida - zbog free rekorda
C---> 
C---> Revision 1.2  90/02/08  16:34:02  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  87/04/09  11:11:04  joca
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMSTF.COM'
      INCLUDE 'COMMFX.COM'
C
      IF(NSTAMX.NE.0)GOTO 10
C     NEMA OSLOBODJENIH ENTRY-A
      NMAE=NMAE+1  
      INDSTA=0 
      IEX=NMAE 
1     IR=(IEX-1)/NXMA+2
      IE=IEX-(IR-2)*NXMA
C      PRINT *,'IE,IR,IEX,NMAE',IE,IR,IEX,NMAE 
      GOTO 99  
C  
C     FIFO=STACK
C
10    IEX=MXSTAC(4)
      NSTAMX=NSTAMX-1  
C
C     SAZIMANJE
C
      DO 11 I=1,NSTAMX 
11    MXSTAC(I+3)=MXSTAC(I+4)  
      INDSTA=1 
      GOTO 1
C
99    RETURN
      END  
