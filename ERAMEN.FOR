      SUBROUTINE ERAMEN 
C=================================================================
C1  GLAVNI MENI ZA FUNKCIJU ERASE
C1  AKO SMO POZICIONIRANI U COMMON AREA BRISE U SVIM LAYER-IMA
C
C   ULAZ:
C
C     KOMON COMVAR
C     USER INTERFACE
C
C   IZLAZ:
C
C     EKRAN
C     KOMONI COMDRF, COMSEG
C
C   OPCIJE:
C
C     ENTITY  = PRIMITIVE 
C     SPECIAL =             SPECIJALNE SEGMENTE
C     ALL     = PRIMITIVE & SPECIJALNE SEGMENTE
C     FORCE   = SVE U SVIM LAYER-IMA
C-----------------------------------------------------------------
C $Header: eramen.f,v 1.1 90/03/21 11:39:35 sveta Exp $
C $Log:	eramen.f,v $
C---> Revision 1.1  90/03/21  11:39:35  sveta
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMVAR.COM'
C
      CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.'ENTI')IER=1
          IF(ID(1).EQ.'ALL ')IER=2
          IF(ID(1).EQ.'FORC')IER=3
          IF(ID(1).EQ.'SPEC')IER=4
          GOTO 10
      ENDIF
C
1     CALL WIDTH(10)
      PRINT *,'--------------------'
      PRINT *,' *** ERASE MENU *** '
      PRINT *,'--------------------'
      PRINT *,' 1. ENTITIES        '
      PRINT *,' 2. ALL LAYER       '
      PRINT *,' 3. FORCE           '
      PRINT *,' 4. SPECIAL SEGMENTS'
      PRINT *,'--------------------'
      PRINT *,' ENTER NUMBER       '
C
      CALL RINT1(1,4,IER,IND)
      IF(IND.EQ.-1)GOTO 99
      IF(IND.EQ.1)GOTO 1 
C
C  FUNKCIJA ERASE
C
10    CALL ERINI(IER)
C
99    RETURN
      END  
