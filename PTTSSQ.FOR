      SUBROUTINE PTTSSQ(IRB,X,Y,IND)
C=====================================================================
C1 ODREDJIVANJE TEST TACAKA KOD SQR SIMBOLA.
C1 DUMMY
C
C  ULAZNE PROMENLJIVE:
C
C    IRB = REDNI BROJ ENTITETA U TABELI
C
C  IZLAZNE PROMENLJIVE:
C
C    X,Y = KOORDINATE TEST TACAKA
C    IND = BROJ TEST TACAKA
C--------------------------------------------------------------------
C $Header: pttssq.f,v 1.2 90/02/12 08:49:29 sveta Exp $
C $Log:	pttssq.f,v $
C---> Revision 1.2  90/02/12  08:49:29  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/12/06  12:51:11  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMDRF.COM'
      REAL X(*),Y(*)
C
      IND=0
      IT=IELTYP(IRB)
      IF(IT.NE.19)GOTO 99
C
      IP=IELPOI(IRB)
C
99    RETURN
      END

