      SUBROUTINE HEADER
C===============================================================
C1  HEADER FOR PROGRAM BOLID
C
C   IZLAZ:
C
C      EKRAN
C---------------------------------------------------------------
C $Header: header.f,v 1.18 90/04/26 09:39:35 sveta Exp $
C $Log:	header.f,v $
C---> Revision 1.18  90/04/26  09:39:35  sveta
C---> nova verzija interpretera
C---> 
C---> Revision 1.17  90/04/20  12:59:20  sveta
C---> ??
C---> 
C---> Revision 1.16  90/03/21  11:41:51  sveta
C---> revizija 2.6.0
C---> 
C---> Revision 1.15  90/02/15  11:40:13  sveta
C---> nova verzija 2.5.2
C---> 
C---> Revision 1.14  90/02/07  09:31:54  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.13  90/01/29  15:03:26  sveta
C---> nova verzija 2.5.0
C---> 
C---> Revision 1.12  89/12/06  12:30:29  sveta
C---> nova verzija 2.4.4
C---> 
C---> Revision 1.11  89/08/29  11:33:44  sveta
C---> nova verzija 2.4.3
C---> 
C---> Revision 1.10  89/08/16  11:07:47  sveta
C---> nova vezija 2.4.2
C---> 
C---> Revision 1.9  89/06/05  12:07:50  sveta
C---> nova revizija 2.4.1 - CIRCLE - DRAGGING
C--->                     - ZASTITA - UBACENO SKREMBLOVANJE
C--->                     - CURSXY - NOVE OPCIJE : X,T,K
C---> 
C---> Revision 1.8  89/05/08  15:20:05  sveta
C---> BROJ VERZIJE SE SADA NALAZI U PODPROGRAMU
C---> 
C---> Revision 1.7  88/05/23  10:59:22  sveta
C---> ispravljen BUG
C---> 
C---> Revision 1.6  88/04/08  09:16:09  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.5  88/02/04  08:47:48  sveta
C---> ?
C---> 
C---> Revision 1.4  88/02/01  13:46:17  sveta
C---> ??
C---> 
C---> Revision 1.3  87/10/06  08:32:39  sveta
C---> ?
C---> 
C---> Revision 1.2  87/04/10  08:27:53  joca
C---> *** empty log message ***
C---> 
C---> Revision 1.1  87/04/09  10:51:47  joca
C---> Initial revision
C---> 
C===============================================================
C
      CHARACTER FF*24,FF1*50,VERZ*7
C
C######################################
C###### VERZIJA PROGRAMA ##############
      DATA VERZ/'2.6.2  '/
C######################################
C
      CALL FDATE(FF)
      FF1=' '
      FF1(27:50)=FF
      CALL NASLOV
      CALL PORUKA(303,0)
      CALL PORUKA(833,1)
      CALL PRTOUT(3,7,0,VERZ,0)
      CALL PORUKA(305,0)
      CALL PORUKA(306,0)
      CALL PRTOUT(3,50,0,FF1,0)
      CALL PORUKA(303,0)
      DO 10 II=1,11
         CALL PRTOUT(3,1,0,' ',0)
10    CONTINUE
C  
      RETURN
      END  
