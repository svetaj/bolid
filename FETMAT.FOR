      SUBROUTINE FETMAT
C======================================================================
C1  FETCH A LIST OF FREE RECORDS FROM THE LINKED CHAIN.
C1  FREE RECORD IS EMPTY AT THIS POINT
C----------------------------------------------------------------------
C $Header: fetmat.f,v 1.3 90/02/15 11:39:34 sveta Exp $
C $Log:	fetmat.f,v $
C---> Revision 1.3  90/02/15  11:39:34  sveta
C---> iz solida - zbog free rekorda
C---> 
C---> Revision 1.2  90/02/06  10:25:43  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/05/23  10:56:56  sveta
C---> Initial revision
C---> 
C---> Revision 1.1  88/02/24  06:58:11  joca
C---> Initial revision
C---> 
C======================================================================
C
      INCLUDE 'CONFIG.COM'
      INCLUDE 'COMMAX.COM'
      INCLUDE 'COMSTA.COM'
      INCLUDE 'TMPDUM.COM'
C
C VERIFY THAT LINK NUMBER IS OK ( MUST BE A VALID RECORD )
C
      IF (LASTRC.LE.0.OR.LASTRC.GT.NAWMA) GOTO 98
C
C GET POINTED RECORD
C
      CALL READD(LUNMAT,LASTRC,TEMP)
C
C VERIFY RECORD VALIDITY
C
      IF (TEMP(2).NE.IVALKY) GOTO 98
      IF (TEMP(3).NE.ISIGT ) GOTO 98
C
C CHAIN IS OK ! LOAD STACK FROM RECORD
C
      DO 10 I=1,NSMAXT
         MASTAC(I+1)=TEMP(I+3)
10    CONTINUE
      NSTAMA=NSMAXT
C
C RESET INDEX TO NEXT LINKED RECORD
C
      LASTRC=TEMP(1)
C
      GOTO 99
C----------------------------------
C INTERNAL ERROR : CHAIN CORRUPTION
C
98    CONTINUE
C      CALL PORUKA(1662,0)
      LASTRC=0
C
99    CONTINUE
      RETURN
      END
