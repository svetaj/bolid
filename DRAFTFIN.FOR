      PROGRAM DRAFTFINI
C=================================================================
C1   INICIJALIZACIJA DATOTEKA ZA BOLID  VERZIJA 2.0
C1   FONTOVI - BOLIDFNT, BOLIDFNX
C
C  IZLAZ:
C
C     BAZA PODATAKA FONTOVA
C-----------------------------------------------------------------
C $Header: draftfini.f,v 1.2 90/02/05 13:19:13 sveta Exp $
C $Log:	draftfini.f,v $
C---> Revision 1.2  90/02/05  13:19:13  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/06/07  08:33:13  sveta
C---> Initial revision
C---> 
C---> Revision 1.1  87/04/09  08:02:22  joca
C---> Initial revision
C---> 
C=================================================================
C
      INTEGER*4 MASTAC(280)
      CHARACTER*12 IMEMAT,IMEMAX
C
C******************************
C* BROJ REZERVISANIH RECORD-A *
C*                            *
      IBUSY=2
C******************************
      IMEMAT='BOLIDFNT'
      IMEMAX='BOLIDFNX'
      IRECL=280
      LUNMAT=17
      LUNMAX=18
      NXMA=56
      NBMA=280
C
      CALL OPEND(LUNMAT,IMEMAT,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 1
      WRITE(6,10) IMEMAT,ISTAT
10    FORMAT(' ***ERROR IN: FILE ',A12,'.  STATUS:',I5)
C
1     CALL OPEND(LUNMAX,IMEMAX,IRECL,ISTAT)
      IF(ISTAT.EQ.0) GOTO 2
      WRITE(6,10) IMEMAX,ISTAT
C
2     CONTINUE
      DO 3 I=1,NBMA
3     MASTAC(I)=0
C
      DO 20 I=1,IBUSY+1
          CALL WRITED(LUNMAT,I,MASTAC)
20    CONTINUE
C
      MASTAC(2)=IBUSY+1
      CALL WRITED(LUNMAX,1,MASTAC)
C
      MASTAC(2)=0
      CALL WRITED(LUNMAX,2,MASTAC)
C
      CALL CLOSED(LUNMAT)
      CALL CLOSED(LUNMAX)
C
      STOP
      END
