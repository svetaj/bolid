      SUBROUTINE PTTSLN(IRB,X,Y,IND)
C=====================================================================
C1  ODREDJIVANJE TEST TACAKA KOD PRIMITIVE LINE. TEST TACKE SU 
C1  KRAJNJE TACKE DUZI, KAO I SREDINA DUZI.
C
C  ULAZNE PROMENLJIVE:
C
C    IRB = REDNI BROJ ENTITETA U TABELI
C
C  IZLAZNE PROMENLJIVE:
C
C    X,Y = KOORDINATE TEST TACAKA
C    IND = BROJ TEST TACAKA
C--------------------------------------------------------------------
C $Header: pttsln.f,v 1.2 90/02/12 08:49:19 sveta Exp $
C $Log:	pttsln.f,v $
C---> Revision 1.2  90/02/12  08:49:19  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/12/06  12:50:06  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMDRF.COM'
      REAL X(*),Y(*)
C
      ITP=0
      IT=IELTYP(IRB)
      IF(IT.NE.1)GOTO 99
      IP=IELPOI(IRB)
C
      X(1)=ARGEL(IP)
      Y(1)=ARGEL(IP+1)
      X(2)=ARGEL(IP+2)
      Y(2)=ARGEL(IP+3)
C
      X(3)=(X(1)+X(2))/2
      Y(3)=(Y(1)+Y(2))/2
      IND=3
C
99    RETURN
      END
