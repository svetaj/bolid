      SUBROUTINE INTEND
C======================================================================
C1 END OF EXECUTION
C1 INTERPRET COMMAND : / END  /
C----------------------------------------------------------------------
C $Header: intend.f,v 1.1 90/04/26 09:39:50 sveta Exp $
C $Log:	intend.f,v $
C---> Revision 1.1  90/04/26  09:39:50  sveta
C---> Initial revision
C---> 
C======================================================================
C
      INCLUDE 'CONFIG.COM'
      INCLUDE 'COMFOR.COM'
      INCLUDE 'LINBR.COM'
      INCLUDE 'LIN80.COM'
      INCLUDE 'COMDO.COM'
C
C END OF FILE
C
      IF(IDOFLA.NE.0)THEN
          IEOF=-1
          L=LEND
      ENDIF
C    
C RESET POINTERS
C
      IDIP=0
      IDSUB=0
      NSUBS=0
      NLABS=0
C
      RETURN
      END
