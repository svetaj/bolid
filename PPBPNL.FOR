      SUBROUTINE PPBPNL(X,Y,IBOUND)
C=============================================================
C1 INTERNA EDGKS2 RUTINA
C1 ZAPOCINJE PANEL
C
C   ULAZNE PROMENLJIVE:
C
C     X,Y  = KOORDINATA POCETKA [cm]
C     IBOUND = DA LI SE CRTA OKVIR ILI NE
C              0 - NE
C              1 - DA
C
C   IZLAZ:
C
C     EKRAN, PLOTER
C-------------------------------------------------------------
C $Header: ppbpnl.f,v 1.2 90/02/09 09:37:45 sveta Exp $
C $Log:	ppbpnl.f,v $
C---> Revision 1.2  90/02/09  09:37:45  sveta
C---> ispravljeno zaglavlje
C---> 
C=============================================================
C
      COMMON/COMGO/ IGOUN,IG
      COMMON /GSPLT/FACTF,XT,YT,FACTM,FACTW,XS,YS,SCX,SCY
C 
      GOTO(10,20),IG
10    IX=IFIX(X*FACTW)
      IY=IFIX(Y*FACTW)
      CALL LLBPNL(IX,IY,IBOUND)
      GOTO 99
20    CALL HPBPNL(X,Y,IBOUND)
99    RETURN
      END
