      SUBROUTINE NET(XPOC,YPOC,ALF1,ALF2,N1UK,N2UK,N1,N2,AA,BB)
C==================================================================
C1  CRTA MREZU LINIJA ( U APS. JEDINICAMA)
C 
C ULAZNE PROMENLJIVE:
C
C   (XPOC,YPOC)    = DONJE LEVO TEME MREZE
C   ALF1           = UGAO IZMEDJU X-OSE I HORIZ. OSE (u stepenima)
C   ALF2           = UGAO IZMEDJU HOR. I VERT. OSE (u stepenima)
C   N1UK           = BROJ RAZLICITIH KORAKA PO HOR.OSI
C   N2UK           = BROJ RAZL. KORAKA PO VERTIKALNOJ OSI
C   N1             = VEKTOR KOJI SADRZI BROJ KORAKA PO HOR. OSI
C   N2             = VEKTOR KOJI SADRZI BROJ KORAKA PO VERT. OSI
C   AA             = VEKTOR SA KORACIMA PO HORIZONTALNOJ OSI
C   BB             = VEKTOR SA KORACIMA PO VERTIKALNOJ OSI
C
C IZLAZ:
C
C   EKRAN, PLOTER
C------------------------------------------------------------------
C $Header: net.f,v 1.4 90/02/08 15:49:40 sveta Exp $
C $Log:	net.f,v $
C---> Revision 1.4  90/02/08  15:49:40  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.3  87/10/23  18:18:24  sveta
C---> ??
C---> 
C---> Revision 1.2  87/10/20  19:32:46  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.1  87/10/20  19:04:46  sveta
C---> Initial revision
C---> 
C==================================================================
C 
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMGRI.COM'
      DIMENSION N1(1),N2(1),AA(1),BB(1)
      DATA PI/3.1415926/
C
      ALFA1=ALF1*PI/180.
      ALFA2=ALF2*PI/180.
C
C CRTANJE HORIZONTALNIH LINIJA
C
      X=XPOC
      Y=YPOC
      DIST1=0.
      DO 10 I=1,N1UK
         DIST1=DIST1+AA(I)*N1(I)
10    CONTINUE
C
      DO 30 J=1,N2UK
         D2=BB(J)
         DX=D2*COS(ALFA1+ALFA2)
         DY=D2*SIN(ALFA1+ALFA2)
         XX=DIST1*COS(ALFA1)+X
         YY=DIST1*SIN(ALFA1)+Y
C
         IF(J.EQ.1)THEN
            IPOC=0
         ELSE
            IPOC=1
         ENDIF
C
         DO 20 I=IPOC,N2(J)
            X1=X+DX*I
            Y1=Y+DY*I
            X2=XX+DX*I
            Y2=YY+DY*I
            CALL PLOT(X1*FS+X0,Y1*FS+Y0,3)
            CALL PLOT(X2*FS+X0,Y2*FS+Y0,2)
20       CONTINUE
         X=N2(J)*BB(J)*COS(ALFA1+ALFA2)+X
         Y=N2(J)*BB(J)*SIN(ALFA1+ALFA2)+Y
30    CONTINUE
C
C
C CRTANJE VERTIKALNIH LINIJA
C
      X=XPOC
      Y=YPOC
      DIST1=0.
      DO 100 I=1,N2UK
         DIST1=DIST1+BB(I)*N2(I)
100   CONTINUE
C
      DO 300 J=1,N1UK
         D2=AA(J)
         DX=D2*COS(ALFA1)
         DY=D2*SIN(ALFA1)
         XX=DIST1*COS(ALFA1+ALFA2)+X
         YY=DIST1*SIN(ALFA1+ALFA2)+Y
C
         IF(J.EQ.1)THEN
            IPOC=0
         ELSE
            IPOC=1
         ENDIF
C
         DO 200 I=IPOC,N1(J)
            X1=X+DX*I
            Y1=Y+DY*I
            X2=XX+DX*I
            Y2=YY+DY*I
            CALL PLOT(X1*FS+X0,Y1*FS+Y0,3)
            CALL PLOT(X2*FS+X0,Y2*FS+Y0,2)
200      CONTINUE
         X=N1(J)*AA(J)*COS(ALFA1)+X
         Y=N1(J)*AA(J)*SIN(ALFA1)+Y
300   CONTINUE
C
      RETURN
      END
