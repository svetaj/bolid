      SUBROUTINE NEWDRF(IR)
C=================================================================
C1   DODELI SLOBODNI MAF-BUFFER IZ MAF-DATOTEKE
C1   NE KORISTI SE
C
C   ULAZ:
C
C     MAF BAZA PODATAKA
C
C   IZLAZNE PROMENLJIVE:
C
C     IR   = REDNI BROJ DODELJENOG REKORDA
C-----------------------------------------------------------------
C $Header: newdrf.f,v 1.3 90/02/15 11:41:12 sveta Exp $
C $Log:	newdrf.f,v $
C---> Revision 1.3  90/02/15  11:41:12  sveta
C---> iz solida - zbog free rekorda
C---> 
C---> Revision 1.2  90/02/08  16:34:00  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  87/04/09  11:09:43  joca
C---> Initial revision
C---> 
C=================================================================
C  
      INCLUDE 'COMMFX.COM'
      INCLUDE 'COMSTF.COM'
      IF(NSTAMA.NE.0)GOTO 10
C
C     NEMA OBRISANIH OSLOBODJENIH BUFFERA  
C     DODELJUJE SE SLEDECI BUFFER PO REDU  
C
      NAWMA=NAWMA+1
      IR=NAWMA 
      GOTO 99  
C
C     IMA OSLOBODJENIH BUFFERA U STACKU
C     UZIMA SE PO FIFO PRINCIPU
C
10    IR=MASTAC(2) 
      NSTAMA=NSTAMA-1  
C     SAZIMANJE
      DO 11 I=1,NSTAMA 
11    MASTAC(I+1)=MASTAC(I+2)  
C  
99    RETURN
      END  
