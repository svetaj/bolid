      SUBROUTINE LOACOM
C=====================================================================  
C1 ISCITAVA VREDNOSTI ARGUMENATA COMMON-A LAYER , COMDIM , COMDIA
C1 IZ NEFORMATIZOVANOG FAJLA (STARI RESTORE)
C1 NE KORISTI SE 
C---------------------------------------------------------------------
C $Header: loacom.f,v 1.7 90/02/07 14:14:37 sveta Exp $
C $Log:	loacom.f,v $
C---> Revision 1.7  90/02/07  14:14:37  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.6  88/04/08  09:17:29  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.5  87/10/07  19:32:33  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.4  87/10/05  11:46:40  sveta
C---> ???
C---> 
C---> Revision 1.3  87/07/27  18:23:03  sveta
C---> Promenjeno ime fajla u saveIME
C---> 
C---> Revision 1.2  87/06/10  08:13:37  sveta
C---> UBACENO SPASAVANJE COMDIA I COMDIM
C---> 
C=====================================================================
C  
      INCLUDE 'COMDRF.COM'  
      INCLUDE 'COMDIA.COM'
      INCLUDE 'COMDIM.COM'
      INCLUDE 'COMPAS.COM' 
      INCLUDE 'COMCA.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'EROR.COM' 
      CHARACTER*12  STATVS 
      CHARACTER*12  FNAME,ZZZ
C  
      IERR=0
      CALL VAR
      IF(ITIP.EQ.1)THEN
         FNAME='save'//ID(1)//ID(2)//ID(3)
      ELSE
         FNAME='save'//USRIME  
      ENDIF
      ZZZ=FNAME
C
      STATVS='OLD' 
      IUNSAV=17
      OPEN(IUNSAV,FILE=FNAME,STATUS=STATVS,FORM='UNFORMATED',ERR=98) 
C
C   COMMON COMDRF  
C  
 10   CONTINUE 
      READ(IUNSAV)FU,IFU
      READ(IUNSAV)LAYER
      READ(IUNSAV)IDTMAX,IDFONT,DIMTYP,IDLAST,IDCURR,DIMPAR,IAFON1,
     *            IAFON2,IKFONT,ITFONT,SX,SY,HC,IAFMAX,IKFMAX,ITFMAX,
     *            IDANG,INAC,IANGUL
      READ(IUNSAV)IAAA,IBBB,ICCC,IDDD
      READ(IUNSAV)FSCA,X0CA,Y0CA,X0CAA,Y0CAA,X0CAB,Y0CAB,
     *            XGRCA,YGRCA,IXGRCA,IYGRCA,RXGRCA,RYGRCA,
     *            NRXCA,NRYCA,RAXCA,RAYCA
      CLOSE(IUNSAV)
      SAVIME=ZZZ
      INDIC=1
      GOTO 99  
C
98    CONTINUE 
      IERR=1
      CALL PORUKA(319,0)
      IF(INDIC.NE.1)SAVIME=' NOT LOADED'
C
99    RETURN
      END  
