      SUBROUTINE ARR1(SX,SY,XA,YA,ALFA)
C=================================================================
C1  CRTA KOTNU STRELICU :  <
C
C ULAZNE PROMENLJIVE:
C
C   SX,SY     =  PARAMETRI STRELICE
C   XA,YA     =  KOORDINATA STRELICE
C   ALFA      =  UGAO STRELICE
C
C IZLAZ:
C
C   EKRAN, PLOTER
C-----------------------------------------------------------------
C $Header: arr1.f,v 1.2 90/02/02 10:13:43 sveta Exp $
C $Log:	arr1.f,v $
C---> Revision 1.2  90/02/02  10:13:43  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  87/05/25  13:34:55  sveta
C---> Initial revision
C---> 
C=================================================================
C
      DATA PI/3.1415926/
C
      BETA=ATAN(SY/SX)
      ALFA1=ALFA*PI/180.
      R=SQRT(SX**2+SY**2)
      GAMA1=ALFA1+BETA
      GAMA2=ALFA1-BETA
C
      X1=XA+R*COS(GAMA1)
      Y1=YA+R*SIN(GAMA1)
      X2=XA+R*COS(GAMA2)
      Y2=YA+R*SIN(GAMA2)
C
      CALL MOVE(X1,Y1)
      CALL DRAW(XA,YA)
      CALL DRAW(X2,Y2)
C
      RETURN
      END
