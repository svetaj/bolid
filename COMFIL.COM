      COMMON /COMFIL/ IFA,IBFA,IEFA,ITX,ILIN,ISLIN,LRPEN,IGPRC,
     *                LRSTY,IMARK,IBRD,UDANG,UDDST,PLFAC,IALIND
C=====================================================================
C1  KOMON ZA CUVANJE DEFAULT VREDNOSTI BOJE, HATCH ITD...
C
C   OPIS PROMENLJIVIH:
C
C     IFA       = TIP FILL-AREA
C     IBFA      = NE KORISTI SE
C     IEFA      = NE KORISTI SE
C     ITX       = BOJA TEKSTA
C     ILIN      = BOJA LINIJA
C     ISLIN     = STIL LINIJA
C     LRPEN     = BOJA RASTERA
C     IGPRC     = STIL TEKSTA (STRING/STROKE)
C     LRSTY     = STIL RASTERA
C     IMARK     = TIP MARKERA
C     IBRD      = INDIKATOR BORDERA - HATCH
C     UDANG     = UGAO SRAFURE
C     UDDST     = RAZMAK SRAFURE
C     PLFAC     = PLOTER FACTOR ZA STIL LINIJE
C     IALIND    = ALTERNATIVNA BOJA (MDIM,SQR,FPT,IDS)
C---------------------------------------------------------------------
C $Header: COMFIL.COM,v 1.6 90/02/01 15:55:00 sveta Exp $
C $Log:	COMFIL.COM,v $
C---> Revision 1.6  90/02/01  15:55:00  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.5  89/12/06  11:20:56  sveta
C---> UBACEN ALTERNATIVNI INDEX BOJE LINIJE
C---> 
C---> Revision 1.4  89/10/11  10:56:29  sveta
C---> dodata promenljiva za setovanje sirine paterna isprekidane 
C---> linije na ploteru
C---> 
C---> Revision 1.3  89/06/21  11:34:08  sveta
C---> UBACEN INDIKATOR BORDERA, UGAO I RAZMAK SRAFURE.
C---> 
C---> Revision 1.2  87/09/09  13:51:25  sveta
C---> dodat tip markera
C---> 
C---> Revision 1.1  87/04/10  08:49:03  joca
C---> Initial revision
C---> 
C=====================================================================
