      SUBROUTINE PRTTAB(II)
C=================================================================
C1 INTERNA RUTINA ZA KONTROLNU STAMPU. STAMPA I POINTERE
C1 KOMANDA "TABLE DUMP"
C
C  ULAZNE PROMNELJIVE:
C
C    II   = REDNI BROJ PRIMITIVE
C
C  ULAZ:
C
C    KOMON COMDRF
C
C  IZLAZ:
C
C    EKRAN
C-----------------------------------------------------------------
C $Header: prttab.f,v 1.5 90/02/09 12:25:15 sveta Exp $
C $Log:	prttab.f,v $
C---> Revision 1.5  90/02/09  12:25:15  sveta
C---> ispravljeno zaglavlje
C---> 
C=================================================================
C
       INCLUDE 'COMDRF.COM'
C
       IF(II.EQ.0)THEN
           DO 10 I=1,IELI
               CALL PORUKA(344,1)
               CALL PRTOUT(1,I,0,' ',1)
               CALL PORUKA(345,0)
               CALL PORUKA(346,1)
               CALL PRTOUT(1,IELTYP(I),0,' ',1)
               CALL PRTOUT(1,IELPOI(I),0,' ',1)
               CALL PRTOUT(1,IELNR(I),0,' ',1)
               CALL PRTOUT(1,IELMA(I),0,' ',0)
               CALL PORUKA(347,1)
               CALL PRTOUT(1,IELCOL(I),0,' ',1)
               CALL PRTOUT(1,IELSTY(I),0,' ',1)
               CALL PRTOUT(1,IELLAY(I),0,' ',0)
               CALL PORUKA(348,1)
               CALL PRTOUT(1,IELSEG(1,I),0,' ',1)
               CALL PRTOUT(1,IELSEG(2,I),0,' ',0)
10          CONTINUE
      ELSE
          CALL PORUKA(344,1)
          CALL PRTOUT(1,II,0,' ',1)
          CALL PORUKA(345,0)
          CALL PORUKA(346,1)
          CALL PRTOUT(1,IELTYP(II),0,' ',1)
          CALL PRTOUT(1,IELPOI(II),0,' ',1)
          CALL PRTOUT(1,IELNR(II),0,' ',1)
          CALL PRTOUT(1,IELMA(II),0,' ',0)
          CALL PORUKA(347,1)
          CALL PRTOUT(1,IELCOL(II),0,' ',1)
          CALL PRTOUT(1,IELSTY(II),0,' ',1)
          CALL PRTOUT(1,IELLAY(II),0,' ',0)
          CALL PORUKA(348,1)
          CALL PRTOUT(1,IELSEG(1,II),0,' ',1)
          CALL PRTOUT(1,IELSEG(2,II),0,' ',0)
      ENDIF
C
      RETURN
      END
