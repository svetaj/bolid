      SUBROUTINE TRROT(X1,Y1,R,ALFA1,XP,YP,C,X2,Y2,ALFA2)
C===============================================================
C2 ZA ZADAT POLOZAJ LOKALNOG KOORDINATNOG SISTEMA POSLE
C2 TRANSLACIJE I ROTACIJE DATE MATRICOM TRANSFORMACIJE
C2 NALAZI NOVI POLOZAJ LOKALNOG KOORDINATNOG SISTEMA
C
C  ULAZNE PROMENLJIVE:
C
C     X1,Y1  - CENTAR LOKALNOG KOORDINATNOG SISTEMA
C     R      - DUMMY
C     ALFA1  - UGAO ROTACIJE LOKALNOG KOORDINATNOG SISTEMA
C     C      - MATRICA TRANSFORMACIJE  
C     XP,YP  - PIVOT-POINT (ZA ROTACIJU)
C
C  IZLAZNE PROMENLJIVE:
C
C     X2,Y2  - NOVI CENTAR POSLE TRANSLACIJE I ROTACIJE
C     ALFA2  - NOVI UGAO
C---------------------------------------------------------------
C $Header: trrot.f,v 1.5 90/02/13 11:56:43 sveta Exp $
C $Log:	trrot.f,v $
C---> Revision 1.5  90/02/13  11:56:43  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.4  88/02/11  12:02:04  sveta
C---> ?
C---> 
C---> Revision 1.3  87/12/04  15:46:45  sveta
C---> ??
C---> 
C---> Revision 1.2  87/05/25  14:02:06  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  12:04:04  joca
C---> Initial revision
C---> 
C===============================================================
C
      DIMENSION C(3,3) 
C  
      PI=3.1415926 
      AL1=PI/180.*ALFA1
      X11=X1+R*COS(AL1)
      Y11=Y1+R*SIN(AL1)
      CALL POTR2D(X1,Y1,XP,YP,C,X2,Y2) 
      CALL POTR2D(X11,Y11,XP,YP,C,X22,Y22) 
C  
      COS2=(X22-X2)/R  
      SIN2=(Y22-Y2)/R  
C  
      IF(ABS(SIN2).LE.0.5)THEN 
          WY0=AASIN(SIN2)
C  
C NALAZENJE 2 MOGUCA UGLA  
C  
          IF(WY0.GE.0) THEN
             WY1=WY0
             WY2=PI-WY0
          ELSE 
             WY1=PI-WY0
             WY2=2.*PI+WY0 
          ENDIF
          C1=COS(WY1)  
          C2=COS(WY2)  
C  
C ODABIRANJE PRAVOG UGLA
C  
          TEST=ABS(COS2-C1)
C  
      ELSE 
          WY0=ACOS(COS2)
C  
C  NALAZENJE 2 MOGUCA UGLA 
C  
          WY1=WY0  
          WY2=2.*PI-WY0
          S1=SIN(WY1)  
          S2=SIN(WY2)  
C  
C ODABIRANJE PRAVOG UGLA
C  
          TEST=ABS(SIN2-S1)
      ENDIF
      IF(TEST.LT.0.01) THEN
         ALFA2=WY1 
      ELSE 
         ALFA2=WY2 
      ENDIF
C  
      QQ1=180.*WY1/PI  
      QQ2=180.*WY2/PI  
C  
      ALFA2=180./PI*ALFA2  
      IF(ALFA2.GE.360.) ALFA2=ALFA2-360.
C  
      RETURN
      END  
