      SUBROUTINE MACMEN
C================================================================
C1  ODABIRANJE KOMANDE IZ MACRO MODA
C
C  ULAZ:
C
C    USER INTERFACE
C
C  POZIVI PODPROGRAMA:
C
C    RINT   = UCITAVA INTEGER
C    READL1 = READ (A80)
C----------------------------------------------------------------
C $Header: macmen.f,v 1.5 90/04/20 12:59:50 sveta Exp $
C $Log:	macmen.f,v $
C---> Revision 1.5  90/04/20  12:59:50  sveta
C---> ??
C---> 
C---> Revision 1.4  90/03/21  11:43:15  sveta
C---> ubacen interpreter
C---> 
C---> Revision 1.3  90/02/08  12:46:36  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  15:10:55  sveta
C---> ubacena komanda CWD
C---> 
c Revision 1.1  89/12/06  12:38:34  sveta
c Initial revision
c 
C================================================================
C
      INCLUDE 'EROR.COM'
C
1     CALL WIDTH(22)
      IERR=0
      PRINT *,'---------------'
      PRINT *,' *** MACRO *** '
      PRINT *,'---------------'
      PRINT *,'  1. LIST      '
      PRINT *,'  2. LOAD      '
      PRINT *,'  3. COMPILE   '
      PRINT *,'  4. DELETE    '
      PRINT *,'  5. RENAME    '
      PRINT *,'  6. COPY      '
      PRINT *,'  7. FREE      '
      PRINT *,'  8. EDIT      '
      PRINT *,'  9. UNPACK    '
      PRINT *,' 10. EXPLODE   '
      PRINT *,' 11. CONTENTS  '
      PRINT *,' 12. MARC      '
      PRINT *,' 13. MARX      '
      PRINT *,' 14. CWD       '
      PRINT *,'---------------'
      PRINT *,'  ENTER NUMBER'
C
      CALL RINT1(1,14,ICOM,IND)
      IF(IND.EQ.1)THEN
         CALL MACHLP
         CALL HOLD
         GOTO 1
      ENDIF
      IF(IND.EQ.-1)GOTO 99
      IF(IND.EQ.-2)GOTO 1
C
      GOTO(100,110,120,130,140,150,160,170,180,190,
     *     200,210,220,230),ICOM
C
100   CALL INTCOM('LIST')
      GOTO 1  
110   CALL INTCOM('LOAD')
      GOTO 1  
120   CALL INTCOM('COMP')
      GOTO 1  
130   CALL INTCOM('DELE')
      GOTO 1  
140   CALL INTCOM('RENA')
      GOTO 1  
150   CALL INTCOM('COPY')
      GOTO 1  
160   CALL INTCOM('FREE')
      GOTO 1  
170   CALL INTCOM('EDIT')
      GOTO 1  
180   CALL INTCOM('UNPA')
      GOTO 1  
190   CALL INTCOM('EXPL')
      GOTO 1  
200   CALL INTCOM('CONT')
      GOTO 1  
210   CALL INTCOM('MARC')
      GOTO 1  
220   CALL INTCOM('MARX')
      GOTO 1  
230   CALL INTCOM('CWD ')
      GOTO 1  
C
99    RETURN
      END
