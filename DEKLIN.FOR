      SUBROUTINE DEKLIN
C=================================================================
C1  GLAVNA RUTINA ZA KREIRANJE LINIJA
C
C   ULAZ:
C
C     USER INTERFACE
C
C   IZLAZ:
C
C     KOMON COMDRF
C     EKRAN
C-----------------------------------------------------------------
C $Header: deklin.f,v 1.17 90/03/21 11:35:04 sveta Exp $
C $Log:	deklin.f,v $
C---> Revision 1.17  90/03/21  11:35:04  sveta
C---> ubacen interpreter
C---> 
C---> Revision 1.16  90/02/05  10:21:56  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.15  90/01/29  14:50:26  sveta
C---> ubacen UNDO
C---> 
C---> Revision 1.14  89/12/06  11:36:02  sveta
C---> ubacene nove opcije i RINT1
C---> 
C---> Revision 1.13  89/06/05  12:52:01  sveta
C---> vraceno stanje pre nego sto je Pera menjao
C---> 
C---> Revision 1.11  89/02/13  07:52:59  sveta
C---> ubacena nova opcija - niz paralelnih linija
C---> 
C---> Revision 1.10  88/05/23  10:54:05  sveta
C---> ispravljen BUG
C---> 
C---> Revision 1.9  88/04/11  11:33:57  vera
C---> MSG --->  PORUKA!!!
C---> 
C---> Revision 1.8  88/04/06  10:48:14  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.7  88/02/01  13:42:36  sveta
C---> ??
C---> 
C---> Revision 1.6  87/11/12  11:51:49  sveta
C---> ??
C---> 
C---> Revision 1.5  87/10/05  11:38:29  sveta
C---> ???
C---> 
C---> Revision 1.4  87/06/29  06:54:40  sveta
C---> ????
C---> 
C---> Revision 1.3  87/06/10  08:11:38  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.2  87/05/25  13:48:47  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  07:58:12  joca
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDO.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMDRM.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'GINBEG.COM'
      INCLUDE 'EROR.COM'
      DIMENSION XB1(2),YB1(2),XB2(2),YB2(2)
      DIMENSION IVEC(1)
      CHARACTER*1 NKEY
      DATA ACCY/1.E-5/
C  
      IF(IWTEK.EQ.1)THEN
         CALL WIDTH(3)
         CALL PORUKA(137,0)
         CALL PORUKA(138,0)
         GOTO 99
      ENDIF
C
C INTERPRETERSKI DEO
C
      CALL VARG
      IF(ITIP.NE.2)THEN
          IF(IDOFLA.EQ.1)GOTO 99
          GOTO 1
      ENDIF
      X1=RNUM
      CALL VARG
      IF(ITIP.NE.2)GOTO 99
      Y1=RNUM
      CALL VARG
      IF(ITIP.NE.2)GOTO 99
      X2=RNUM
      CALL VARG
      IF(ITIP.NE.2)GOTO 99
      Y2=RNUM
      CALL UNDO(2,0,IVEC)
      CALL DRFLIN(X1,Y1,X2,Y2)
      CALL SCRAZU(IELI)
      GOTO 99
C
C  POINT SELECTION BY LOCATOR DEFAULT
C
      IPOSEL=0 
1     CALL WIDTH(18)
      CALL PORUKA(184,0)
      CALL PORUKA(185,0)
      CALL PORUKA(186,0)
      CALL PORUKA(187,0)
      CALL PORUKA(188,0)
      CALL PORUKA(189,0)
      CALL PORUKA(190,0)
      CALL PORUKA(812,0)
      WRITE(6,'(A)')' 7. TANGENT TO TWO CURVES'
      WRITE(6,'(A)')' 8. PARALEL LINE TANGENT TO CURVE'
      WRITE(6,'(A)')' 9. NORMAL LINE TANGENT TO CURVE'
      WRITE(6,'(A)')'10. ANGLE TANGENT TO CURVE'
      WRITE(6,'(A)')'11. VERTICAL LINE'
      WRITE(6,'(A)')'12. HORIZONTAL LINE'
      CALL PORUKA(183,0)
      CALL PORUKA(185,0)
      CALL PORUKA(16,0)
C
      CALL RINT1(1,12,IC,IND)
      IF(IND.EQ.1)GOTO 1
      IF(IND.EQ.-1)GOTO 99
      IF(IND.EQ.-2)GOTO 1
C
      GOTO(10,20,30,40,50,60,70,80,90,110,120,130,99),IC
C
C-------------------------------------------------------------  
C     TWO POINTS
C
10    IF(IPOSEL.NE.0)GOTO 15
C
      CALL WIDTH(2)
      CALL PORUKA(92,0)
      CALL CURSXY(NKEY,X,Y)
      IF(NKEY.EQ.'E')GOTO 1
      IF(NKEY.EQ.'Q')GOTO 99
      XFP=X
      YFP=Y
C
      CALL WIDTH(2)
      CALL PORUKA(93,0)
      IRUBER=1
      CALL CURSXY(NKEY,X,Y)
      IRUBER=0
      IF(NKEY.EQ.'E')GOTO 1
      IF(NKEY.EQ.'Q')GOTO 99
      XX=X 
      YY=Y 
      GOTO 111  
C
15    CALL WIDTH(2)
      CALL PORUKA(547,0)
      CALL READL1
      CALL VARG 
      IF(ID(1).EQ.LE)GOTO 1
      IF(ITIP.NE.2)GOTO 15 
      XFP=RNUM  
C
151   CALL VARG 
      IF(ID(1).EQ.LE)GOTO 1
      IF(ITIP.NE.2)THEN
          CALL READL1  
          GOTO 151 
      ENDIF
      YFP=RNUM  
C
152   CALL WIDTH(2)
      CALL PORUKA(548,0)
      CALL READL1
      CALL VARG 
      IF(ID(1).EQ.LE)GOTO 1
      IF(ITIP.NE.2)GOTO 152
      XX=RNUM
C
153   CALL VARG 
      IF(ID(1).EQ.LE)GOTO 1
      IF(ITIP.NE.2)THEN
          CALL READL1  
          GOTO 153 
      ENDIF
      YY=RNUM  
C
111   CALL LINA(XFP,YFP,XX,YY,X1,Y1,X2,Y2,IND)
      GOTO 100
C-----------------------------------------------------------------
C     NORMAL TO A LINE 
C
20    IF(IPOSEL.NE.0)GOTO 25
C
      CALL WIDTH(2)
      CALL PORUKA(194,0)
      CALL CURSXY(NKEY,X,Y)
      IF(NKEY.EQ.'E')GOTO 1
      IF(NKEY.EQ.'Q')GOTO 99
      X1=X 
      Y1=Y 
      CALL MARKER(2,X,Y)
C
21    CONTINUE
      CALL WIDTH(2)
2113  CALL PORUKA(97,0)
      CALL PICKPE(X,Y,IE)  
      IF(IE)1,22,23
22    CONTINUE
      CALL WIDTH(2)
      CALL PORUKA(166,0)
      GOTO 2113
C
C     DA LI JE PICKOVAN ELEMENT PRAVA  
C
23    IT=IELTYP(IE)
      IF(IT.EQ.1)GOTO 231  
      IF(IT.EQ.2) THEN 
          CALL WIDTH(2)
          CALL PORUKA(195,0)
          GOTO 2113
      ENDIF
C
C     NIJE 
C
      CALL WIDTH(2)
      CALL PORUKA(196,0)
      GOTO 2113
C-----------------------------------------------------------------
C     LINE 
C
231   IA=IELPOI(IE)
      XK=ARGEL(IA) 
      YK=ARGEL(IA+1)
      XL=ARGEL(IA+2)
      YL=ARGEL(IA+3)
      GOTO 211
C
25    CALL WIDTH(2)
      CALL PORUKA(610,0)
      CALL READL1
      CALL VARG 
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)GOTO 25 
      X1=RNUM  
C
251   CALL VARG 
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)THEN
          CALL READL1  
          GOTO 251 
      ENDIF
      Y1=RNUM  
      GOTO 21  
C
211   CALL LINB(XK,YK,XL,YL,X1,Y1,X2,Y2,IND)
      IF(IND.EQ.0)GOTO 1
      GOTO 100
C-----------------------------------------------------------------
C     PARALEL TO A LINE
C
30    CONTINUE
      CALL WIDTH(2)
3013  CALL PORUKA(97,0)
      CALL PICKPE(X,Y,IE)
      IF(IE) 1,32,33
32    CONTINUE
      CALL WIDTH(2)
      CALL PORUKA(166,0)
      GOTO 3013
C
C     DA LI JE PIKOVAN ELEMENT PRAVA?
C
33    IT=IELTYP(IE)
      IF(IT.NE.1) GOTO 32
      IP=IELPOI(IE)
      XK=ARGEL(IP)
      YK=ARGEL(IP+1)
      XL=ARGEL(IP+2)
      YL=ARGEL(IP+3)
C
      CALL WIDTH(2)
      CALL PORUKA(197,0)
      CALL CURSXY(NKEY,XJ,YJ)
      IF(NKEY.EQ.'E')GOTO 1
      IF(NKEY.EQ.'Q')GOTO 99
C
      CALL MARKER(2,XJ,YJ)
      CALL WIDTH(3)
      CALL PORUKA(198,0)
      CALL PORUKA(199,0)
      CALL CURSXY(NKEY,XT,YT)
      IF(NKEY.EQ.'E')GO TO 1
      IF(NKEY.EQ.'Q')GO TO 99
C
34    CALL WIDTH(2)
      CALL PORUKA(611,0) 
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GO TO 1
      ENDIF
      IF(ITIP.NE.2)GO TO 34
      DST=RNUM
C
      CALL LINC(XK,YK,XL,YL,XJ,YJ,DST,X11,Y11,X22,Y22,IND)
      IF(IND.EQ.0)GOTO 1
C
      CALL DIST(X11,Y11,XT,YT,D1)
      CALL DIST(X22,Y22,XT,YT,D2)
      IF(D1.LE.D2)THEN
         X1=XJ
         Y1=YJ
         X2=X11
         Y2=Y11
      ELSE
         X1=XJ
         Y1=YJ
         X2=X22
         Y2=Y22
      ENDIF
      GOTO 100
C-----------------------------------------------------------------
C     ANGLE TO A LINE
C
40    CONTINUE
      CALL WIDTH(3)
4013  CALL PORUKA(97,0)
      CALL PICKPE(X,Y,IE)
      IF(IE) 1,42,43
42    CONTINUE
      CALL WIDTH(3)
      CALL PORUKA(166,0)
      GOTO 4013
C
C     DA LI JE PIKOVAN ELEMENT PRAVA? 
C
43    IT=IELTYP(IE)
      IF(IT.NE.1) GOTO 42
      IP=IELPOI(IE)
      XK=ARGEL(IP)
      YK=ARGEL(IP+1)
      XL=ARGEL(IP+2)
      YL=ARGEL(IP+3)
C
      CALL WIDTH(3)
      CALL PORUKA(200,0)
      CALL CURSXY(NKEY,XT,YT)
      IF(NKEY.EQ.'E')GO TO 99
      IF(NKEY.EQ.'Q')GO TO 99
      CALL DIST(XT,YT,XK,YK,D1)
      CALL DIST(XT,YT,XL,YL,D2)
      IF(D2.LT.D1)THEN
         XT=XK
         YT=YK
         XK=XL
         YK=YL
         XL=XT
         YL=YT
      ENDIF
C
48    CALL WIDTH(3)
      CALL PORUKA(612,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE) GOTO 99
      ENDIF
      IF(ITIP.NE.2) GOTO 48
      ANG1=RNUM
C
49    CALL WIDTH(3)
      CALL PORUKA(611,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE) GOTO 99
      ENDIF
      IF(ITIP.NE.2) GOTO 49
      DST=RNUM
C
      CALL LIND(XK,YK,XL,YL,DST,ANG1,X2,Y2,IND)
      IF(IND.EQ.0) GOTO 1
      X1=XK
      Y1=YK
      GOTO 100
C
C-----------------------------------------------------------------
C     TANGENT TO AN ARC/CIRCLE 
C
50    CONTINUE
      CALL WIDTH(2)
5013  PRINT *,' PICK AN ARC/CIRCLE'
      CALL PICKPE(X,Y,IE)
      IF(IE) 1,52,53
C
52    CONTINUE
      CALL WIDTH(2)
      CALL PORUKA(166,0)
      GOTO 5013
C
C     DA LI JE PIKOVAN ELEMENT KRUG
C
53    IT=IELTYP(IE)
      IF(IT.EQ.4)THEN
         IP=IELPOI(IE)
         XC=ARGEL(IP)
         YC=ARGEL(IP+1)
         RC=ARGEL(IP+2)
      ELSE IF(IT.EQ.5)THEN
         IP=IELPOI(IE)
         XC=ARGEL(IP)
         YC=ARGEL(IP+1)
         CALL DIST(XC,YC,ARGEL(IP+2),ARGEL(IP+3),RC)
      ELSE
         GOTO 52
      ENDIF
C
      CALL WIDTH(2)
      CALL PORUKA(69,0)
      CALL CURSXY(NKEY,XJ,YJ)
      IF(NKEY.EQ.'E')GOTO 99
      CALL MARKER(2,XJ,YJ)
C
      CALL LINE(XC,YC,RC,XJ,YJ,X,Y,XX,YY,IND)
      IF(IND.EQ.0) GOTO 99
C
      CALL WIDTH(2)
      CALL PORUKA(171,0)
      CALL CURSXY(NKEY,XT,YT)
      IF(NKEY.EQ.'E')GOTO 1
      IF(NKEY.EQ.'Q')GOTO 99
      CALL DIST(XT,YT,X,Y,D1)
      CALL DIST(XT,YT,XX,YY,D2)
      IF(D2.LT.D1)THEN
         X=XX
         Y=YY
      ENDIF
      X1=XJ
      Y1=YJ
      X2=X
      Y2=Y
      GOTO 100
C-----------------------------------------------------------------  
C-----------------------------------------------------------------
C     ARRAY OF LINES PARALEL TO A LINE
C
60    CONTINUE
      CALL WIDTH(2)
6013  CALL PORUKA(97,0)
      CALL PICKPE(X,Y,IE)
      IF(IE) 1,62,63
62    CONTINUE
      CALL WIDTH(2)
      CALL PORUKA(166,0)
      GOTO 6013
C
C  DA LI JE PIKOVAN ELEMENT PRAVA ?
C
63    IT=IELTYP(IE)
      IF(IT.NE.1)GOTO 62
      IP=IELPOI(IE)
      XK=ARGEL(IP)
      YK=ARGEL(IP+1)
      XL=ARGEL(IP+2)
      YL=ARGEL(IP+3)
C
C UNOSENJE OFFSETA
C
64    CALL WIDTH(2)
      CALL PORUKA(813,0) 
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)GO TO 64
      DST=RNUM
C
C UNOSENJE BROJA DUZI
C
65    CALL WIDTH(2)
      CALL PORUKA(814,0) 
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GO TO 1
      ENDIF
      IF(ITIP.NE.2)GO TO 65
      NOLIN=INUM
C
      CALL WIDTH(2)
      CALL PORUKA(171,0)
      CALL CURSXY(NKEY,XT,YT)
      IF(NKEY.EQ.'E')GO TO 1
      IF(NKEY.EQ.'Q')GO TO 99
C
      CALL PARLIN(XK,YK,XL,YL,DST,XB1,YB1,XB2,YB2,IND)
      IF(IND.NE.2)GOTO 1
      CALL LINB(XB1(1),YB1(1),XB2(1),YB2(1),XT,YT,XS1,YS1,IND)
      CALL LINB(XB1(2),YB1(2),XB2(2),YB2(2),XT,YT,XS2,YS2,IND)
      CALL DIST(XT,YT,XS1,YS1,D1)
      CALL DIST(XT,YT,XS2,YS2,D2)
      IF(D1.EQ.D2)GOTO 1
      IF(D1.LT.D2)L=1
      IF(D1.GT.D2)L=2
C
      DO 66 IJK=1,NOLIN
          DST1=DST*FLOAT(IJK)
          CALL PARLIN(XK,YK,XL,YL,DST1,XB1,YB1,XB2,YB2,IND)
          CALL DRFLIN(XB1(L),YB1(L),XB2(L),YB2(L))
          CALL SCRAZU(IELI)
          CALL COMAZU(IELI,IELI)
66    CONTINUE
      GOTO 1
C
C-----------------------------------------------------------------
C     TANGENT TO TWO CURVES
C
70    CONTINUE
      CALL WIDTH(2)
7013  PRINT *,'SELECT FIRST CURVE'
      CALL PICKPE(XF1,YF1,IE1)
      IF (IE1.LT.0)GOTO 1
      call slarci(IE1,xk1,yk1,xl1,yl1,xc1,yc1,ind1)
      if (ind1.eq.0) then
          CALL WIDTH(2)
          CALL PORUKA(166,0)
          goto 7013
      endif
C
73    CONTINUE
      CALL WIDTH(2)
7313  print *,'SELECT SECOND CURVE'
      call pickpe(XF2,YF2,IE2)
      if (IE2.lt.0) goto 1
      call slarci(IE2,xk2,yk2,xl2,yl2,xc2,yc2,ind2)
C
      dw1=abs(xc1-xc2)
      dw2=abs(yc1-yc2)
      if (ind2.eq.0.or.(dw1.lt.accy.and.dw2.lt.accy)) then
          CALL WIDTH(2)
          CALL PORUKA(166,0)
          goto 7313
      endif
C
      call dist(xk1,yk1,xc1,yc1,rc1)
      call dist(xk2,yk2,xc2,yc2,rc2)
C
      call t2cigl(xc1,yc1,rc1,xc2,yc2,rc2,XF1,YF1,XF2,YF2,
     1            X1,Y1,X2,Y2,ind)
      if(ind.eq.0) then
         CALL WIDTH(2)
         print *,'*** SOLUTION NOT FOUND !'
         goto 1
      endif
      goto 100
C
C
C-----------------------------------------------------------------
C     PARALEL LINE TANTO CURVE
80    alfa=0
      goto 115
C
C     NORMAL LINE TANTO CURVE
90    alfa=90
      goto 115
C
C     ANGLE TANTO CURVE
C
C     ENTER ANGLE
110   CONTINUE
      call rreal(612,alfa,ind)
      if (ind.eq.1) goto 1
C
C     ZAJEDNICKI DEO
115   CALL WIDTH(2)
11513 print *,'SELECT BASE LINE'
      call linsel(X3,Y3,X4,Y4,Z,Z1,IND5)
      if (IND5) 1,116,117
C
116   CONTINUE
      CALL WIDTH(2)
      CALL PORUKA(166,0)
      goto 11513
C
117   CONTINUE
      CALL WIDTH(2)
11713 print *,'SELECT CURVE'
      call pickpe(XT,YT,IE) 
      if (IE.lt.0) goto 1
      call slarci(IE,XK,YK,XL,YL,XC,YC,IND6)
C
      if (IND6.eq.0) then
          CALL PORUKA(166,0)
          goto 11713
      endif
C
      call dist(XK,YK,XC,YC,RR)
      call pls(X3,Y3,X4,Y4,XC,YC,RR,XT,YT,Z,Z1,
     1         alfa,XQ,YQ,XW,YW,IND7)
C
      call xyabc(XQ,YQ,XW,YW,AA,BB,CC,IND8)
      if (IND8.eq.0) then
         CALL WIDTH(2)
         print *,'*** SOLUTION NOT FOUND !'
         goto 1
      endif
C
      call inliek(AA,BB,CC,X1,Y1,X2,Y2)
C
      goto 100
C
C-----------------------------------------------------------------
C
C     VERTICAL LINE
C
120   CONTINUE
      CALL WIDTH(2)
      print *,'PICK THE POINT'
      call CURSXY(NKEY,X,Y)
      if (NKEY.eq.'E') goto 1
      if (NKEY.eq.'Q') goto 99
      xv=X
      yv=Y
      call MARKER(3,xv,yv)
C
C     POZIV RUTINE ZA CRTANJE VERTIKALE
      call verho1(1,xv,yv,XQ,YQ,XW,YW,AA,BB,CC)
      call inliek(AA,BB,CC,X1,Y1,X2,Y2)
C
      goto 100
C
C-----------------------------------------------------------------
C
C     HORIZONTAL LINE
C
130   CONTINUE
      CALL WIDTH(2)
      print *,'PICK THE POINT'
      call CURSXY(NKEY,X,Y)
      if (NKEY.eq.'E') goto 1
      if (NKEY.eq.'Q') goto 99
      call MARKER(3,X,Y)
C
C     POZIV RUTINE ZA CRTANJE HORIZONTALE
      call verho1(2,X,Y,XQ,YQ,XW,YW,AA,BB,CC)
      call inliek(AA,BB,CC,X1,Y1,X2,Y2)
C
C
      goto 100
C
C-----------------------------------------------------------------
100   CALL UNDO(1,0,IVEC)
      CALL DRFLIN(X1,Y1,X2,Y2)
      CALL SCRAZU(IELI)
      CALL COMAZU(IELI,IELI)
C
      GOTO(10,20,30,40,50,60,70,80,90,110,120,130,99),IC

99    RETURN
      END  
