      SUBROUTINE OPNSAV
C=====================================================================
C1   VRSI OPEN SAV BAZE PODATAKA
C
C  ULAZ:
C
C    SAVE BAZA PODATAKA
C--------------------------------------------------------------------
C $Header: opnsav.f,v 1.3 90/02/08 16:34:45 sveta Exp $
C $Log:	opnsav.f,v $
C---> Revision 1.3  90/02/08  16:34:45  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  15:15:34  sveta
C---> ubacen radni direktorijum
C---> 
c Revision 1.1  89/12/06  12:43:29  sveta
c Initial revision
c 
C=====================================================================
C
      INCLUDE 'COMMAX.COM'
      INCLUDE 'PROTCT.COM'
      INCLUDE 'COMSET.COM'
      INCLUDE 'COMSTA.COM'
      INCLUDE 'COMPAS.COM'
      INCLUDE 'COMLMA.COM'
      INCLUDE 'EROR.COM'
      INTEGER*4 IME(3),TEMP(280)
      CHARACTER*12 IMEMAT,IMEMAX
      EQUIVALENCE (IME(1),USRIME)
C
      REKLEN=280
      IERR=0
      IMEMAT='BOLIDSAV'
      IMEMAX='BOLIDSVX'
      IRECL=REKLEN
      LUNMAT=UNTSAV
      LUNMAX=UNTSAV+1
      NXMA=REKLEN/5
      NBMA=REKLEN
      MAXIMA=1000
C
C VALIDITY AND SIGNATURE KEYWORDS
C
      EQVALK='@OK!'
      EQSIGT='@MAT'
      EQSIGX='@MAX'
C
      CALL OPEND(LUNMAT,WDIR(1:IWD)//IMEMAT,IRECL,ISTAT)
      IF(ISTAT.EQ.0)THEN
C
C DA LI JE READ-ONLY, IRDO=1 --> JESTE
C
         IF(IRDO.EQ.0)THEN
             K=0
C
C  TEST DA LI JE FAJL ZAUZET OD STRANE DRUGOG KORISNIKA
C
10           CALL READD(LUNMAT,2,TEMP)
             IF(K.GT.NAUTO)THEN
                 CALL PRTOUT(3,12,0,IMEMAT,1)
                 CALL PORUKA(565,1)
                 CALL PORUKA(566,1)
                 DO 55 J=2,4
                 CALL PRTOUT(3,4,0,TEMP(J),0)
55               CONTINUE
                 IERR=1
                 GOTO 99
             ENDIF
             K=K+1
C
C  ZAUZET
C
             IF(TEMP(1).NE.0)THEN
                  CALL SLOW(10)
                  GOTO 10
             ELSE
C
C  SLOBODAN
C
                  TEMP(1)=1
                  TEMP(2)=IME(1)
                  TEMP(3)=IME(2)
                  TEMP(4)=IME(3)
                  CALL WRITED(LUNMAT,2,TEMP)
                  IERR=0
             ENDIF
          ENDIF
      ELSE
         IERR=1
         CALL PORUKA(517,1)
         CALL PRTOUT(3,12,0,IMEMAT,1)
         CALL PORUKA(518,1)
         CALL PRTOUT(1,ISTAT,0,' ',0)
         GOTO 99
      ENDIF
C
      CALL OPEND(LUNMAX,WDIR(1:IWD)//IMEMAX,IRECL,ISTAT)
      IF(ISTAT.NE.0)THEN   
          CALL PORUKA(517,1)
          CALL PRTOUT(3,12,0,IMEMAX,1)
          CALL PORUKA(518,1)
          CALL PRTOUT(1,ISTAT,0,' ',0)
          IERR=1
          GOTO 99
      ENDIF
C
C     UCITA STACK ZA MAT-FILE
C
      CALL READD(LUNMAT,1,MASTAC)
      NSTAMA=MASTAC(1)
C
C UCITA FREE RECORD AND ENTRY STACK LINKS
C
      CALL READD(LUNMAT,3,TEMP)
      LASTRC=TEMP(4)
      LASTEY=TEMP(5)
C
C     UCITA STACK ZA MATX-FILE
C
      CALL READD(LUNMAX,1,MXSTAC)
      NMAE=MXSTAC(1)
      NAWMA=MXSTAC(2)
      NSTAMX=MXSTAC(3)
C
C     UCITA INDEX-BLOK MATX-FILEA
C
      CALL READD(LUNMAX,2,IPRMAX)
      ISEQMA=2
C
99    RETURN
      END
