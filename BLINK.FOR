      SUBROUTINE BLINK(IE,IND)
C====================================================================
C1  "BLINKOVANJE" PRIMITIVE IZ TABELE
C
C ULAZNE PROMENLJIVE:
C
C  IE        = REDNI BROJ PRIMITIVE U TABELI
C  IND       = 1 - BLINKOVANJE ; 0 - PRESTANAK BLINKOVANJA
C
C IZLAZ:
C
C  EKRAN
C--------------------------------------------------------------------
C $Header: blink.f,v 1.2 90/02/02 11:10:11 sveta Exp $
C $Log:	blink.f,v $
C---> Revision 1.2  90/02/02  11:10:11  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  89/10/11  10:59:53  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMWIN.COM'
C
      IWW=1
      IF(IWTEK.GT.1)IWW=2
C
      IS=IELSEG(IWW,IE)
      IF(IS.EQ.0)GOTO 99
C
      CALL LLHISG(IS,IND)
C
99    RETURN
      END
