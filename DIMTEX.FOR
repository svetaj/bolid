      SUBROUTINE DIMTEX(ITFONT,XK,YK,ALFA,HC,VAL)
C==================================================================
C1  ISPISUJE TEKST PRI KOTIRANJU
C
C  ULAZNE PROMENLJIVE:
C
C    (XK,YK)    =  MESTO ISPISIVANJA TEKSTA
C    ALFA       =  UGAO TEKSTA
C    HC         =  VISINA CIFARA
C    VAL        =  BROJ KOJI SE ISPISUJE
C    ITFONT     =  BROJ DECIMALA
C
C  IZLAZ:
C
C    EKRAN, PLOTER
C------------------------------------------------------------------
C $Header: dimtex.f,v 1.4 90/02/05 12:12:54 sveta Exp $
C $Log:	dimtex.f,v $
C---> Revision 1.4  90/02/05  12:12:54  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.3  88/02/03  12:51:32  sveta
C---> ??
C---> 
C---> Revision 1.2  87/06/10  08:12:36  sveta
C---> *** empty log message ***
C---> 
C==================================================================
C
      IF(ITFONT.EQ.0)GOTO 99
C
      ITF=ITFONT
      IF(ITF.EQ.999)ITF=0
      CALL NUMB(XK,YK,HC,VAL,ALFA,ITF)
C
99    RETURN
      END
