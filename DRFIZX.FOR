      SUBROUTINE DRFIZX
C=================================================================
C1  IZVRSAVA INSTRUKCIJE IZ DRF-BUFFERA
C1    I VRSI UPISIVANJE U ARCHIVE FAJL
C
C    ULAZ:
C
C      KOMON COMMAF
C   
C    IZLAZ:
C
C      ASCII FAJL U SPECIJALNOM FORMATU
C-----------------------------------------------------------------
C $Header: drfizx.f,v 1.2 90/02/05 14:28:55 sveta Exp $
C $Log:	drfizx.f,v $
C---> Revision 1.2  90/02/05  14:28:55  sveta
C---> izmenjeno zaglavlje
C---> 
C---> Revision 1.1  89/04/24  09:03:42  sveta
C---> Initial revision
C---> 
C=================================================================
C  
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMMAF.COM'
      INCLUDE 'DRFBUF.COM'
      INCLUDE 'MEASUN.COM'
      DIMENSION RMAF(5,56) 
      DIMENSION IVEC(5),RVEC(4)
      CHARACTER*4 AVEC(4)
      EQUIVALENCE (RMAF(1,1),MAF(1,1)) 
C  
1     II=MAF(1,KR) 
C  
C  DA LI JE KRAJ TEKUCEG MAKROA ?  
C  
      IF(II.EQ.0)THEN
          IVEC(1)=0
          IVEC(2)=0
          IVEC(3)=0
          IVEC(4)=0
          IVEC(5)=0
C----------------------------------------------------------------
          CALL PUTAR(13,' ',1.,IVEC,5)
C----------------------------------------------------------------
          GOTO 99
      ENDIF
C  
C DA LI JE BIO NASTAVAK?
C  
      IF(II.LT.0)CALL TEGDRF  
C  
C  U PITANJU JE JEDNA OD PRIMITIVA 
C  
      IF(II.GE.1.AND.II.LE.15)THEN 
C  
C AKO JE MAKRO 
C  
           IF(II.EQ.14)THEN
               IVEC(1)=14
               IVEC(2)=0
               IVEC(3)=0
               IVEC(4)=0
               IVEC(5)=0
C----------------------------------------------------------------
               CALL PUTAR(13,' ',1.,IVEC,5)
C----------------------------------------------------------------
               IVEC(1)=MAF(2,KR) 
               IVEC(2)=MAF(3,KR) 
               IVEC(3)=MAF(4,KR) 
C----------------------------------------------------------------
               CALL PUTAR(11,' ',1.,IVEC,3)
C----------------------------------------------------------------
               CALL TEGDRF 
               RVEC(1)=RMAF(2,KR)
               RVEC(2)=RMAF(3,KR)
               RVEC(3)=RMAF(4,KR) 
               RVEC(4)=0.
C----------------------------------------------------------------
               CALL PUTAR(14,' ',RVEC,0,4)
C----------------------------------------------------------------
           ELSE
C  
C AKO NIJE MAKRO
C  
               CALL LOADRX
           ENDIF
       ENDIF
C  
       IF(II.GT.15)THEN
           CALL PORUKA(221,0)
           GOTO 99 
       ENDIF
       GOTO 1  
C  
99     RETURN  
       END 
