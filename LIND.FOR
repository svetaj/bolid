      SUBROUTINE LIND(X1,Y1,X2,Y2,DIST,ANGLE,XM,YM,IND)
C=================================================================
C2 NALAZI DUZ KOJA IMA JEDNO ZAJEDNICKO TEME SA ZADATOM DUZI.
C2 TRAZENA DUZ JE DUZINE DIST I ZAKLAPA SA DATOM DUZI UGAO ANGLE
C 
C ULAZNE PROMENLJIVE:
C
C  (X1,Y1) I (X2,Y2) = KRAJNJE TACKE PRVE DUZI
C  (X1,Y1)           = JEDNA OD KRAJNJIH TACAKA DRUGE DUZI
C  DIST              = DUZINA DRUGE DUZI
C  ANGLE             = UGAO IZMEDJU PRVE I DRUGE DUZI (U STEPENIMA)
C
C IZLAZNE PROMENLJIVE:
C
C  (XM,YM)           = KRAJNJA TACKA DRUGE DUZI
C   IND              = INDIKATOR BROJA RESENJA (0,1)
C
C POZIVI PODPROGRAMA:
C
C  ANG2D             = RACUNA UGAO IZMEDJU DUZI I  X-OSE
C-----------------------------------------------------------------
C $Header: lind.f,v 1.4 90/02/07 14:14:10 sveta Exp $
C $Log:	lind.f,v $
C---> Revision 1.4  90/02/07  14:14:10  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.3  87/06/10  08:13:20  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.2  87/05/25  13:56:54  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  11:00:33  joca
C---> Initial revision
C---> 
C=================================================================
C
      DATA PI/3.1415926/
C
      CALL ANG2D(X1,Y1,X2,Y2,ALFA)
C
      BETA=(ANGLE+ALFA)*PI/180.
      XM=X1+DIST*COS(BETA)
      YM=Y1+DIST*SIN(BETA)
      IND=1
C
      RETURN
      END

