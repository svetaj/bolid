      SUBROUTINE RP3(X1,Y1)
C==========================================================
C1  STAMPA KOORDINATE DATE TACKE 
C
C  ULAZNE PROMENLJIVE:
C
C     X1,Y1  = DATA TACKA
C
C  IZLAZ:
C
C     EKRAN
C----------------------------------------------------------
C $Header: rp3.f,v 1.2 90/02/12 11:27:23 sveta Exp $
C $Log:	rp3.f,v $
C---> Revision 1.2  90/02/12  11:27:23  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/10/11  11:51:21  sveta
C---> Initial revision
C---> 
C==========================================================
C
      CALL WIDTH(6)
      PRINT *,'    *** POINT ***'
      PRINT *,' ---------------------'
      PRINT *,' X1     =',X1
      PRINT *,' Y1     =',Y1
C
      RETURN 
      END
