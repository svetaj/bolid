      SUBROUTINE DIVIDE
C===========================================================
C1  DELI DUZI I LUKOVE NA N DELOVA
C
C  ULAZ:
C
C     USER INTERFACE
C
C  IZLAZ:
C
C     KOMON COMDRF
C     EKRAN
C-----------------------------------------------------------
C $Header: divide.f,v 1.3 90/02/05 12:12:58 sveta Exp $
C $Log:	divide.f,v $
C---> Revision 1.3  90/02/05  12:12:58  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  90/01/29  14:53:17  sveta
C---> ubacen UNDO
C---> 
c Revision 1.1  89/12/06  11:38:41  sveta
c Initial revision
c 
C===========================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'GRPTMP.COM'
      DIMENSION C(3,3),IVEC(1)
C
C UNOSENJE BROJA DELOVA
C
10    CALL WIDTH(3)
      PRINT *,' ENTER NUMBER OF SEGMENTS'
      CALL RINT1(2,100,NOS,IND)
      IF(IND.EQ.1)GOTO 10
      IF(IND.EQ.-1)GOTO 99
      IF(IND.EQ.-2)GOTO 10
C
C POKAZIVANJE LUKA/DUZI
C
20    CALL WIDTH(3)
      PRINT *,' PICK LINE/ARC'
      CALL PICKPE(X,Y,IE)  
      IF(IE.EQ.0)THEN 
         CALL PORUKA(14,0)
         GOTO 20
      ENDIF
      IF(IE.EQ.-1)GOTO 99
C
      IT=IELTYP(IE)
      IP=IELPOI(IE)
      NOPG=1
      ITEMPG(1)=IE
C
C DELJENJE DUZI
C
      IF(IT.EQ.1)THEN
         XB=ARGEL(IP)
         YB=ARGEL(IP+1)
         XE=ARGEL(IP+2)
         YE=ARGEL(IP+3)
         CALL MARKER(2,XB,YB)
         CALL MARKER(2,XE,YE)
         CALL DIST(XB,YB,XE,YE,DST)
         DELTA=DST/FLOAT(NOS)
C
         IVEC(1)=IE
         CALL UNDO(11,0,IVEC)
C
C AZURIRANJE PRVE DUZI
C
         CALL LIND(XB,YB,XE,YE,DELTA,0.,XN,YN,IND)
         ARGEL(IP+2)=XN
         ARGEL(IP+3)=YN
         CALL MARKER(2,XN,YN)
         CALL SCRAZU(IE)
C
C KREIRANJE NOVIH DUZI I AZURIRANJE
C
         DO 30 I=2,NOS
            CALL COPDRF(IE)
            IP=IELPOI(IELI)
            XB=XN
            YB=YN
            CALL LIND(XB,YB,XE,YE,DELTA,0.,XN,YN,IND)
            ARGEL(IP)=XB
            ARGEL(IP+1)=YB
            ARGEL(IP+2)=XN
            ARGEL(IP+3)=YN
            CALL MARKER(2,XN,YN)
            CALL SCRAZU(IELI)
30       CONTINUE
C
C DELJENJE LUKA
C
      ELSE IF(IT.EQ.5)THEN
         XC=ARGEL(IP)
         YC=ARGEL(IP+1)
         XB=ARGEL(IP+2)
         YB=ARGEL(IP+3)
         XE=ARGEL(IP+4)
         YE=ARGEL(IP+5)
         CALL MARKER(2,XC,YC)
         CALL MARKER(2,XB,YB)
         CALL MARKER(2,XE,YE)
         CALL DIST(XB,YB,XC,YC,RQ)
         CALL ANGARC(XC,YC,XB,YB,XE,YE,VALUE)
         DELTA=VALUE/FLOAT(NOS)
C
         IVEC(1)=IE
         CALL UNDO(11,0,IVEC)
C
C AZURIRANJE POCETNOG LUKA
C
         CALL LIND(XC,YC,XB,YB,RQ,DELTA,XN,YN,IND)
         ARGEL(IP+4)=XN
         ARGEL(IP+5)=YN
         CALL MARKER(2,XN,YN)
         CALL SCRAZU(IE)
C
C KREIRANJE NOVIH LUKOVA I AZURIRANJE
C
         DO 40 I=2,NOS
            CALL COPDRF(IE)
            IP=IELPOI(IELI)
            XB=XN
            YB=YN
            CALL LIND(XC,YC,XB,YB,RQ,DELTA,XN,YN,IND)
            ARGEL(IP+2)=XB
            ARGEL(IP+3)=YB
            ARGEL(IP+4)=XN
            ARGEL(IP+5)=YN
            CALL MARKER(2,XN,YN)
            CALL SCRAZU(IELI)
40       CONTINUE
      ENDIF
C
99    RETURN
      END
