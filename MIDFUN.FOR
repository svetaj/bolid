      SUBROUTINE MIDFUN(IOPER,ILIT,IPOS,LENGTH,AREZ)
C==========================================================
C1  ROUTINE TO PERFORM STRING OPERATIONS
C1  MID$(ILIT,IPOS,LENGTH)
C
C  ULAZNE PROMENLJIVE:
C
C     IOPER  = 1 - IZVRSAVA SE SLEDECA FUNKCIJA
C                  MID$(ILIT,IPOS,LENGTH)
C     ILIT   = 
C     IPOS   =
C     LENGTH =
C
C  IZLAZNE PROMENLJIVE:
C
C     AREZ   =
C----------------------------------------------------------
C $Header: midfun.f,v 1.3 90/02/08 15:48:37 sveta Exp $
C $Log:	midfun.f,v $
C---> Revision 1.3  90/02/08  15:48:37  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  88/04/08  11:52:06  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.1  87/10/05  11:47:08  sveta
C---> Initial revision
C---> 
C==========================================================
C
      INCLUDE 'COMTXT.COM'
      INCLUDE 'EROR.COM'
C
      CHARACTER*41 AREZ,TEMP
C
      IF(IOPER.EQ.1) THEN
C
C MID$ FUNCTION
C
           AREZ=' '
           AREZ(1:1)=CHAR(0)
C
C FETCH LITERAL OPERAND
C
           CALL VARIO(8,ILIT)
C
C TEST ARGUMENT VALIDITY
C
           IF (IPOS.LT.0.OR.IPOS.GT.NTX) GOTO 98
           IF (LENGHT.LT.0) GOTO 98
           IF((IPOS+LENGTH-1).GT.NTX) THEN
C
C CHARACTER OVERFLAW
C
               ILEN=NTX+1-IPOS
           ELSE
C
C OK !!
C
               ILEN=LENGTH
           ENDIF
C
           AREZ(1:1)=CHAR(ILEN)
           CALL CONLTX(TEMP)
C
           IB1=IPOS+1
           IB2=IB1+ILEN-1
           AREZ(2:ILEN+1)=TEMP(IB1:IB2)
           GOTO 99
      ENDIF	
C--------------------------------------------
C ERRORS
C
98    CONTINUE
      CALL PORUKA(320,0)
      GOTO 99
C
99    CONTINUE
      RETURN
      END
