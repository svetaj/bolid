      SUBROUTINE DRFX(IDSAV)
C=========================================================
C1  UCITAVA DUMMY IZ BAZE PODATAKA MAKROA
C
C  ULAZNE PROMENLJIVE:
C
C    IDSAV   = IME MAKROA KOJI SE CITA IZ BAZE PODATAKA
C              I UPISUJE U ARCHIVE FAJL
C---------------------------------------------------------
C $Header: drfx.f,v 1.2 90/02/05 15:08:14 sveta Exp $
C $Log:	drfx.f,v $
C---> Revision 1.2  90/02/05  15:08:14  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/04/24  09:04:48  sveta
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'DRFBUF.COM'
      INCLUDE 'CFIND.COM'
      INCLUDE 'COMMAF.COM'
      INCLUDE 'COMLMF.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMDRM.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'PROTCT.COM'
      INCLUDE 'EROR.COM'
      INCLUDE 'COMPAS.COM'
      DIMENSION RMAF(5,56) 
      CHARACTER*4 IDSAV(3) 
      EQUIVALENCE (MAF(1,1),RMAF(1,1)) 
C  
C OTVARANJE DRF-FAJLOVA
C
      IRDO=1
      CALL OPNDRF
      IF(IERR.NE.0)GOTO 99
C
      ID(1)=IDSAV(1)
      ID(2)=IDSAV(2)
      ID(3)=IDSAV(3)
C
13    CONTINUE
C  
      DO 230 JJ=1,3
         DO 220 II=1,3 
             IF(II.EQ.JJ)THEN  
                 B(II,JJ)=1.
             ELSE  
                 B(II,JJ)=0.
             ENDIF 
220       CONTINUE 
230    CONTINUE
C  
      CALL FNDDRF(LENMA)
      IF(LENMA.EQ.0)THEN
          CALL PORUKA(353,1)
          CALL PRTOUT(3,4,0,ID,0)
          IERR=1
          GOTO 98  
      ENDIF
C  
C   START READING DRF-MACRO
C  
      IDEEP=1  
      CALL SREDRF  
C  
C  SAVE MAIN DRF-MACRO 
C  
      K=2  
      DO 25 I=1,5  
         DO 20 J=1,56  
            K=K+1
            IBUF(IDEEP,K)=MAF(I,J)
20       CONTINUE 
25    CONTINUE 
C
      DO 30 I=1,MAXIMA
         IADR(IDEEP,I)=LISTMA(I)  
30    CONTINUE 
C
      CALL DRFIZX
      GOTO 98
C  
88    CALL SNXMSG  
C  
C ZATVARANJE DRF-FAJLOVA
C
98    CALL CLSDRF
C
99    RETURN
      END  
