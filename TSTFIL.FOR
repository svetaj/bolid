      SUBROUTINE TSTFIL(IE,IE1,IE2,IND)
C====================================================================
C1 TESTIRA DA LI PRIMITIVA IMA FILL PATTERN
C1 NE KORISTI SE
C
C  ULAZNE PROMENLJIVE:
C
C     IE    = REDNI BROJ PRIMITIVE U TABELI
C
C  ULAZ:
C
C     KOMON COMDRF 
C
C  IZLAZNE PROMENLJIVE:
C
C     IE1   = REDNI BROJ BEGIN FILL
C     IE2   = REDNI BROJ END FILL
C     IND   = 0 - NEMA FILL  
C             1 - IMA FILL
C--------------------------------------------------------------------
C $Header: tstfil.f,v 1.2 90/02/13 11:56:45 sveta Exp $
C $Log:	tstfil.f,v $
C---> Revision 1.2  90/02/13  11:56:45  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/05/23  11:05:33  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMDRF.COM'
C
      IND=0
C
C TRAZENJE END FILL AREA U TABELI
C
      DO 201 I=IE+1,IELI
          IF(IELTYP(I).EQ.13)THEN
              IE2=I
              GOTO 202
          ENDIF
201   CONTINUE
      GOTO 99
C
C TRAZENJE BEGIN FILL AREA U TABELI
C
202   DO 203 I=IE-1,1,-1
          IF(IELTYP(I).EQ.13)THEN
              IE1=I
              GOTO 204
          ENDIF
203   CONTINUE
      GOTO 99
C
204   IF(IELCOL(IE1).EQ.1.AND.IELCOL(IE2).EQ.2)IND=1
C
99    RETURN
      END
