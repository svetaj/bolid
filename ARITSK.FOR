      SUBROUTINE ARITSK
C=============================================================
C1  ARITMETICKI SKANER
C1  FORMIRA VEKI I VEKA VEKTORE DO ZAREZA
C-------------------------------------------------------------
C $Header: aritsk.f,v 1.4 90/02/02 10:13:38 sveta Exp $
C $Log:	aritsk.f,v $
C---> Revision 1.4  90/02/02  10:13:38  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.3  88/09/01  13:55:32  olga
C---> COMVAB zamenjen sa COMVAR, ID sa EQID.
C---> 
C---> Revision 1.2  88/04/01  08:41:30  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.1  87/04/09  07:35:03  joca
C---> Initial revision
C---> 
C=============================================================
C
      INCLUDE 'VEKI.COM'
      INCLUDE 'REPREZ.COM'
      INCLUDE 'COMVAR.COM'
      DIMENSION RVEKA(72)
      EQUIVALENCE (VEKA(1),RVEKA(1))
C
      LERR=0
      ISYM=3
      NSYM=0
      DO 1 I=1,72  
      VEKI(I)=0
      VEKA(I)=NOFIN9
1     CONTINUE 
C  
2     CALL ATOM
      IF(ITIP.EQ.0) GOTO 99
      IF(ITIP.EQ.ZAREZ9) GOTO 99
      IF(ITIP.LT.0) GOTO 98
C
C     NSYM   - BROJ SIMBOLA
C     ISYM   - R.BR. LOKACIJE U VEKI OD KOJE POCINJU SIMBOLI
      NSYM=NSYM+1  
C
C     OSTAVLJAJU SE PRVA DVA MESTA PRAZNA U VEKI
C     ZBOG EVENTUALNOG UMETANJA  VAR=  
C
      IVEK=NSYM+2  
       IF(IVEK.GT.72) THEN 
           CALL PORUKA(8,0)
           GOTO 99 
       ENDIF
C
      VEKI(IVEK)=ITIP  
      IF(ITIP.EQ.INT9) GOTO 20 
      IF(ITIP.EQ.ID9) CALL SID 
C
C     ZAMENI ID SA NJEGOVOM ADRESOM U TABELI IDPAR ILI IDVAR
C
      VEKA(IVEK)=EQID(1) 
      GOTO 2
C
20    RVEKA(IVEK)=RNUM 
      GOTO 2
C
98    LERR=1
99     CONTINUE
C
      RETURN
      END  
