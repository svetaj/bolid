         SUBROUTINE DRFHAT
C=================================================================
C1  UBACIVANJE SRAFIRANJA U TABELU
C
C   ULAZ:
C
C     KOMON COMPOL   
C
C   IZLAZ:
C
C     KOMON COMDRF
C-----------------------------------------------------------------
C $Header: drfhat.f,v 1.2 90/02/05 13:19:48 sveta Exp $
C $Log:	drfhat.f,v $
C---> Revision 1.2  90/02/05  13:19:48  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/06/21  11:43:47  sveta
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMPOL.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'EROR.COM'
C
      IERR=0
      IAROLD=IARGI
C
      IELI=IELI+1  
      IF(IELI.GT.IELMAX)THEN  
         CALL PORUKA(219,1)
         CALL PRTOUT(1,IELMAX,0,' ',0)
         IERR=1
         GOTO 99  
      ENDIF
C
      IF((IARGI+ILMAX+IXYPM*2+5).GT.IARMAX)THEN 
         CALL PORUKA(220,1)
         CALL PRTOUT(1,IARMAX,0,' ',0)
         IERR=1
         GOTO 99  
      ENDIF
C
      IELTYP(IELI)=16
      IELPOI(IELI)=IARGI+1 
      IELMA(IELI)=0
      IELCOL(IELI)=ILIN
      IELSTY(IELI)=ISLIN
      IELLAY(IELI)=ILAY
      IELSEG(1,IELI)=0
      IELSEG(2,IELI)=0
C-------------------------------------------------------------
      IARGI=IARGI+1
      ARGEL(IARGI)=FLOAT(ILOOP)
      ICP=0
      DO 41 I=1,ILOOP
          IARGI=IARGI+1
          ARGEL(IARGI)=FLOAT(INL(I))
          ICP=ICP+INL(I)
41    CONTINUE 
C
      IARGI=IARGI+1
      ARGEL(IARGI)=FLOAT(IFA)
      IARGI=IARGI+1
      ARGEL(IARGI)=FLOAT(IBRD)
      IARGI=IARGI+1
      ARGEL(IARGI)=UDANG
      IARGI=IARGI+1
      ARGEL(IARGI)=UDDST
C
      DO 42 I=1,ICP
          IARGI=IARGI+1
          ARGEL(IARGI)=XPOL(I)
          IARGI=IARGI+1
          ARGEL(IARGI)=YPOL(I)
42    CONTINUE
C-------------------------------------------------------------
      IELNR(IELI)=IARGI-IAROLD
      PIV(1,IELI)=XPOL(1)
      PIV(2,IELI)=YPOL(1)
      CALL PUTGRP(IELI)
C
99    RETURN
      END  
