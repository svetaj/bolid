      SUBROUTINE RSTKEY(NFUNCT,LEVEL)  
C=========================================================
C1  RUTINA ZA RESETOVANJE FUNKCIJSKIH TIPKI  
C
C  ULAZNE PROMENLJIVE:
C
C     NFUNCT   =  REDNI BROJ FUNKCIJSKE TIPKE (NPR.1 ZA F1)  
C     LEVEL    =  PRITISNUTA: 
C                 1 - SAMO FUNKCIJSKA TIPKA
C                 2 - SHIFT I FUNKCIJSKA
C                 3 - CTRL I FUNKCIJSKA
C                 4 - SHIFT,CTRL I FUNKCIJSKA  
C                 5 - ONLY KEYPAD KEY  
C                 6 - SHIFT AND KEYPAD KEY 
C                 7 - CTRL AND KEYPAD KEY  
C                 8 - SHIFT,CTRL AND KEYPAD KEY
C---------------------------------------------------------
C $Header: rstkey.f,v 1.3 90/02/12 11:27:41 sveta Exp $
C $Log:	rstkey.f,v $
C---> Revision 1.3  90/02/12  11:27:41  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  88/04/06  14:10:14  vera
C---> WRITE ---> PORUKA!!!
C---> 
C---> Revision 1.1  87/04/09  11:23:54  joca
C---> Initial revision
C---> 
C=========================================================
C  
      CHARACTER*1 FRESET
C  
      IF(LEVEL.LT.1.OR.LEVEL.GT.8)GO TO 555
      IF(LEVEL.LT.5.AND.(NFUNCT.LT.1.OR.NFUNCT.GT.8))GO TO 555 
      IF(LEVEL.GT.4.AND.(NFUNCT.LT.0.OR.NFUNCT.GT.14))GO TO 555
      GO TO(10,20,30,40,50,60,70,80)LEVEL  
C  
C     UNSHIFTED
C  
10    IFUN=127+NFUNCT  
      GO TO 100
C  
C     SHIFTED  
C  
20    IFUN=135+NFUNCT  
      GO TO 100
C  
C     CTRL 
C  
30    IFUN=-(1+NFUNCT) 
      GO TO 100
C  
C     CTRL-SHIFT
C  
40    IFUN=-(9+NFUNCT) 
      GO TO 100
C  
C     UNSHIFTED KEYPAD KEY 
C  
50    IFUN=-(55+NFUNCT)
      GO TO 100
C  
C     SHIFTED KEYPAD KEY
C  
60    IFUN=-(69+NFUNCT)
      GO TO 100
C  
C     CTRL KEYPAD KEY  
C  
70    IFUN=-(83+NFUNCT)
      GO TO 100
C  
C     CTRL-SHIFT KEYPAD KEY
C  
80    IFUN=-(97+NFUNCT)
      GO TO 100
C  
C     PORUKA O GRESCI  
C  
555   CONTINUE
      CALL PORUKA(579,0)
      GO TO 999
C  
100   CALL LLDMAC(IFUN,0,FRESET)
999   RETURN
      END  
