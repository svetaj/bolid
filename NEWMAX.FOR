      SUBROUTINE NEWMAX(IE,IR,IEX,INDSTA)
C=====================================================================
C1    NA POZIV DODELJUJE SLOBODNI ENTRY U MATH-FILEU
C
C   ULAZ:
C
C      MAT BAZA PODATAKA
C
C   IZLAZNE PROMENLJIVE:
C
C      IR     = ADRESA MATX-BUFFERA U MATX-DATOTECI
C      IE     = REDNI BROJ ENTRY-A U MATX-BUFFERU
C      IEX    = REDNI BROJ ENTRY-A U MATX-DATOTECI,IEX=(IR-1)*NXMA+IE
C      INDSTA = INDIKATOR DA LI JE ENTRY DODELJEN IZ STEKA
C---------------------------------------------------------------------
C $Header: newmax.f,v 1.1 90/02/15 11:41:17 sveta Exp $
C $Log:	newmax.f,v $
C---> Revision 1.1  90/02/15  11:41:17  sveta
C---> Initial revision
C---> 
C=====================================================================
C
      INCLUDE 'CONFIG.COM'
      INCLUDE 'COMSTA.COM'
      INCLUDE 'COMMAX.COM'
C
1     CONTINUE
      IF(NSTAMX.GT.0) THEN
C-----------------------------
C  IMA OSLOBODJENIH ENTRY-a U STACK-u
C  UZIMA SE PO FIFO PRINCIPU
         IEX=MXSTAC(4)
         NSTAMX=NSTAMX-1
C  SAZIMANJE
         DO 11 I=1,NSTAMX
11       MXSTAC(I+3)=MXSTAC(I+4)
         MXSTAC(NSTAMX+4)=0
         INDSTA=1
      ELSE
C-----------------------------
C  DA LI IMA SLOBODNIH ENTRIJA U LINKOVANOM LANCU ?
         IF (LASTEY.NE.0) THEN
            CALL FETMAX
            GOTO 1
         ENDIF
C  NEMA OBRISANIH OSLOBODJENIH ENTRIJA
C  DODELJUJE SE SLEDECI ENTRY  PO REDU
         NMAE=NMAE+1
         IEX=NMAE
         INDSTA=0
      ENDIF
C-----------------------------
C VERIFY THAT IR IS A LEGAL RECORD ( IF NOT GET A NEW ONE )
C
      IF (IEX.LE.0.OR.IEX.GT.NMAE) THEN
         CALL PORUKA(1664,0)
         GOTO 1
      ENDIF
C
      IR=(IEX-1)/NXMA+2
      IE=IEX-(IR-2)*NXMA
C
      RETURN
      END
