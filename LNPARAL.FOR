      SUBROUTINE LNPARAL(X1,Y1,X2,Y2,O,XO1,YO1,XO2,YO2)
C===============================================================
C2  NALAZI DUZ KOJA JE PARALELNA ZADATOJ DUZI TAKO DA SE DUZI
C2  NALAZE NA ZADATOM RASTOJANJU. DUZI SU ISTE DUZINE.
C2  /PERINA RUTINA !?!?!/
C
C   ULAZNE PROMENLJIVE:
C
C      X1,Y1      = POCETNA TACKA DUZI
C      X2,Y2      = KRAJNJA TACKA DUZI
C      O          = VELICINA OFSETA 
C                   (NORMALNOG RASTOJANJA)
C                   AKO JE O>0 - OFSET JE POZITIVAN
C                          O<0 - OFSET JE NEGATIVAN
C                          O=0 - NEMA OFSETA
C
C    IZLAZNE PROMENLJIVE:
C
C       (XO1,YO1),
C       (XO2,YO2) = TRAZENA DUZ
C
C    POZIVI POTPROGRAMA:
C
C       LIND      = IZRACUNAVA TACKE DVE DUZI SA JEDNIM
C                   ZAJEDNICIKIM TEMENOM
C---------------------------------------------------------------
C $Header: lnparal.f,v 1.2 90/02/07 14:14:32 sveta Exp $
C $Log:	lnparal.f,v $
C---> Revision 1.2  90/02/07  14:14:32  sveta
C---> ispravljeno zaglavlje
C---> 
C===============================================================
C
       angle=-90
       call lind(x1,y1,x2,y2,o,angle,xo1,yo1,ind)
       angle=90
       call lind(x2,y2,x1,y1,o,angle,xo2,yo2,ind)
        
100     return
        end
