      SUBROUTINE GLTAA(XC1,YC1,XP1,YP1,XK1,YK1,
     *                 XC2,YC2,XP2,YP2,XK2,YK2,IND)
C========================================================================
C2  TESTIRA DA LI JE SPOJ DVA LUKA GLADAK ( GLATKOST SAMO U SLUCAJU
C2  DA SE DVA LUKA TANGIRAJU )
C
C  ULAZNE PROMENLJIVE:
C
C   XC1,YC1     = CENTAR PRVOG LUKA
C   XP1,YP1     = POCETNA TACKA PRVOG LUKA
C   XK1,YK1     = KRAJNJA TACKA PRVOG LUKA
C   XC2,YC2     = CENTAR DRUGOG LUKA
C   XP2,YP2     = POCETNA TACKA DRUGOG LUKA
C   XK2,YK2     = KRAJNJA TACKA DRUGOG LUKA
C
C  IZLAZNE PROMENLJIVE:
C
C   IND         = INDIKATOR GLATKOSTI  ; 0 - NIJE GLADAK PRELAZ
C                                        1 - PRELAZ JE GLADAK
C
C  POZIVI PODPROGRAMA:
C
C   PARLIN      = PRONALAZI DUZ KOJA JE PARALELNA DATOJ
C   LINB        = PRONALAZI DUZ KOJA JE NORMALNA NA DATU
C   DIST        = PRONALAZI RASTOJANJE DVE DUZI
C------------------------------------------------------------------------
C $Header: gltaa.f,v 1.2 90/02/06 12:17:36 sveta Exp $
C $Log:	gltaa.f,v $
C---> Revision 1.2  90/02/06  12:17:36  sveta
C---> ispravljeno zaglavlje
C---> 
C========================================================================
C
      DIMENSION XB1(2),YB1(2),XB2(2),YB2(2),INDS(2)
      DATA DST/1.E-2/,ACCY/1.E-5/
C
C RACUNA POLUPRECNIKE LUKOVA
C
      CALL DIST(XC1,YC1,XP1,YP1,R1)
      CALL DIST(XC2,YC2,XP2,YP2,R2)
      RMAX=R1
      IF(R2.GT.R1) RMAX=R2
C
C NALAZENJE TEST DUZI
C
      CALL DIST(XC1,YC1,XC2,YC2,R12)
C
C SLUCAJ #1 - JEDNOSTAVNO SPOJIMO 2 CENTRA
C
      IF(R12.GT.RMAX) THEN
         CALL PARLIN(XC1,YC1,XC2,YC2,DST,XB1,YB1,XB2,YB2,IND)
      ELSE
C
C SLUCAJ #2 - SPOJIMO ZAJEDNICKU KRAJNJU TACKU I BLIZI CENTAR
C
C ODREDJIVANJE ZAJEDNICKE TACKE
C
         CALL DIST(XP1,YP1,XP2,YP2,DSTPP)
         IF(DSTPP.LT.ACCY) THEN
            XPAR1=XP1
            YPAR1=YP1
         ELSE
            CALL DIST(XK1,YK1,XK2,YK2,DSTKK)
            IF(DSTKK.LT.ACCY) THEN
               XPAR1=XK1
               YPAR1=YK1
            ELSE
               CALL DIST(XP1,YP1,XK2,YK2,DSTPK)
               IF(DSTPK.LT.ACCY) THEN
                  XPAR1=XP1
                  YPAR1=YP1
               ELSE
                  XPAR1=XK1
                  YPAR1=YK1
               ENDIF
            ENDIF
         ENDIF
C
C ODREDJIVANJE KOJI JE CENTAR BLIZI
C
         CALL DIST(XPAR1,YPAR1,XC1,YC1,DST1)
         CALL DIST(XPAR1,YPAR1,XC2,YC2,DST2)
         IF(DST1.LT.DST2) THEN
            XPAR2=XC1
            YPAR2=YC1
         ELSE
            XPAR2=XC2
            YPAR2=YC2
         ENDIF
         CALL PARLIN(XPAR1,YPAR1,XPAR2,YPAR2,DST,XB1,YB1,XB2,YB2,IND)
      ENDIF
      IF(IND.NE.2) THEN
         IND=0
         GOTO 99
      ENDIF
C
      DO 10 I=1,2
         XL1=XB1(I)
         YL1=YB1(I)
         XL2=XB2(I)
         YL2=YB2(I)
         CALL INTLSA(XL1,YL1,XL2,YL2,XC1,YC1,XP1,YP1,XK1,YK1,
     *               XPR1,YPR1,XPR2,YPR2,IND1)
         CALL INTLSA(XL1,YL1,XL2,YL2,XC2,YC2,XP2,YP2,XK2,YK2,
     *               XPR1,YPR1,XPR2,YPR2,IND2)
         INDS(I)=IND1+IND2
10    CONTINUE
      IF(INDS(1).EQ.1.AND.INDS(2).EQ.1) THEN
C        GLADAK PRELAZ
         IND=1
      ELSE
         IND=0
      ENDIF
C
C
99    RETURN
      END
