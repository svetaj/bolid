      COMMON /COMMFX/NMAE,IARMA,IAWMA,NAWMA,ISEQMA,LUNMAT,LUNMAX,
     *NENMA,NXMA,MAFXB(5,56)
C
      COMMON /FLMAT/ LASTRC,LASTEY,IVALKY,ISIGT,ISIGX
      INTEGER        LASTRC,LASTEY,IVALKY,ISIGT,ISIGX
      CHARACTER*4    EQVALK,EQSIGT,EQSIGX
      EQUIVALENCE    (IVALKY,EQVALK)
      EQUIVALENCE    (ISIGT,EQSIGT)
      EQUIVALENCE    (ISIGX,EQSIGX)
C=======================================================================
C1 KOMON KOJI PREDSTAVLJA BUFFER ZA ADRESE PRI I/O IZ BAZE PODATAKA
C1 MAKROA
C
C OPIS PROMENLJIVIH:
C
C NMAE     = NUMBER OF ENTRIES IN X-FILE
C ISEQMA   = NUMBER OF RECORD STORED IN MAFXB
C NAWMA    = LAST OCCUPIED RECORD IN T-FILE
C LUNMAT   = LOGICAL UNIT OF T-FILE
C LUNMAX   = LOGICAL UNIT OF X-FILE
C NENMA    = POINTER TO AN ENTRY IN MAFXB BUFFER
C NXMA     = DIMENSION OF X RECORD
C MAFXB    = RECORD FROM X-FILE
C
C LASTRC  = RECORD WITH THE LIST OF FREE RECORDS (LINK)
C LASTEY  = RECORD WITH THE LIST OF FREE ENTRIES (LINK)
C IVALKY  = VALIDATION KEYWORD
C ISIGX   = SIGNATURE KEYWORD FOR "X" FILE
C ISIGT   = SIGNATURE KEYWORD FOR "T" FILE
C----------------------------------------------------------------------
C $Header: COMMFX.COM,v 1.3 90/02/01 15:55:29 sveta Exp $
C $Log:	COMMFX.COM,v $
C---> Revision 1.3  90/02/01  15:55:29  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  88/05/23  11:07:05  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.10  88/02/24  06:50:46  joca
C---> Prosirenje slobodnih lista !
C---> 
C---> Revision 1.9  88/01/07  15:41:20  joca
C---> Configuration file is introduced !
C---> 
C---> Revision 1.8  87/03/18  14:25:45  joca
C---> Izbacena IRDO zastavica iz ovog komona i stavljena u PROTCT !
C---> 
C---> Revision 1.7  87/02/07  14:11:34  vera
C---> Ubacen je read/write I/O indikator !!
C---> 
C======================================================================
