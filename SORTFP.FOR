        SUBROUTINE SORTFP(FPARR,NDIM)
C==================================================================
C2 SORTIRA KOMPONENTE REALNOG VEKTORA
C2 /PERINA RUTINA !!!/
C
C  ULAZNE PROMENLJIVE:
C
C      FPARR = REALNI VEKTOR KOJI SORTIRAMO
C      NDIM  = DIMENZIJA VEKTORA
C
C  IZLAZNE PROMENLJIVE:
C
C      FPARR = SORTIRANI VEKTOR
C------------------------------------------------------------------
C $Header: sortfp.f,v 1.2 90/02/12 16:04:44 sveta Exp $
C $Log:	sortfp.f,v $
C---> Revision 1.2  90/02/12  16:04:44  sveta
C---> ispravljeno zaglavlje
C---> 
C==================================================================
C 
        real fparr(1)
        do 10 i=1,ndim-1
                k=i
                ff=fparr(i)
                do 20 j=i+1,ndim
                        if(fparr(j).lt.ff)then
                                 k=j
                                 ff=fparr(k)
                        end if
20              continue
                fparr(k)=fparr(i)
                fparr(i)=ff
10      continue
        return
        end 
