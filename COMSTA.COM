      COMMON/COMSTA/NSTAMA,MASTAC(280),NSTAMX,MXSTAC(280)               
C======================================================================
C1 FREE STACKS FOR I/O OPERATIONS ON MAT FILES
C
C OPIS PROMENLJIVIH:
C
C   NSTAMA     = BROJ ZAUZETIH ENTRY-A U MASTAC-U
C   MASTAC     = VEKTOR SA BROJEVIMA SLOBODNIH REKORADA U T-FAJLU
C   NSTAMX     = BROJ ZAUZETIH ENTRY-A U MXSTAC-U
C   MXSTAC     = VEKTOR SA BROJEVIMA SLOBODNIH REKORADA U X-FAJLU
C----------------------------------------------------------------------
C $Header: COMSTA.COM,v 1.3 90/02/01 15:55:54 sveta Exp $
C $Log:	COMSTA.COM,v $
C---> Revision 1.3  90/02/01  15:55:54  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.2  88/05/23  11:07:23  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.7  88/01/07  15:43:32  joca
C---> Configuration file is introduced !
C---> 
C======================================================================
