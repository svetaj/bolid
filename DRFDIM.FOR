      SUBROUTINE DRFDIM(X1,Y1,X2,Y2,XK,YK,DU)
C======================================================================
C1   SMESTANJE PRIMITIVE DIMENSION U TABELU
C1   (ARHITEKTONSKO KOTIRANJE)
C
C ULAZNE PROMENLJIVE:
C
C   IAFON1            =  FONT LEVE STRELICE
C   IAFON2            =  FONT DESNE STRELICE
C   IKFONT            =  FONT KOTNE LINIJE
C   ITFONT            =  FONT TEKSTA
C   SX,SY             =  PARAMETRI STRELICE
C   HC                =  VISINA TEKSTA
C   (X1,Y1), (X2,Y2)  =  REFERENTNE KOTNE TACKE
C   DU                =  UDALJENOST KOTNE LINIJE OD REFERENTNIH
C                        TACAKA
C   (XK,YK)           =  MESTO ISPISIVANJA TEKSTA
C
C IZLAZ:
C
C   KOMON COMDRF
C-----------------------------------------------------------------------
C $Header: drfdim.f,v 1.8 90/02/05 13:19:42 sveta Exp $
C $Log:	drfdim.f,v $
C---> Revision 1.8  90/02/05  13:19:42  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.7  88/04/06  13:13:23  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.6  88/02/25  16:17:16  sveta
C---> ubaceno definisanje grupa pomocu GOPEN i GCLOSE
C---> 
C---> Revision 1.5  87/11/12  11:52:56  sveta
C---> ??
C---> 
C---> Revision 1.4  87/10/06  17:06:15  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.3  87/06/10  08:13:07  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.2  87/05/25  13:52:50  sveta
C---> ????
C---> 
C=======================================================================
C  
      INCLUDE 'COMDIM.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'EROR.COM'
C
      IERR=0
      IELI=IELI+1  
      IF(IELI.GT.IELMAX) THEN  
         CALL PORUKA(219,1)
         CALL PRTOUT(1,IELMAX,0,' ',0)
         IERR=1
         GOTO 99  
      ENDIF
C
      IF((IARGI+15).GT.IARMAX)THEN  
         CALL PORUKA(220,1)
         CALL PRTOUT(1,IARMAX,0,' ',0)
         IERR=1
         GOTO 99  
      ENDIF
C
      IELTYP(IELI)=12  
      IELPOI(IELI)=IARGI+1 
      IELNR(IELI)=15  
      IELMA(IELI)=0
      IELCOL(IELI)=ILIN
      IELSTY(IELI)=ISLIN
      IELLAY(IELI)=ILAY
      IELSEG(1,IELI)=0
      IELSEG(2,IELI)=0
      PIV(1,IELI)=X1
      PIV(2,IELI)=Y1
C
      ITF=ITFONT
      IF(INAC.GE.3)ITF=-ITF
      ARGEL(IARGI+1)=FLOAT(IANGUL)
      ARGEL(IARGI+2)=FLOAT(IAFON1)
      ARGEL(IARGI+3)=FLOAT(IAFON2)
      ARGEL(IARGI+4)=FLOAT(IKFONT)
      ARGEL(IARGI+5)=FLOAT(ITF)
      ARGEL(IARGI+6)=SX
      ARGEL(IARGI+7)=SY
      ARGEL(IARGI+8)=HC
      ARGEL(IARGI+9)=X1
      ARGEL(IARGI+10)=Y1
      ARGEL(IARGI+11)=X2
      ARGEL(IARGI+12)=Y2
      ARGEL(IARGI+13)=XK
      ARGEL(IARGI+14)=YK
      ARGEL(IARGI+15)=DU
      IARGI=IARGI+15
      CALL PUTGRP(IELI)
C
99    RETURN
      END  
