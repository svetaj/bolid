      SUBROUTINE PTINT(X,Y)
C====================================================================
C1 TRAZI PRESECNU TACKU DVE PRIMITIVE
C1 /PERINA RUTINA !!!/ 
C1  NE KORISTI SE
C
C   ULAZ:
C
C     USER INTERFACE
C     KOMON COMDRF
C
C   IZLAZNE PROMENLJIVE:
C
C     X,Y      = KOORDINATE PRESECNE TACKE DVA ENTITETA
C
C   POZIVI PODPROGRAMA:
C
C     PICKPE   = POKAZIVANJE PRIMITIVE NA EKRANU
C--------------------------------------------------------------------
C $Header: ptint.f,v 1.3 90/02/09 12:25:26 sveta Exp $
C $Log:	ptint.f,v $
C---> Revision 1.3  90/02/09  12:25:26  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  89/12/06  12:47:40  sveta
C---> ??
C---> 
C---> Revision 1.1  89/03/30  08:31:08  vera
C---> Initial revision
C---> 
C====================================================================
C
        include 'COMDRF.COM'
        include 'COMVAR.COM'
        include 'OZNAKE.COM'
        include 'TTYPE.COM'
        include 'ptcom.com'
        dimension distn(3),xk(2),yk(2),xl(2),yl(2),xc(2),yc(2),itt(2)

        character*1 nkey

        do 10 i=1,2
        if(i.eq.1)call poruka(ibpt(21),0)
        if(i.eq.2)call poruka(ibpt(22),0)
        call pickpe(xff,yff,ie)
        it=ieltyp(ie)
C
C ako je selektirana duz
C
        ip=ielpoi(ie)
        if(it.eq.1)then
                itt(i)=1
                xk(i)=argel(ip)
                yk(i)=argel(ip+1)
                xl(i)=argel(ip+2)
                yl(i)=argel(ip+3)
                goto 10
        end if
c
C ako je selektiran luk
C
        if(it.eq.5)then
                itt(i)=5
                xc(i)=argel(ip)
                yc(i)=argel(ip+1)
                xk(i)=argel(ip+2)
                yk(i)=argel(ip+3)
                xl(i)=argel(ip+4)
                yl(i)=argel(ip+5)
                goto 10
        end if
C
C   ako je izabrana kruznica
C
        if(it.eq.4)then
                xc(i)=argel(ip)
                yc(i)=argel(ip+1)
                r=argel(ip+2)
                call citoar(xc(i),yc(i),r,xk(i),yk(i),xl(i),yl(i))
                itt(i)=5
        end if
C
C sredisna tacka duzi ili tacka luka/kruznice na nultom stepenu
C
10      continue
C
C odredjivanje kombinacija
C
C   1 --- linija/linija
c
        if(itt(1).eq.1.and.itt(2).eq.1)ncom=1
C   2 --- linija/luk
        if(itt(1).eq.1.and.itt(2).eq.5)ncom=2
        if(itt(1).eq.5.and.itt(2).eq.1)ncom=2
C   3 --- luk/luk
        if(itt(1).eq.5.and.itt(2).eq.5)ncom=3
C   4 --- linija/konik
C   5 --- luk/konik
C   6 --- konik/konik
C   7 --- linija/splajn
C   8 --- luk/splajn
C   9 --- konik/splajn
C  10 --- splajn/splajn
c        print *,'itt1,itt2,ncom',itt(1),itt(2),ncom
C ------------------------------------------------------------
C
C presek linija/linija
C
          if(ncom.eq.1)then
                 call xyabc(xk(1),yk(1),xl(1),yl(1),a1,b1,c1,ind99)
                 call xyabc(xk(2),yk(2),xl(2),yl(2),a2,b2,c2,ind99)
                 call int2li(a1,b1,c1,a2,b2,c2,x,y,ind98)
                 goto 999
          end if
C
C presek linija/luk
C
          if(ncom.eq.2)then
              if(itt(1).eq.1)then
                         xkl=xk(1)
                         ykl=yk(1)
                         xll=xl(1)
                         yll=yl(1)
                         xkk=xk(2)
                         ykk=yk(2)
                         xlk=xl(2)
                         ylk=yl(2)
                         xck=xc(2)
                         yck=yc(2)
              else
                         xkk=xk(1)
                         ykk=yk(1)
                         xlk=xl(1)
                         ylk=yl(1)
                         xck=xc(1)
                         yck=yc(1)
                         xkl=xk(2)
                         ykl=yk(2)
                         xll=xl(2)
                         yll=yl(2)
              end if
              call xyabc(xkl,ykl,xll,yll,a,b,c,ind99)
              call abcfg(a,b,c,x0,f,y0,g,IX)
              call dist(xck,yck,xlk,ylk,rr)
              call intlc(x0,f,y0,g,xck,yck,rr,x9,y9,x99,y99,ind3)
C
C  izbor najblize tacke
C
              call ptnear(x9,y9,x99,y99,xff,yff,ind3,x,y,ind1)
              if(ind1.eq.1)call poruka(ibpt(20),0)
          end if
C
C   presek luk/luk
C
          if(ncom.eq.3)then
               call dist(xc(1),yc(1),xk(1),yk(1),dist1)
               call dist(xc(2),yc(2),xk(2),yk(2),dist2)
               call circa(xc(1),yc(1),xc(2),yc(2),dist1,dist2,x9,y9,
     1x99,y99,rrr,ind3)
              call ptnear(x9,y9,x99,y99,xff,yff,ind3,x,y,ind1)
              if(ind1.eq.1)call poruka(ibpt(20),0)
          end if
999       return
          end
