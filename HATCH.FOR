      SUBROUTINE HATCH(ITYPE,IBORD,ISTY,ICOL,ANGLE,DST)
C===============================================================
C1  CRTA SRAFURU SLOZENOG POLIGONA ZADATOG U COMMON-U COMPOL
C
C  ULAZNE PROMENLJIVE:
C
C    ITYPE  = TIP SRAFURE /999 - USER DEFINED/
C    IBORD  = 0 - OKVIR SE NE CRTA
C             1 - CRTANJE OKVIRA
C    ISTY   = STIL CRTANJA LINIJA
C    ICOL   = BOJA CRTANJA LINIJA
C    ANGLE  = UGAO SRAFURE SA X-OSOM
C    DST    = RASTOJANJE SRAFURNIH LINIJA
C
C  ULAZ:
C
C    COMPOL = KOMON KOJI SADRZI DEFINICIJU POLIGONA / SA RUPAMA/
C   
C  IZLAZ:
C
C    TERMINAL,PLOTER
C---------------------------------------------------------------
C  $Header: hatch.f,v 1.3 90/02/07 09:31:50 sveta Exp $
C   $Log:	hatch.f,v $
C---> Revision 1.3  90/02/07  09:31:50  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  89/07/20  12:46:50  sveta
C---> ??
C---> 
C===============================================================
C
      INCLUDE 'COMGRI.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMPOL.COM'
      DIMENSION ITEMP(ILMAX)
      DATA ACCY/1.E-5/
C
C USER DEFINED HATCH
C
      IF(ITYPE.EQ.999)THEN
          CALL PPLNST(ISTY)
          CALL PPLNIN(ICOL)
          CALL SRAF(ANGLE,DST*FDS/FS)  
C
C CRTANJE OKVIRA
C
          IF(IBORD.EQ.1)THEN
              K=0
              DO 20 I=1,ILOOP
                 K=K+1
                 KT1=K
                 CALL MOVE(XPOL(K),YPOL(K))
                 DO 10 J=2,INL(I)
                     K=K+1
                     CALL DRAW(XPOL(K),YPOL(K))
10               CONTINUE
                 KT2=INL(I)
                 CALL DIST(XPOL(KT1),YPOL(KT1),XPOL(KT2),YPOL(KT2),DS)
                 IF(DS.GT.ACCY)CALL DRAW(XPOL(KT1),YPOL(KT1))
20            CONTINUE
          ENDIF
      ELSE
C
C HARDVERSKO SRAFIRANJE
C
          CALL PPLNST(ISTY)
          CALL PPLNIN(ICOL)
          CALL PPSFIL(ITYPE) 
C
          K=0
          DO 40 I=1,ILOOP
             K=K+1
             CALL PPBPNL(XPOL(K)*FS+X0,YPOL(K)*FS+Y0,IBORD)
             DO 30 J=2,INL(I)
                K=K+1
                CALL DRAW(XPOL(K),YPOL(K))
30           CONTINUE
40       CONTINUE
C
         CALL PPEPNL
      ENDIF
C
99    RETURN
      END
