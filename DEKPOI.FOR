      SUBROUTINE DEKPOI
C=================================================================
C1   GLAVNA RUTINA ZA KREIRANJE TACKE
C
C   ULAZ:
C
C      USER INTERFACE
C      KOMON COMFIL
C
C    IZLAZ:
C
C      KOMON COMDRF
C      EKRAN
C-----------------------------------------------------------------
C $Header: dekpoi.f,v 1.11 90/03/21 11:35:59 sveta Exp $
C $Log:	dekpoi.f,v $
C---> Revision 1.11  90/03/21  11:35:59  sveta
C---> ubacen interpreter
C---> 
C---> Revision 1.10  90/02/05  10:22:04  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.9  90/01/29  14:50:39  sveta
C---> ubacen UNDO
C---> 
C---> Revision 1.8  89/06/05  12:54:39  sveta
C---> vraceno stanje pre nego sto je Pera menjao.
C---> 
C---> Revision 1.6  88/04/11  11:34:16  vera
C---> MSG --->  PORUKA!!!
C---> 
C---> Revision 1.5  88/04/06  10:48:43  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.4  87/11/12  11:52:08  sveta
C---> ??
C---> 
C---> Revision 1.3  87/10/05  11:38:48  sveta
C---> ???
C---> 
C---> Revision 1.2  87/05/25  13:49:30  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  07:58:33  joca
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDO.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMDRM.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'EROR.COM'
      DIMENSION IVEC(1)
      CHARACTER NKEY
C  
      IF(IWTEK.EQ.1)THEN
         CALL WIDTH(3)
         CALL PORUKA(137,0)
         CALL PORUKA(138,0)
         GOTO 99
      ENDIF
C
C INTERPRETERSKI DEO
C
      CALL VARG
      IF(ITIP.NE.2)THEN
         IF(IDOFLA.EQ.1)GOTO 99
         GOTO 1
      ENDIF
      X=RNUM
      CALL VARG
      IF(ITIP.NE.2)GOTO 99
      Y=RNUM
      CALL UNDO(2,0,IVEC)
      CALL DRFPOI(X,Y)
      CALL SCRAZU(IELI)
      GOTO 99
C  
1     IPOSEL=0 
      IF(IPOSEL.NE.0)GOTO 15
      CALL PORUKA(69,0)
      CALL CURSXY(NKEY,X,Y)
      IF(NKEY.EQ.'E')GOTO 99
      IF(NKEY.EQ.'Q')GOTO 99
C
      IF(IND.LT.0)GOTO 99  
      X1=X 
      Y1=Y 
      GOTO 18  
15    CALL PORUKA(550,0)
      CALL READL1
      CALL VARG 
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 99
      ENDIF
      IF(ITIP.NE.2)GOTO 15 
      X1=RNUM  
C
151   CALL VARG 
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 99
      ENDIF
      IF(ITIP.NE.2)THEN
         CALL READL1  
         GOTO 151 
      ENDIF
C
      Y1=RNUM  
C
18    CALL UNDO(1,0,IVEC)
      CALL DRFPOI(X1,Y1)
      CALL SCRAZU(IELI)
      CALL COMAZU(IELI,IELI)
      GO TO 1
C
99    RETURN
      END  
