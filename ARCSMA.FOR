          subroutine arcsma(x1,y1,x2,y2,xc,yc,xp1,yp1,xp2,yp2,ind)
C=========================================================================
C2  LUK ZADAT CENTROM I DVEMA TACKAMA UREDJUJE TAKO DA IZRACUNA MANJI
C
C  ULAZNE PROMENLJIVE:
C
C    x1,y1   = prva tacka na luku (smer nije bitan)
C    x2,y2   = druga tacka na luku (smer nije bitan)
C    xc,yc   = centar luka
C
C  IZLAZNE PROMENLJIVE:
C
C    xp1,yp1 = prva tacka na luku u CCW smeru
C    xp2,yp2 = druga tacka na luku u CCW smeru
C    ind     = indeks postojanosti resenja
C                           0 - ne postoji resenje 
C                              (prva i druga tacka
C                               se preklapaju)
C                           1 - postoji resenje
C                           2 - 180 stepeni
C------------------------------------------------------------------------
C $Header: arcsma.f,v 1.2 90/02/02 10:13:36 sveta Exp $
C $Log:	arcsma.f,v $
C---> Revision 1.2  90/02/02  10:13:36  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  89/07/20  12:32:10  sveta
C---> Initial revision
C---> 
C=========================================================================
          ind=1
          indd=0
          if(x1.eq.x2.and.y1.eq.y2)then
               ind=0
               goto 999
          end if
c
c   ugao koji zaklapaju dve tacke na luku
c
          call arcang(x1,y1,x2,y2,xc,yc,ang)
          ang=anint(ang)
c
c 180 stepeni
c
          if(ang.eq.180)then
                 ind=2
                 indd=0
                 goto 10
          end if
          if(ang.eq.-180)then
                 ind=2
                 indd=0
                 goto 10
          end if
          if(ang.lt.0.and.ang.gt.-180)then
                 indd=1
          else if(ang.gt.180.and.ang.lt.360)then
                 indd=1
          else if(ang.lt.-180.and.ang.gt.-360)then
                 indd=0
          end if
10        if(indd.eq.0)then
                 xp1=x1
                 yp1=y1
                 xp2=x2
                 yp2=y2
          else
                 xp1=x2
                 yp1=y2
                 xp2=x1
                 yp2=y1
          end if
999       return
          end
