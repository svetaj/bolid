      SUBROUTINE VERIFY 
C=======================================================
C1 VERIFIKOVANJE (STAMPANJE) OSOBINA DATE PRIMITIVE
C1 /TACKA, DUZ, LUK/
C
C  ULAZ:
C
C     USER INTERFACE
C
C   IZLAZ:
C
C     EKRAN
C-------------------------------------------------------
C $Header: verify.f,v 1.2 90/02/13 12:54:33 sveta Exp $
C $Log:	verify.f,v $
C---> Revision 1.2  90/02/13  12:54:33  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  89/10/11  11:55:36  sveta
C---> Initial revision
C---> 
C=======================================================
C
       INCLUDE 'COMDRF.COM'
C
1      CALL WIDTH(2)
       PRINT *,' PICK THE PRIMITIVE'
       CALL PICKPE(X,Y,IE)
       IF(IE.EQ.0)GOTO 1
       IF(IE.EQ.-1)GOTO 99
C
       IT=IELTYP(IE)
       IP=IELPOI(IE)
C
C DUZ
C
       IF(IT.EQ.1)THEN
          X1=ARGEL(IP)
          Y1=ARGEL(IP+1)
          X2=ARGEL(IP+2)
          Y2=ARGEL(IP+3)
          CALL RP1(X1,Y1,X2,Y2)
C
C LUK
C
       ELSE IF(IT.EQ.5)THEN
          X1=ARGEL(IP)
          Y1=ARGEL(IP+1)
          X2=ARGEL(IP+2)
          Y2=ARGEL(IP+3)
          X3=ARGEL(IP+4)
          Y3=ARGEL(IP+5)
          CALL RP2(X1,Y1,X2,Y2,X3,Y3)
C
C TACKA
C
       ELSE IF(IT.EQ.10)THEN
          X1=ARGEL(IP)
          Y1=ARGEL(IP+1)
          CALL RP3(X1,Y1)
       ENDIF
C
99     RETURN
       END
