      SUBROUTINE SCRAZU(IPRIM) 
C========================================================= 
C1 CRTA JEDNU PRIMITIVU IZ DRF TABELE U SEGMENTU 
C1  ( I MAKRO !)
C
C  ULAZNE PROMENLJIVE:
C
C    IPRIM   = REDNI BROJ PRIMITIVE ( MAKROA)
C
C  IZLAZ:
C
C    EKRAN
C
C  POZIVI PODPROGRAMA:
C
C   WIREL    = CRTA JEDAN ENTRY IZ TABELE CRTEZA
C---------------------------------------------------------
C $Header: scrazu.f,v 1.7 90/02/12 12:00:58 sveta Exp $
C $Log:	scrazu.f,v $
C---> Revision 1.7  90/02/12  12:00:58  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.6  88/10/13  13:15:52  sveta
C---> ubaceno zamrzavanje layer-a
C---> 
C---> Revision 1.5  88/01/07  14:04:48  sveta
C---> UVEDENA DINAMICKA DODELA BROJEVA SEGMENATA.
C---> 
C---> Revision 1.4  87/10/20  19:05:25  sveta
C---> ??
C---> 
C---> Revision 1.3  87/10/05  11:50:19  sveta
C---> ???
C---> 
C---> Revision 1.2  87/05/25  13:59:25  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  11:47:14  joca
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMSCR.COM'
      INCLUDE 'COMWIN.COM'
C  
      IF(IFREEZ(IELLAY(IPRIM)).EQ.1)GOTO 99
C
      I=IPRIM
      IWW=1
      IF(IWTEK.GT.1)IWW=2
C  
C CRTA U DATOM PROZORU SAMO ODGOVARAJUCI LAYER
C U WIND 0 CRTA SVE LAYER-E
C
1     CONTINUE
      IF(IWTEK.EQ.1)THEN
         IL=ILAY
      ELSE
         IL=IELLAY(I)
      ENDIF
      IF(IL.EQ.ILAY)THEN
C
          IT=IELTYP(I)
          IM=IELMA(I)
C
C OTVARA SEGMENT SAMO ZA PRIMITIVE KOJE NE PRIPADAJU MAKROU
C
          IF(IM.EQ.0)THEN
              IS=IELSEG(IWW,I)
              IF(IS.GT.0)CALL SSDLSG(IS)
              CALL PIVOT(PIV(1,I),PIV(2,I))
              CALL SSOPSG(IS)
              IELSEG(IWW,I)=IS
          ENDIF
C
C CRTA JEDNU PRIMITIVU IZ TABELE
C
          CALL WIREL(I)
C------------------------------------------------------------------
C NE ZATVARA SEGMENT ZA : - MAKRO
C                         - PRIMITIVE KOJE PRIPADAJU MAKROU
C                            (OSIM POSLEDNJE)
C ZATVARA SEGMENT ZA    : - OBICNE PRIMITIVE
C                         - POSLEDNJU PRIMITIVU KOJA PRIPADA MAKROU
C
          IF(IM.EQ.0)THEN
              IF(IT.EQ.14)THEN
                  IMAC1=I
                  I=I+1
                  GOTO 1
              ELSE 
                  CALL SSCLSG
              ENDIF
          ELSE
              IF(I+1.LE.IELI)THEN
                  IF(IELMA(I+1).NE.IMAC1)THEN
                     IMAC1=0
                     CALL SSCLSG
                  ELSE
                     I=I+1
                     GOTO 1
                  ENDIF
              ELSE
                  CALL SSCLSG
              ENDIF
          ENDIF
C------------------------------------------------------------------
      ENDIF
C
99    RETURN
      END
