      SUBROUTINE V2DKEY(KEY,ITIP)  
C=========================================================
C1 DEKODUJE KOMANDU IZ DRAFT MODA, VRACA REDNI BROJ
C1 KOMANDE
C1 NE KORISTI SE
C
C   ULAZNE PROMENLJIVE:
C
C     KEY   = PRVA 4 SLOVA KOMANDE
C
C   IZLAZNE PROMENLJIVE:
C
C     ITIP  = REDNI BROJ KOMANDE 
C             0 - NIJE NADJENO
C---------------------------------------------------------
C $Header: v2dkey.f,v 1.12 90/02/13 12:54:09 sveta Exp $
C $Log:	v2dkey.f,v $
C---> Revision 1.12  90/02/13  12:54:09  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.11  89/05/15  11:46:12  sveta
C---> ubacene komande ICOMPILE i ILOAD 
C---> 
C---> Revision 1.10  89/05/08  15:26:14  sveta
C---> UBACENE KOMANDE MARC MARX
C---> 
C---> Revision 1.9  89/04/24  09:07:19  sveta
C---> DODATO KREIRANJE MACRO ARCHIVE FAJLA
C---> 
C---> Revision 1.8  88/12/08  09:17:37  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.7  88/10/13  13:19:31  sveta
C---> NOVE KOMANDE ZA RAD SA LAYER-IMA
C---> 
C---> Revision 1.6  88/05/23  11:05:57  sveta
C---> dodatni IZVESTAJI
C---> 
C---> Revision 1.5  87/11/12  11:57:02  sveta
C---> ??
C---> 
C---> Revision 1.4  87/10/05  11:52:24  sveta
C---> ???
C---> 
C---> Revision 1.3  87/07/16  14:07:26  sveta
C---> ????
C---> 
C---> Revision 1.2  87/05/25  14:03:03  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  12:08:35  joca
C---> Initial revision
C---> 
C=========================================================
      CHARACTER*4 KEY,IKEY(27) 
      DATA IKEY/'END','HELP','VIEW','TABL','RESC','GO',
     *'DLOA','DCOM','DREN','DDEL','DCOP','DLIS','LAYE',
     *'DEDI','DLO1','LCOP','LVIE','LFRE','LMOV','LUNF',
     *'DARC','DARX','MARC','MARX','TOUC','ICOM','ILOA'/
C  
      NK=27
      DO 1 I=1,NK  
      IF(KEY.NE.IKEY(I)) GOTO 1
      ITIP=I
      GOTO 2
1     CONTINUE 
      ITIP=0
C
2     RETURN
      END  
