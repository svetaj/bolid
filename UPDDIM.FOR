      SUBROUTINE UPDDIM
C====================================================================
C1 AUTOMATSKO MENJANJE PARAMETARA SVIH KOTA 
C1 ZA ARHITEKTONSKO KOTIRANJE
C
C  ULAZ:
C
C     USER INTERFACE
C
C  IZLAZ:
C
C     EKRAN
C     KOMON COMDRF
C--------------------------------------------------------------------
C $Header: upddim.f,v 1.2 90/02/13 11:57:17 sveta Exp $
C $Log:	upddim.f,v $
C---> Revision 1.2  90/02/13  11:57:17  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/12/16  12:32:53  sveta
C---> Initial revision
C---> 
C====================================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMDIM.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMTXT.COM'
      INCLUDE 'EROR.COM'
      DIMENSION INDC(8)
      CHARACTER*12 AA(4)
      CHARACTER*1 NKEY
C
      INDNEW=0
      DO 33 I=1,8
         INDC(I)=0
33    CONTINUE
C
1     CALL WIDTH(15)
      CALL PORUKA(30,0)
      CALL PORUKA(31,0)
      CALL PORUKA(32,0)
      CALL PORUKA(33,0)
      CALL PORUKA(34,0)
      CALL PORUKA(35,0)
      CALL PORUKA(36,0)
      CALL PORUKA(37,0)
      CALL PORUKA(38,0)
      CALL PORUKA(39,0)
      CALL PORUKA(40,0)
      CALL PORUKA(41,0)
      CALL PORUKA(31,0)
      CALL PORUKA(42,0)
      CALL PORUKA(43,0)
      CALL PORUKA(31,0)
      CALL PORUKA(44,0)
C
      CALL READL1  
      CALL NORMAL(0)
      CALL VAR 
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.'FIX')GOTO 98
         IF(ID(1).EQ.LE)GOTO 99 
         IF(ID(1).EQ.LH)GOTO 1
      ENDIF
      IF(ITIP.NE.2)GOTO 1
      IF(INUM.LT.1.OR.INUM.GT.9)THEN
          CALL PORUKA(45,0)
          GOTO 1
      ENDIF
C
      IC=INUM  
C
      GOTO(10,20,30,40,50,60,70,80,90),IC
C
C-------------------------- TIP LEVE STRELICE --------------------------------
10    CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE)GOTO 1
C          IF(ID(1).EQ.LH)CALL ARRHLP
          GOTO 10
      ENDIF
      IF(ITIP.NE.2)THEN
          CALL WIDTH(3)
          CALL PORUKA(46,0)
          CALL READL1
          GOTO 10
      ENDIF
      IF(INUM.GT.IAFMAX)THEN
          CALL WIDTH(3)
          CALL PORUKA(47,0)
          CALL PORUKA(48,0)
          CALL READL1
          CALL NORMAL(0)
          GOTO 10
      ENDIF
      IAF1=INUM
      INDC(1)=1
      GOTO 1
C------------------------------------------------------------------------
C-------------------------- TIP DESNE STRELICE --------------------------
20    CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE)GOTO 1
C          IF(ID(1).EQ.LH)CALL ARRHLP
          GOTO 20
      ENDIF
      IF(ITIP.NE.2)THEN
          CALL WIDTH(3)
          CALL PORUKA(49,0)
          CALL READL1
          GOTO 20
      ENDIF
      IF(INUM.GT.IAFMAX)THEN
          CALL WIDTH(3)
          CALL PORUKA(47,0)
          CALL PORUKA(48,0)
          CALL READL1
          CALL NORMAL(0)
          GOTO 20
      ENDIF
      IAF2=INUM
      INDC(2)=1
      GOTO 1
C------------------------------------------------------------------------
C-------------------------- TIP KOTNE LINIJE ----------------------------
30    CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE)GOTO 1
C          IF(ID(1).EQ.LH)CALL KLNHLP
          GOTO 30
      ENDIF
      IF(ITIP.NE.2)THEN
          CALL WIDTH(3)
          CALL PORUKA(50,0)
          CALL READL1
          GOTO 30
      ENDIF
      IF(INUM.LT.0.OR.INUM.GT.IKFMAX)THEN
          CALL WIDTH(3)
          CALL PORUKA(47,0)
          CALL PORUKA(51,0)
          CALL READL1
          CALL NORMAL(0)
      ENDIF
      IKF=INUM
      INDC(3)=1
      GOTO 1
C------------------------------------------------------------------------
C-------------------------- TIP KOTNOG TEKSTA ---------------------------
40    CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE)GOTO 1
C          IF(ID(1).EQ.LH)CALL KTXHLP
          GOTO 40
      ENDIF
      IF(ITIP.NE.2)THEN
          CALL WIDTH(3)
          CALL PORUKA(52,0)
          CALL PORUKA(53,0)
          CALL READL1
          GOTO 40
      ENDIF
      IF(INUM.EQ.999)INUM=0
      IF(INUM.EQ.0)INUM=999
      ITF=INUM
      INDC(4)=1
      GOTO 1
C------------------------------------------------------------------------
C------------------ DUZINA I VISINA STELICE -----------------------------
50    CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)THEN
         CALL WIDTH(3)
         CALL PORUKA(54,0)
         CALL READL1
         GOTO 50
      ENDIF
      SXX=RNUM
      CALL VARG
      IF(ITIP.NE.2)GOTO 50
      SYY=RNUM
      INDC(5)=1
      GOTO 1
C------------------------------------------------------------------------
C--------------------------- VISINA TEKSTA ------------------------------
60    CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)THEN
         CALL WIDTH(3)
         CALL PORUKA(55,0)
         CALL READL1
         GOTO 60
      ENDIF
      HCC=RNUM
      INDC(6)=1
      GOTO 1
C------------------------------------------------------------------------
70    GOTO 1
C------------------------ RAZDALJINA KOTNE LINIJE OD TACAKA -------------
80    CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE)GOTO 1
      ENDIF
      IF(ITIP.NE.2)THEN
         CALL WIDTH(4)
         CALL PORUKA(57,0)
         CALL PORUKA(58,0)
         CALL READL1
         GOTO 80
      ENDIF
      DOLD=DU
      DU=RNUM
      INDC(8)=1
      GOTO 1
C--------------------------------------------------------------------------
90    GOTO 1
C---------- SMESTANJE ISPRAVKI U TABELU -----------------------------------
98    CONTINUE
      DO 200 I=1,IELI
         IF(IELTYP(I).NE.12)GOTO 200
         IF(IWTEK.NE.1)THEN
             IF(IELLAY(I).NE.ILAY)GOTO 200
         ENDIF
C
         IP=IELPOI(I)
         IF(INDC(1).EQ.0)IAF1=IFIX(ARGEL(IP+1))
         IF(INDC(2).EQ.0)IAF2=IFIX(ARGEL(IP+2))
         IF(INDC(3).EQ.0)IKF=IFIX(ARGEL(IP+3))
         ITFOLD=ITF
         ITF1=IFIX(ARGEL(IP+4))
         IF(INDC(4).EQ.0)THEN
             ITF=ITF1
         ELSE
             IF(ITF1.LT.0)ITF=-ITF
         ENDIF
         IF(INDC(5).EQ.0)SXX=ARGEL(IP+5)
         IF(INDC(5).EQ.0)SYY=ARGEL(IP+6)
         IF(INDC(6).EQ.0)HCC=ARGEL(IP+7)
         X1=ARGEL(IP+8)
         Y1=ARGEL(IP+9)
         X2=ARGEL(IP+10)
         Y2=ARGEL(IP+11)
         IF(INDC(7).EQ.0)XKK=ARGEL(IP+12)
         IF(INDC(7).EQ.0)YKK=ARGEL(IP+13)
         IF(INDC(8).EQ.0)DU=ARGEL(IP+14)
C
C AZURIRANJE POZICIJE TEKSTA
C
         IF(INDC(4).EQ.1.OR.INDC(6).EQ.1.OR.INDC(8).EQ.1)THEN
             NN=ITF
             CALL TEXPOL(X1,Y1,X2,Y2,DU,NN,HCC,XKK,YKK)
             INDC(7)=1
         ENDIF
C
         IF(INDC(1).EQ.1)ARGEL(IP+1)=FLOAT(IAF1)
         IF(INDC(2).EQ.1)ARGEL(IP+2)=FLOAT(IAF2)
         IF(INDC(3).EQ.1)ARGEL(IP+3)=FLOAT(IKF)
         IF(INDC(4).EQ.1)ARGEL(IP+4)=FLOAT(ITF)
         IF(INDC(5).EQ.1)ARGEL(IP+5)=SXX
         IF(INDC(5).EQ.1)ARGEL(IP+6)=SYY
         IF(INDC(6).EQ.1)ARGEL(IP+7)=HCC
         IF(INDC(7).EQ.1)ARGEL(IP+12)=XKK
         IF(INDC(7).EQ.1)ARGEL(IP+13)=YKK
         IF(INDC(8).EQ.1)ARGEL(IP+14)=DU
         CALL SCRAZU(I)
         ITF=ITFOLD
200   CONTINUE
C
99    RETURN
      END
