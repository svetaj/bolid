      SUBROUTINE GLTLA(XD1,YD1,XD2,YD2,XC1,YC1,XC2,YC2,XC3,YC3,IND)
C========================================================================
C2  TESTIRA DA LI JE SPOJ DUZI I LUKA GLADAK
C
C  ULAZNE PROMENLJIVE:
C
C     XD1,YD1     = PRVA TACKA DUZI 
C     XD2,YD2     = DRUGA TACKA DUZI
C     XC1,YC1     = CENTAR LUKA
C     XC2,YC2     = POCETNA TACKA LUKA
C     XC2,YC2     = KRAJNJA TACKA LUKA
C
C  IZLAZNE PROMENLJIVE:
C
C     IND         = INDIKATOR GLATKOSTI  ; 0 - NIJE GLADAK PRELAZ
C                                        1 - PRELAZ JE GLADAK
C
C  POZIVI PODPROGRAMA:
C
C     PARLIN      = PRONALAZI DUZ KOJA JE PARALELNA DATOJ
C     LINB        = PRONALAZI DUZ KOJA JE NORMALNA NA DATU
C     DIST        = PRONALAZI RASTOJANJE DVE DUZI
C------------------------------------------------------------------------
C $Header: gltla.f,v 1.3 90/02/06 12:17:38 sveta Exp $
C $Log:	gltla.f,v $
C---> Revision 1.3  90/02/06  12:17:38  sveta
C---> ispravljeno zaglavlje
C---> 
C========================================================================
C
      DIMENSION XB1(2),YB1(2),XB2(2),YB2(2)
      DATA DST/1.E-2/
C
C PRONALAZENJE DUZI KOJA JE PARALELNA DATOJ 
C
      XT=XC1
      YT=YC1
      CALL PARLIN(XD1,YD1,XD2,YD2,DST,XB1,YB1,XB2,YB2,IND)
      IF(IND.NE.2)THEN
         IND=0
         GOTO 99
      ENDIF
      CALL LINB(XB1(1),YB1(1),XB2(1),YB2(1),XT,YT,XS1,YS1,IND)
      CALL LINB(XB1(2),YB1(2),XB2(2),YB2(2),XT,YT,XS2,YS2,IND)
      CALL DIST(XT,YT,XS1,YS1,D1)
      CALL DIST(XT,YT,XS2,YS2,D2)
      L=1
      IF(D1.LT.D2)L=1
      IF(D1.GT.D2)L=2
C
      XP1=XB1(L)
      YP1=YB1(L)
      XP2=XB2(L)
      YP2=YB2(L)
C
C PRONALAZENJE PRESEKA PARALELNE DUZI SA LUKOM
C
      CALL INTLSA(XP1,YP1,XP2,YP2,XC1,YC1,XC2,YC2,XC3,YC3,
     *            XX,YY,XX1,YY1,IND)
C
      IF(IND.EQ.0) THEN
C        AKO NE POSTOJI PRESEK PARALELNE DUZI I LUKA PRELAZ JE GLADAK
         IND=1
      ELSE
         IND=0
      ENDIF
C
C
99    RETURN
      END
