      SUBROUTINE PRTGLO
C==========================================================
C1   STAMPA GLOBALNE PROMENLJIVE
C1   KOMANDA PRINT <GLOBAL NAMES LIST>
C1   ILI   PRINT  GLOBAL   /  PRINT  ALL
C
C  ULAZ:
C
C    KOMON COMGLO
C
C  IZLAZ:
C
C    EKRAN
C----------------------------------------------------------
C $Header: prtglo.f,v 1.6 90/04/26 09:40:23 sveta Exp $
C $Log:	prtglo.f,v $
C---> Revision 1.6  90/04/26  09:40:23  sveta
C---> nova verzija interpretera
C---> 
C---> Revision 1.5  90/02/09  12:25:00  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.4  88/04/11  13:48:11  vera
C---> MSG --->  PORUKA!!!
C---> 
C---> Revision 1.3  88/04/08  12:05:54  vera
C---> WRITE ---> PORUKA!!!
C---> 
C---> Revision 1.2  88/04/06  11:26:32  vera
C---> WRITE --->  PORUKA!!!
C---> 
C---> Revision 1.1  87/10/05  11:49:07  sveta
C---> Initial revision
C---> 
C==========================================================
C
      INCLUDE 'CONFIG.COM'
      INCLUDE 'REPREZ.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMTXT.COM'
      INCLUDE 'TMPDUM.COM'
C
      IPASS=0
C
10    CONTINUE
      CALL VARG
      IF(ITIP.EQ.NOFIN9) THEN
         IF(IPASS.EQ.0) THEN
            GOTO 50
         ELSE
            CALL PORUKA(832,0)
            GOTO 99
         ENDIF
      ENDIF
C
      IPASS=IPASS+1
C
      IF(ITIP.EQ.ID9) THEN
         IF(ID(1).EQ.LALL7) GOTO 70
         IF(ID(1).EQ.LE) GOTO 99
C
         CALL CONLTX(IDNAME)
         CALL PRTOUT(3,NTX,0,IDNAME(2:NTX+1),1)
         GOTO 10
      ELSE IF (ITIP.EQ.INT9) THEN
         CALL PRTOUT(2,0,RNUM,' ',1)
         GOTO 10
      ENDIF
C
C
      GOTO 10
C
50    CONTINUE
      CALL WIDTH(2)
      PRINT *,' ENTER VARIABLE'
      CALL READL1
      GOTO 10
C
C PRINT ALL
C
70    CONTINUE
      CALL VARIO(60,NGL)
      DO 80 I=2,NGL
         CALL VARIO(7,I)
         PRINT *,ID(1)//ID(2)//ID(3),'=',RNUM
80    CONTINUE
      CALL VARIO(70,NGLIT)
      DO 90 I=2,NGLIT
         CALL VARIO(8,I)
         CALL VARIO(9,I)
         CALL CONLTX(IDNAME)
         PRINT *,ID(1)//ID(2)//ID(3),'=',IDNAME(2:NTX+1)
90    CONTINUE
      GOTO 99
C 
97    CONTINUE
      CALL SNXMSG
C
99    CONTINUE
      RETURN
      END
