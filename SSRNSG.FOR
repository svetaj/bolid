      SUBROUTINE SSRNSG(IOLD,INEW)
C===============================================================
C1 PROMENA BROJA POSTOJECEG SEGMENTA / DINAMICKA ORGANIZACIJA /
C 
C  ####################################################
C  # NAPOMENA: SEGMENT IOLD MORA DA VEC POSTOJI !!!!! #
C  #           (KORISNIK SAM O TOME VODI RACUNA)      #
C  ####################################################
C
C ULAZNA PROMENLJIVA:
C
C   IOLD   = STARI BROJ SEGMENTA
C
C IZLAZNA PROMENLJIVA:
C
C   INEW   = NOVI BROJ SEGMENTA
C
C POZIVI PODPROGRAMA:
C
C   LLRNSG = RENAME-SEGMENT (DTI BIBLIOTEKA)
C---------------------------------------------------------------
C $Header: ssrnsg.f,v 1.3 90/02/12 16:05:01 sveta Exp $
C $Log:	ssrnsg.f,v $
C---> Revision 1.3  90/02/12  16:05:01  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  88/04/11  13:16:10  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.1  88/01/07  14:06:53  sveta
C---> Initial revision
C---> 
C===============================================================
C
      INCLUDE 'LIFOSG.COM'
C
      IF(ITAB.GT.MAXTSG)THEN
          CALL PORUKA(588,0)
          INEW=0
      ELSE
          INEW=TABSEG(ITAB)
          TABSEG(ITAB)=IOLD
          CALL LLRNSG(IOLD,INEW)
      ENDIF
C
      RETURN
      END
