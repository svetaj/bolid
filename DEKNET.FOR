      SUBROUTINE DEKNET
C=====================================================================
C1    RUTINA ZA DEFINISANJE PARAMETARA MREZE
C1    KOMANDA "NET"
C
C   ULAZ:
C
C      USER INTERFACE
C
C   IZLAZ:
C
C      KOMON COMDRF
C      EKRAN
C---------------------------------------------------------------------
C $Header: deknet.f,v 1.10 90/03/21 11:35:50 sveta Exp $
C $Log:	deknet.f,v $
C---> Revision 1.10  90/03/21  11:35:50  sveta
C---> ubacen interpreter
C---> 
C---> Revision 1.9  90/02/05  10:22:00  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.8  90/01/29  14:50:32  sveta
C---> ubacen UNDO
C---> 
C---> Revision 1.7  88/04/11  11:34:09  vera
C---> MSG --->  PORUKA!!!
C---> 
C---> Revision 1.6  88/04/06  10:48:33  vera
C---> PRINT --> PORUKA !!!
C---> 
C---> Revision 1.5  88/02/23  12:28:10  sveta
C---> ?
C---> 
C---> Revision 1.4  88/02/01  13:42:50  sveta
C---> ??
C---> 
C---> Revision 1.3  87/11/12  11:52:01  sveta
C---> ??
C---> 
C---> Revision 1.2  87/10/23  18:16:23  sveta
C---> ??
C---> 
C---> Revision 1.1  87/10/20  19:03:08  sveta
C---> Initial revision
C---> 
C=====================================================================
C  
      INCLUDE 'COMVAR.COM'
      INCLUDE 'COMDO.COM'
      INCLUDE 'OZNAKE.COM'
      INCLUDE 'REPREZ.COM'
      INCLUDE 'CHASIZ.COM'
      INCLUDE 'MEASUN.COM'
      INCLUDE 'COMMSG.COM'
      INCLUDE 'COMDRF.COM'
      INCLUDE 'COMDRM.COM'
      INCLUDE 'COMFIL.COM'
      INCLUDE 'COMWIN.COM'
      INCLUDE 'EROR.COM'
      DIMENSION IVEC(1)
C -------------------------------- DIMENZIJE VEKTORA SU NNMAX
      DIMENSION N1(20),AA(20),N2(20),BB(20)
      CHARACTER NKEY
C  
      NNMAX=20
C
      IF(IWTEK.EQ.1)THEN
         CALL WIDTH(3)
         CALL PORUKA(137,0)
         CALL PORUKA(138,0)
         GOTO 99
      ENDIF
C
C INTERPRETERSKI DEO
C
      CALL VARG
      IF(ITIP.EQ.2)THEN
         X0=RNUM
         CALL VARG
         IF(ITIP.NE.2)GOTO 99
         Y0=RNUM
         CALL VARG
         IF(ITIP.NE.2)GOTO 99
         ALF1=RNUM
         CALL VARG
         IF(ITIP.NE.2)GOTO 99
         ALF2=RNUM
         CALL READL1
         CALL VAR
         IF(ITIP.NE.1)GOTO 99
         IF(ID(1).NE.'NET')GOTO 99
         CALL VARG
         IF(ITIP.NE.2)GOTO 99
         N1UK=INUM
         CALL VARG
         IF(ITIP.NE.2)GOTO 99
         N2UK=INUM
         DO 513 I=1,N1UK
            CALL READL1
            CALL VAR
            IF(ITIP.NE.1)GOTO 99
            IF(ID(1).NE.'NET')GOTO 99
            CALL VARG
            IF(ITIP.NE.2)GOTO 99
            N1(I)=INUM
            CALL VARG
            IF(ITIP.NE.2)GOTO 99
            AA(I)=RNUM
513      CONTINUE
         DO 514 I=1,N2UK
            CALL READL1
            CALL VAR
            IF(ITIP.NE.1)GOTO 99
            IF(ID(1).NE.'NET')GOTO 99
            CALL VARG
            IF(ITIP.NE.2)GOTO 99
            N2(I)=INUM
            CALL VARG
            IF(ITIP.NE.2)GOTO 99
            BB(I)=RNUM
514      CONTINUE
         CALL UNDO(1,0,IVEC)
         CALL DRFNET(X0,Y0,N1UK,N2UK,AA,BB,N1,N2,ALF1,ALF2)
         CALL SCRAZU(IELI)
         GOTO 99
      ELSE
         IF(IDOFLA.EQ.1)GOTO 99
      ENDIF
C
C     NET  
C
300   K=1
400   CONTINUE
      CALL PORUKA(203,0)
      CALL READL1
      CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE) GOTO 99  
          IF(ID(1).EQ.'FIX') GOTO 401  
      ENDIF
      IF(ITIP.NE.2)GOTO 400
      N1(K)=INUM  
C
500   CALL PORUKA(615,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE) GOTO 99  
      ENDIF
      IF(ITIP.NE.2)GOTO 500
      AA(K)=RNUM  
      N1UK=K
      K=K+1
      IF(K.GT.NNMAX)THEN
         CALL PORUKA(204,0)
         GOTO 401
      ENDIF
      GOTO 400
C
401   K=1
4011  CONTINUE
      CALL PORUKA(205,0)
      CALL READL1
      CALL VAR
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE) GOTO 99  
          IF(ID(1).EQ.'FIX') GOTO 402  
      ENDIF
      IF(ITIP.NE.2)GOTO 4011
      N2(K)=INUM  
C
501   CONTINUE
      CALL PORUKA(206,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
          IF(ID(1).EQ.LE) GOTO 99  
      ENDIF
      IF(ITIP.NE.2)GOTO 501
      BB(K)=RNUM  
      N2UK=K
      K=K+1
      IF(K.GT.NNMAX)THEN
         CALL PORUKA(204,0)
         GOTO 402
      ENDIF
      GOTO 4011
C
402   CONTINUE
      CALL PORUKA(207,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE) GOTO 99  
      ENDIF
      IF(ITIP.NE.2) GOTO 402
      ALF2=RNUM 
C
403   CONTINUE
      CALL PORUKA(208,0)
      CALL READL1
      CALL VARG
      IF(ITIP.EQ.1)THEN
         IF(ID(1).EQ.LE) GOTO 99  
      ENDIF
      IF(ITIP.NE.2) GOTO 403
      ALF1=RNUM 
C
      CALL PORUKA(209,0)
      CALL CURSXY(NKEY,X0,Y0)  
      IF(NKEY.EQ.'E') GOTO 400
      IF(NKEY.EQ.'Q') GOTO 99
C
      CALL UNDO(1,0,IVEC)
      CALL DRFNET(X0,Y0,N1UK,N2UK,AA,BB,N1,N2,ALF1,ALF2)
C
98    CALL SCRAZU(IELI)
      GO TO 300
C  
99    RETURN
      END  
