      SUBROUTINE BCKINI
C=========================================================
C1  INICIJALIZACIJA BACK-UP-A
C---------------------------------------------------------
C $Header: bckini.f,v 1.3 90/02/15 11:37:41 sveta Exp $
C $Log:	bckini.f,v $
C---> Revision 1.3  90/02/15  11:37:41  sveta
C---> povecano vreme
C---> 
C---> Revision 1.2  90/02/02  11:09:58  sveta
C---> sredjeno zaglavlje
C---> 
C---> Revision 1.1  90/01/29  14:41:34  sveta
C---> Initial revision
C---> 
C=========================================================
C
      INCLUDE 'COMSET.COM'
C
      CALL DELTAT(IDELTA)
      ITIME=0
      IBACK=60*60*5
C
      RETURN
      END  
