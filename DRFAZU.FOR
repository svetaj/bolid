      SUBROUTINE DRFAZU(IXXX,C)
C=================================================================
C1  AZURIRA TABELU CRTEZA NAKON TRANSLACIJE I ROTACIJE
C
C  ULAZNE PROMENLJIVE:
C
C    IXXX    = REDNI BROJ PRIMITIVE 
C    C       = MATRICA TRANSFORMACIJE
C
C  IZLAZ:
C
C    KOMON COMDRF
C-----------------------------------------------------------------
C $Header: drfazu.f,v 1.10 90/02/05 13:19:24 sveta Exp $
C $Log:	drfazu.f,v $
C---> Revision 1.10  90/02/05  13:19:24  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.9  89/10/11  11:05:06  sveta
C---> ubaceno azuriranje IDS , SQR i FPT simbola
C---> 
C---> Revision 1.8  89/07/20  12:40:45  sveta
C---> ubacene primitive HATCH MDIM
C---> 
C---> Revision 1.7  87/10/23  18:16:43  sveta
C---> ??
C---> 
C---> Revision 1.6  87/10/20  19:03:28  sveta
C---> ??
C---> 
C---> Revision 1.5  87/10/19  17:22:43  sveta
C---> ???
C---> 
C---> Revision 1.4  87/06/29  06:55:01  sveta
C---> ????
C---> 
C---> Revision 1.3  87/06/10  08:12:41  sveta
C---> *** empty log message ***
C---> 
C---> Revision 1.2  87/05/25  13:52:24  sveta
C---> ????
C---> 
C---> Revision 1.1  87/04/09  08:04:16  joca
C---> Initial revision
C---> 
C=================================================================
C
      INCLUDE 'COMDRF.COM'
      INCLUDE 'DIMENS.COM'
      DIMENSION C(3,3),XTEMP(1000),YTEMP(1000)
      NTMP=15
C
      IX=IXXX
      ICOUN=IX
      XP=PIV(1,IX)
      YP=PIV(2,IX)
      ITT=IELTYP(IX)
C
1     CONTINUE
C
      IT=IELTYP(IX)
      IP=IELPOI(IX)
      NARG=IELNR(IX)
C  
C     DUZ  
C  
         IF(IT.EQ.1)THEN
             CALL POTR2D(ARGEL(IP),ARGEL(IP+1),XP,YP,C,X1,Y1)
             ARGEL(IP)=X1 
             ARGEL(IP+1)=Y1
             CALL POTR2D(ARGEL(IP+2),ARGEL(IP+3),XP,YP,C,X2,Y2) 
             ARGEL(IP+2)=X2
             ARGEL(IP+3)=Y2
C  
C     POLYLINE 
C  
         ELSE IF(IT.EQ.2)THEN
             CALL POTR2D(ARGEL(IP),ARGEL(IP+1),XP,YP,C,X1,Y1)
             ARGEL(IP)=X1 
             ARGEL(IP+1)=Y1
             ICOUN1=2  
21           X=ARGEL(IP+ICOUN1)
             ICOUN1=ICOUN1+1
             Y=ARGEL(IP+ICOUN1)
             ICOUN1=ICOUN1+1
             CALL POTR2D(X,Y,XP,YP,C,X1,Y1)
             ARGEL(IP+ICOUN1-2)=X1
             ARGEL(IP+ICOUN1-1)=Y1
             IF(ICOUN1.LT.NARG)GOTO 21
C  
C     SPLINE
C  
         ELSE IF(IT.EQ.3)THEN
             CONTINUE 
C  
C     CIRCLE 
C  
         ELSE IF(IT.EQ.4)THEN
             CALL POTR2D(ARGEL(IP),ARGEL(IP+1),XP,YP,C,X1,Y1)
             ARGEL(IP)=X1 
             ARGEL(IP+1)=Y1
C  
C     ARC  
C  
         ELSE IF(IT.EQ.5)THEN
C
C     ARC 
C
             CALL POTR2D(ARGEL(IP),ARGEL(IP+1),XP,YP,C,X1,Y1)
             CALL POTR2D(ARGEL(IP+2),ARGEL(IP+3),XP,YP,C,X2,Y2) 
             CALL POTR2D(ARGEL(IP+4),ARGEL(IP+5),XP,YP,C,X3,Y3) 
             ARGEL(IP)=X1 
             ARGEL(IP+1)=Y1
             ARGEL(IP+2)=X2
             ARGEL(IP+3)=Y2
             ARGEL(IP+4)=X3
             ARGEL(IP+5)=Y3
C  
C     ELLIPSE  
C
         ELSE IF(IT.EQ.6)THEN
             CALL TRROT(ARGEL(IP),ARGEL(IP+1),ARGEL(IP+2),ARGEL(IP+4),
     *                  XP,YP,C,X2,Y2,ALFA2)  
             ARGEL(IP)=X2 
             ARGEL(IP+1)=Y2
             ARGEL(IP+4)=ALFA2
C  
C     HYPERBOLA
C
         ELSE IF(IT.EQ.7)THEN
             CONTINUE 
C  
C     PARABOLA 
C
         ELSE IF(IT.EQ.8)THEN
             CONTINUE 
C  
C     BOX  
C
         ELSE IF(IT.EQ.9)THEN
             CALL TRROT(ARGEL(IP),ARGEL(IP+1),ARGEL(IP+2),ARGEL(IP+4),
     *                  XP,YP,C,X2,Y2,ALFA2)  
             ARGEL(IP)=X2 
             ARGEL(IP+1)=Y2
             ARGEL(IP+4)=ALFA2
C  
C     POINT
C
         ELSE IF(IT.EQ.10)THEN
             CALL POTR2D(ARGEL(IP),ARGEL(IP+1),XP,YP,C,X1,Y1)
             ARGEL(IP)=X1 
             ARGEL(IP+1)=Y1
C  
C     TEXT 
C
         ELSE IF(IT.EQ.11)THEN
             CALL TRROT(ARGTX(1,IP),ARGTX(2,IP),1.,ARGTX(4,IP),
     *                  XP,YP,C,X2,Y2,ALFA2) 
             ANGTX=ALFA2  
             ARGTX(1,IP)=X2
             ARGTX(2,IP)=Y2
             ARGTX(4,IP)=ALFA2
C  
C     DIM  
C
         ELSE IF(IT.EQ.12)THEN
             CALL POTR2D(ARGEL(IP+8),ARGEL(IP+9),XP,YP,C,X1,Y1)
             CALL POTR2D(ARGEL(IP+10),ARGEL(IP+11),XP,YP,C,X2,Y2)
             CALL POTR2D(ARGEL(IP+12),ARGEL(IP+13),XP,YP,C,XK,YK)
             ARGEL(IP+8)=X1
             ARGEL(IP+9)=Y1
             ARGEL(IP+10)=X2
             ARGEL(IP+11)=Y2
             ARGEL(IP+12)=XK
             ARGEL(IP+13)=YK
C  
C     SET - BEGIN FILL AREA, END FILL AREA 
C
         ELSE IF(IT.EQ.13)THEN
             CONTINUE
C  
C     NET  
C
         ELSE IF(IT.EQ.15)THEN
             CALL TRROT(ARGEL(IP),ARGEL(IP+1),1.,ARGEL(IP+2),
     *                  XP,YP,C,X2,Y2,ALFA2)  
             ARGEL(IP)=X2 
             ARGEL(IP+1)=Y2
             ARGEL(IP+2)=ALFA2
C
C--------------------------------------------------------------
C HATCH
C
      ELSE IF(IT.EQ.16)THEN
          ILOOP=IFIX(ARGEL(IP))
          ICP=0
          DO 41 I=1,ILOOP
              INL=IFIX(ARGEL(IP+I))
              ICP=ICP+INL
41        CONTINUE 
C
          IPC=IP+ILOOP+4
C
          DO 42 I=1,ICP
              IPC=IPC+1
              XPOL=ARGEL(IPC)
              IPC=IPC+1
              YPOL=ARGEL(IPC)
              CALL POTR2D(XPOL,YPOL,XP,YP,C,ARGEL(IPC-1),ARGEL(IPC))
42        CONTINUE
C--------------------------------------------------------------
C MECHANICAL DIMENSIONING
C
      ELSE IF(IT.EQ.17)THEN
          IDIM=IFIX(ARGEL(IP))
          IPD=IP
          DO 1710 I=1,IIXDMX
             IPD=IPD+1
1710      CONTINUE
          DO 1720 I=1,IXDMAX
             IPD=IPD+1
1720      CONTINUE
          IPDSAV=IPD
          DO 1730 I=1,NTMP
             IPD=IPD+1
             XTEMP(I)=ARGEL(IPD)
1730      CONTINUE
          XOLD=XTEMP(2)
          DO 1740 I=1,NTMP
             IPD=IPD+1
             YTEMP(I)=ARGEL(IPD)
1740      CONTINUE
C
          DO 1741 I=1,NTMP
              CALL POTR2D(XTEMP(I),YTEMP(I),XP,YP,C,XT1,YT1)
              XTEMP(I)=XT1
              YTEMP(I)=YT1
1741      CONTINUE
          IF(IDIM.EQ.6.OR.IDIM.EQ.7)XTEMP(2)=XOLD
C
          IPD=IPDSAV
          DO 2730 I=1,NTMP
             IPD=IPD+1
             ARGEL(IPD)=XTEMP(I)
2730      CONTINUE
          DO 2740 I=1,NTMP
             IPD=IPD+1
             ARGEL(IPD)=YTEMP(I)
2740      CONTINUE
C
C--------------------------------------------------------------
C IDS
C
      ELSE IF(IT.EQ.18)THEN
          IPPP=IP+10
          DO 3000 I=1,9
             CALL POTR2D(ARGEL(IPPP),ARGEL(IPPP+1),XP,YP,C,XT1,YT1)
             ARGEL(IPPP)=XT1
             ARGEL(IPPP+1)=YT1
             IPPP=IPPP+2
3000      CONTINUE
C
C--------------------------------------------------------------
C SQR
C
      ELSE IF(IT.EQ.19)THEN
          IPPP=IP+33
          CALL POTR2D(ARGEL(IPPP),ARGEL(IPPP+1),XP,YP,C,XT1,YT1)
          CALL TRROT(ARGEL(IPPP),ARGEL(IPPP+1),1.,ARGEL(IPPP-1),
     *                  XP,YP,C,X2,Y2,ALFA2)  
          ARGEL(IPPP)=X2
          ARGEL(IPPP+1)=Y2
          ARGEL(IPPP-1)=ALFA2
          IPPP=IPPP+2
          DO 4000 I=1,8
             CALL POTR2D(ARGEL(IPPP),ARGEL(IPPP+1),XP,YP,C,XT1,YT1)
             ARGEL(IPPP)=XT1
             ARGEL(IPPP+1)=YT1
             IPPP=IPPP+2
4000      CONTINUE
C
C--------------------------------------------------------------
C FPT
C
      ELSE IF(IT.EQ.20)THEN
          IPPP=IP+IFIX(ARGEL(IP))+IFIX(ARGEL(IP+1))*3+10
          DO 5000 I=1,9
             CALL POTR2D(ARGEL(IPPP),ARGEL(IPPP+1),XP,YP,C,XT1,YT1)
             ARGEL(IPPP)=XT1
             ARGEL(IPPP+1)=YT1
             IPPP=IPPP+2
5000      CONTINUE
C-------------------------------------------------------------
      ENDIF
C  
C MAKRO
C
         IF(ITT.EQ.14)THEN
             IF(IT.EQ.14)THEN
                 IPPP=IELPOI(IXXX)
                 A1=ARGMAC(1,IPPP)
                 A2=ARGMAC(2,IPPP)
                 A3=ARGMAC(3,IPPP)
                 CALL TRROT(A1,A2,1.,A3,XP,YP,C,B1,B2,B3)
                 ARGMAC(1,IPPP)=B1 
                 ARGMAC(2,IPPP)=B2 
                 ARGMAC(3,IPPP)=B3 
             ENDIF
C---------------------------------- AZURIRANJE PRIMITIVA
C                                   KOJE PRIPADAJU MAKROU
             ICOUN=ICOUN+1
             IX=ICOUN
             IF(ICOUN.GT.IELI)GOTO 98
             IF(IELMA(ICOUN).EQ.IXXX)GOTO 1
         ENDIF
C
98    CALL POTR2D(XP,YP,XP,YP,C,XXP,YYP)
      PIV(1,IXXX)=XXP
      PIV(2,IXXX)=YYP
C
99    RETURN
      END  
