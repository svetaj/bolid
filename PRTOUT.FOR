      SUBROUTINE PRTOUT(IOPER,IVALUE,RVALUE,AVALUE,INDCR)
C======================================================================
C1  SUBROUTINE TO TAKE CARE FOR ALL OUTPUT PROCESSING ON CONTROL
C1  TERMINAL (UNIT 6)
C
C  ULAZNE PROMENLJIVE:
C
C     IOPER   = INDIKATOR AKCIJE
C               1 - PUNI BUFFER SA IVALUE - FORMAT I7
C               2 - PUNI BUFFER SA RVALUE - FORMAT G10.5
C               3 - PUNI BUFFER SA AVALUE - FORMAT (*)
C                   AVALUE IMA IVALUE KARAKTERA
C     IVALUE  = CEO BROJ KOJI SE STAMPA - IOPER = 1
C               BROJ KARAKTERA          - IOPER = 3
C     RVALUE  = REALNI BROJ KOJI SE STAMPA
C     AVALUE  = STRING KOJI SE STAMPA
C     INDCR   = DA LI SE SALJE <RETURN> (PRAZNI BUFFER)
C               0 - PRAZNI BUFFER
C               1 - JOS NE STAMPA
C
C  IZLAZ:
C
C     EKRAN
C----------------------------------------------------------------------
C $Header: prtout.f,v 1.3 90/02/09 12:25:11 sveta Exp $
C $Log:	prtout.f,v $
C---> Revision 1.3  90/02/09  12:25:11  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.2  88/06/15  09:54:28  sveta
C---> UBACEN GRAFICKI DIJALOG!
C---> 
C---> Revision 1.1  88/03/31  09:02:12  vera
C---> Initial revision
C---> 
C---> Revision 1.2  88/01/12  12:51:23  joca
C---> Printout through PRTOUT is beautified !
C---> 
C---> Revision 1.1  88/01/08  14:24:01  joca
C---> Initial revision
C---> 
C
C======================================================================
C
      INCLUDE 'COMDIA.COM'
      INTEGER IVALUE
      REAL RVALUE
      CHARACTER*(*) AVALUE
      CHARACTER*256 OUTBUF
      CHARACTER*80 TMP,TMP1,MESG
      INTEGER*4 IOUTB(64),ITMP1(20)
      EQUIVALENCE (IOUTB(1),OUTBUF),(TMP1,ITMP1(1))
      DATA MESG/' -----> PRESS <RETURN> TO CONTINUE <-----'/
C
C OUTPUT BUFFER
      DATA KLAST/0/
C
C-----------------------------
       GOTO (100,200,300),IOPER
C-----------------------------
100    CONTINUE
C
C INTEGER FORMATS
C
       WRITE(TMP,9100) IVALUE
9100   FORMAT(1X,I7)
C
       IBEG=KLAST+1
       IEND=IBEG+7
       OUTBUF(IBEG:IEND)=TMP(1:8)
C
       KLAST=IEND
       GOTO 90
C-----------------------------
200    CONTINUE
C
C INTEGER FORMATS
C
       WRITE(TMP,9200) RVALUE
9200   FORMAT(1X,G10.5)
C
       IBEG=KLAST+1
       IEND=IBEG+10
       OUTBUF(IBEG:IEND)=TMP(1:11)
C
       KLAST=IEND
       GOTO 90
C-----------------------------
300    CONTINUE
C
C CHARACTER FORMATS
C
       IBEG=KLAST+1
       IEND=IBEG+IVALUE
       OUTBUF(IBEG:IEND)=AVALUE(1:IVALUE)
C
       KLAST=IEND-1
       GOTO 90
C
C++++++++
C
90     CONTINUE
       IF (INDCR.EQ.0) THEN
C
C FLUSH OUTBUF - STANDARD OUTPUT
C
           IF(IDRAW.EQ.0)THEN
              WRITE(6,9000) OUTBUF(1:KLAST) 
9000          FORMAT(A)
           ELSE
C
C-------------------------- DRAWS DIALOG ON GRAPHICS AREA --------------
C
C              IF(IDWIND.EQ.0.AND.ISWIN.EQ.1)CALL INIDIA
              CALL LLTXIN(IBBB)
              CALL LLGPRC(1)
              IDALAS=IDALAS+1
              IF(IDALAS.EQ.IDAMX)THEN
                  IF(IAAA.EQ.2)THEN
                      DIALOG(IDALAS)=MESG
                      NCD(IDALAS)=40
                      CALL SYMBOL(XDIAL,YDIAL(IDALAS),HDI,IOUTB,0.,
     *                            NCD(IDALAS))
                      CALL READL1
                  ENDIF
                  CALL RSTDIA
                  IDALAS=1
              ENDIF
              DIALOG(IDALAS)=OUTBUF(1:80)
              NCD(IDALAS)=KLAST
              CALL SYMBOL(XDIAL,YDIAL(IDALAS),HDI,IOUTB,0.,NCD(IDALAS))
              CALL LLGPRC(2)
              CALL LLCODE(0)
           ENDIF
C
           KLAST=0
       ENDIF
C
       RETURN
       END
