      SUBROUTINE FETMFX
C======================================================================
C1  FETCH A LIST OF FREE ENTRIES FROM THE LINKED CHAIN.
C1  FREE ENTRIES LIST IS EMPTY AT THIS POINT
C
C  ULAZ:
C
C      MAF BAZA PODATAKA
C----------------------------------------------------------------------
C $Header: fetmfx.f,v 1.1 90/02/15 11:39:55 sveta Exp $
C $Log:	fetmfx.f,v $
C---> Revision 1.1  90/02/15  11:39:55  sveta
C---> Initial revision
C---> 
C---> Revision 1.1  88/02/24  06:58:58  joca
C---> Initial revision
C---> 
C======================================================================
C
      INCLUDE 'CONFIG.COM'
      INCLUDE 'COMMFX.COM'
      INCLUDE 'COMSTF.COM'
      INCLUDE 'TMPDUM.COM'
C
C VERIFY THAT LINK NUMBER IS OK ( MUST BE A VALID RECORD )
C
      IF (LASTEY.LE.0.OR.LASTEY.GT.NAWMA) GOTO 98
C
C GET POINTED RECORD
C
      CALL READD(LUNMAT,LASTEY,TEMP)
C
      INEXT=TEMP(1)
C
C VERIFY RECORD VALIDITY
C
      IF (TEMP(2).NE.IVALKY) GOTO 98
      IF (TEMP(3).NE.ISIGX ) GOTO 98
C
C CHAIN IS OK ! LOAD STACK FROM RECORD
C
      DO 10 I=1,NSMAXX
         MXSTAC(I+3)=TEMP(I+3)
10    CONTINUE
      NSTAMX=NSMAXX
C
C RELEASE RECORD LASTEY
C
      CALL MAFFRE(LASTEY)
C
C RESET INDEX TO NEXT LINKED RECORD
C
      LASTEY=INEXT
C
      GOTO 99
C----------------------------------
C INTERNAL ERROR : CHAIN CORRUPTION
C
98    CONTINUE
      CALL PORUKA(1663,0)
      LASTEY=0
C
99    CONTINUE
      RETURN
      END
