      SUBROUTINE KBLOCK
C======================================================================
C1 KEYBOARD LOCK
C----------------------------------------------------------------------
C $Header: kblock.f,v 1.1 90/03/21 11:42:27 sveta Exp $
C $Log:	kblock.f,v $
C---> Revision 1.1  90/03/21  11:42:27  sveta
C---> Initial revision
C---> 
C======================================================================
C
      INCLUDE 'COMCAP.COM'
C
      IF (ICAPT.EQ.0.AND.IBATCH.EQ.0) THEN
          CALL LLKBLK(1)
          CALL LLDUMP
      ENDIF
C
      RETURN
      END
