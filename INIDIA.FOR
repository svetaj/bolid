      SUBROUTINE INIDIA
C==============================================================
C1 INICIJALIZACIJA GRAFICKOG DIJALOGA
C
C  IZLAZ:
C
C     KOMON COMDIA
C--------------------------------------------------------------
C $Header: inidia.f,v 1.2 90/02/07 10:29:01 sveta Exp $
C $Log:	inidia.f,v $
C---> Revision 1.2  90/02/07  10:29:01  sveta
C---> ispravljeno zaglavlje
C---> 
C---> Revision 1.1  88/06/15  09:53:54  sveta
C---> Initial revision
C---> 
C==============================================================
C
      INCLUDE 'COMDIA.COM'
      INCLUDE 'COMWIN.COM'
C
      RFAXC=1.5
      IF(ISWIN.EQ.1)RFAXC=2.3
      XDIAL=2.
      YDIAL(IDAMX)=0.
      DO 5553 I=IDAMX-1,1,-1
         YDIAL(I)=YDIAL(I+1)+HDI*RFAXC
5553  CONTINUE
C
      RETURN
      END
